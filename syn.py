import os
import sys
import argparse
import datetime

import gc
import cx_Oracle as cx
import pyodbc as hadoop

from elasticsearch import Elasticsearch
from elasticsearch import helpers

from kafka import KafkaConsumer
from kafka import KafkaProducer
from kafka.errors import KafkaError
from datetime import datetime, timedelta
import time

import pandas as pd
import json

from elasticsearch.connection import create_ssl_context
import urllib3
import ssl


def fields(cursor):
    """ Given a DB API 2.0 cursor object that has been executed, returns
    a dictionary that maps each field name to a column index; 0 and up. """
    results = {}
    column = 0
    for d in cursor.description:
        results[d[0]] = column
        column = column + 1

    return results


def target_elasticsearch(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT, _TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                         _TARGET_STATIC, _TARGET_FIELD_STATIC, array_target):
    urllib3.disable_warnings()

    host = _TARGET_PARAM_HOST
    port = _TARGET_PARAM_PORT
    user = _TARGET_DATABASE_USER
    password = _TARGET_DATABASE_PASSWORD
    now = datetime.today()

    ssl_context = create_ssl_context(cafile='cacert-esd.pem')
    ssl_context.check_hostname = False
    ssl_context.verify_mode = ssl.CERT_OPTIONAL
    es = Elasticsearch(hosts=[{'host': host, 'port': _TARGET_PARAM_PORT}],
                       scheme="https",
                       verify_certs=True,
                       ssl_context=ssl_context,
                       http_auth=(user, password))
    if _TARGET_FIELD_STATIC == "":
        document_id = str(now.strftime('%Y-%m-%d'))
    else:
        document_id = _TARGET_FIELD_STATIC

    index = _TARGET_STATIC + document_id
    print(index)
    doc_list = []
    now = datetime.today()
    now_timestatmp = now.strftime('%Y-%m-%dT%H:%M:%S') + ('-%02d' % (now.microsecond / 10000))
    ind = 0
    topic_name = ""
    if len(array_target) > 0:
        for item_target in array_target:
            doc_list += [item_target]

        print(index)
        if len(doc_list) > 0:
            try:
                print(
                    "\nAttempting to index =" + index + " the list of docs using helpers.bulk(" + str(
                        len(doc_list)) + ")")

                resp = helpers.bulk(es, doc_list, index=index, doc_type='_doc')
                print("helpers.bulk " + now_timestatmp + " = ", resp)
            except Exception as e:
                print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
            finally:
                es.close()


# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
def read_config(id):
    return True;


def write_log():
    return True;


def is_connection_validate(HOST, PORT, USER, PASS):
    return len(HOST) > 0 and len(PORT) > 0 and len(USER) > 0 and len(PASS) > 0


def process(host, port, service, user, password):
    try:
        _SOURCE_ID = ""
        _TARGET_ID = ""
        _SOURCE_STATIC = ""
        _SOURCE_CUSTOMIZE = ""
        _TARGET_STATIC = ""
        _TARGET_CUSTOMIZE = ""
        _SOURCE_CONNECTION_NAME = ""
        _SOURCE_PARAM_HOST = ""
        _SOURCE_PARAM_PORT = ""
        _SOURCE_PARAM_SERVICE_NAME = ""
        _SOURCE_CONNECTION_TYPE = ""
        _SOURCE_DATABASE_USER = ""
        _SOURCE_DATABASE_PASSWORD = ""
        _TARGET_CONNECTION_NAME = ""
        _TARGET_PARAM_HOST = ""
        _TARGET_PARAM_PORT = ""
        _TARGET_PARAM_SERVICE_NAME = ""
        _TARGET_CONNECTION_TYPE = ""
        _TARGET_DATABASE_USER = ""
        _TARGET_DATABASE_PASSWORD = ""
        _SOURCE_CHECK_SCHEMA = ""
        _TARGET_CHECK_SCHEMA = ""
        _SOURCE_CHECK_SECURE = ""
        _TARGET_CHECK_SECURE = ""
        _SOURCE_FIELD_STATIC = ""
        _TARGET_FIELD_STATIC = ""
        sql_config = " select " \
                     "\n ss.SOURCE_ID " \
                     "\n ,ss.TARGET_ID" \
                     "\n ,ss.SOURCE_STATIC" \
                     "\n ,ss.SOURCE_CUSTOMIZE" \
                     "\n ,ss.TARGET_STATIC" \
                     "\n ,ss.TARGET_CUSTOMIZE" \
                     "\n ,s.CONNECTION_NAME as SOURCE_CONNECTION_NAME" \
                     "\n ,s.PARAM_HOST as SOURCE_PARAM_HOST" \
                     "\n ,s.PARAM_PORT as SOURCE_PARAM_PORT" \
                     "\n ,s.PARAM_SERVICE_NAME as SOURCE_PARAM_SERVICE_NAME" \
                     "\n ,s.CONNECTION_TYPE as SOURCE_CONNECTION_TYPE" \
                     "\n ,s.DATABASE_USER as SOURCE_DATABASE_USER" \
                     "\n ,s.DATABASE_PASSWORD as SOURCE_DATABASE_PASSWORD" \
                     "\n ,t.CONNECTION_NAME as TARGET_CONNECTION_NAME" \
                     "\n ,t.PARAM_HOST as TARGET_PARAM_HOST" \
                     "\n ,t.PARAM_PORT as TARGET_PARAM_PORT" \
                     "\n ,t.PARAM_SERVICE_NAME as TARGET_PARAM_SERVICE_NAME" \
                     "\n ,t.CONNECTION_TYPE as TARGET_CONNECTION_TYPE" \
                     "\n ,t.DATABASE_USER as TARGET_DATABASE_USER" \
                     "\n ,t.DATABASE_PASSWORD as TARGET_DATABASE_PASSWORD" \
                     "\n ,s.CHECK_SCHEMA as SOURCE_CHECK_SCHEMA" \
                     "\n ,t.CHECK_SCHEMA as TARGET_CHECK_SCHEMA" \
                     "\n ,ss.SOURCE_FIELD_STATIC as SOURCE_FIELD_STATIC" \
                     "\n ,ss.TARGET_FIELD_STATIC as TARGET_FIELD_STATIC" \
                     "\n ,s.CHECK_SECURITY as SOURCE_CHECK_SECURITY" \
                     "\n ,t.CHECK_SECURITY as TARGET_CHECK_SECURITY" \
                     "\n from DWH_CONFIG_SYNCHRONIZE ss" \
                     "\n left join DWH_CONFIG_CONNECTION s on (ss.SOURCE_ID=s.ID)" \
                     "\n left join DWH_CONFIG_CONNECTION t on (ss.TARGET_ID=T.ID)" \
                     "\n where ss.status='0'"
        dsn_tns_config = cx.makedsn(host, port, service_name=service)
        connect_config = cx.connect(user, password, dsn=dsn_tns_config)
        print(connect_config)
        print(sql_config)
        if connect_config:
            print(connect_config)
            cursor_config = connect_config.cursor()
            cursor_config.execute(sql_config)
            result_config = cursor_config.fetchall()
            if len(result_config) > 0:
                for item_config in result_config:
                    _SOURCE_ID = str(item_config[0])
                    _TARGET_ID = str(item_config[1])
                    _SOURCE_STATIC = str(item_config[2])
                    _SOURCE_CUSTOMIZE = str(item_config[3])
                    _TARGET_STATIC = str(item_config[4])
                    _TARGET_CUSTOMIZE = str(item_config[5])
                    _SOURCE_CONNECTION_NAME = str(item_config[6])
                    _SOURCE_PARAM_HOST = str(item_config[7])
                    _SOURCE_PARAM_PORT = str(item_config[8])
                    _SOURCE_PARAM_SERVICE_NAME = str(item_config[9])
                    _SOURCE_CONNECTION_TYPE = str(item_config[10])
                    _SOURCE_DATABASE_USER = str(item_config[11])
                    _SOURCE_DATABASE_PASSWORD = str(item_config[12])
                    _TARGET_CONNECTION_NAME = str(item_config[13])
                    _TARGET_PARAM_HOST = str(item_config[14])
                    _TARGET_PARAM_PORT = str(item_config[15])
                    _TARGET_PARAM_SERVICE_NAME = str(item_config[16])
                    _TARGET_CONNECTION_TYPE = str(item_config[17])
                    _TARGET_DATABASE_USER = str(item_config[18])
                    _TARGET_DATABASE_PASSWORD = str(item_config[19])
                    _SOURCE_CHECK_SCHEMA = str(item_config[20])
                    _TARGET_CHECK_SCHEMA = str(item_config[21])
                    _SOURCE_FIELD_STATIC = str(item_config[22])
                    _TARGET_FIELD_STATIC = str(item_config[23])
                    _SOURCE_CHECK_SECURE = str(item_config[24])
                    _TARGET_CHECK_SECURE = str(item_config[25])

                    is_source = (is_connection_validate(_SOURCE_PARAM_HOST, _SOURCE_PARAM_PORT, _SOURCE_DATABASE_USER,
                                                        _SOURCE_DATABASE_PASSWORD))
                    is_target = (is_connection_validate(_SOURCE_PARAM_HOST, _SOURCE_PARAM_PORT, _SOURCE_DATABASE_USER,
                                                        _SOURCE_DATABASE_PASSWORD))
                    is_source_schema = _SOURCE_CHECK_SCHEMA == "1"
                    is_target_schema = _TARGET_CHECK_SCHEMA == "1"

                    if is_source:
                        if _SOURCE_CONNECTION_TYPE.lower() == "oracle":

                            dsn_tns_source = cx.makedsn(_SOURCE_PARAM_HOST, _SOURCE_PARAM_PORT,
                                                        service_name=_SOURCE_PARAM_SERVICE_NAME)
                            source_connection = cx.connect(_SOURCE_DATABASE_USER, _SOURCE_DATABASE_PASSWORD,
                                                           dsn=dsn_tns_source)
                            print(source_connection)
                            batch_size = 500
                            sql_format_source = ""
                            sql_source = ""
                            field_source_group = ""
                            field_source_qty = 0
                            data_source_target = []
                            array_target = []
                            iseq = 0
                            if _SOURCE_STATIC != "":
                                if _SOURCE_FIELD_STATIC == "":
                                    sql_format_source = "select count(*) as qty from {0}"
                                    sql_source_count = sql_format_source.format(_SOURCE_STATIC)
                                else:
                                    sql_format_source = "select {1},count(*) as qty from {0} group by {1} order by {" \
                                                        "1} desc "
                                    sql_source_count = sql_format_source.format(_SOURCE_STATIC, _SOURCE_FIELD_STATIC)
                            else:
                                sql_source_count = _SOURCE_CUSTOMIZE
                            print(sql_source_count)
                            cursor_source_group_oracle = source_connection.cursor()
                            cursor_source_group_oracle.execute(sql_source_count)
                            result_source_group = cursor_source_group_oracle.fetchall()
                            for item_group in result_source_group:
                                field_source_group = item_group[0]
                                field_source_qty = item_group[1]

                                sql_format_source_where = "select * from {0} where {1} = {2} "
                                sql_source_where = " "
                                with source_connection.cursor() as cursor_source_oracle:
                                    sql_source_where = sql_format_source_where.format(_SOURCE_STATIC,
                                                                                      _SOURCE_FIELD_STATIC,
                                                                                      field_source_group)
                                    print(sql_source_where + "; quantity " + str(field_source_qty) + " rows.")
                                    cursor_source_oracle.execute(sql_source_where)
                                    field_map = fields(cursor_source_oracle)
                                    while True:
                                        # fetch rows
                                        rows = cursor_source_oracle.fetchmany(batch_size)
                                        if not rows:
                                            break
                                        for row in rows:
                                            iseq += 1
                                            item = []
                                            item_target = {}
                                            for col in field_map:
                                                value = row[field_map[col]]
                                                # print(col, ":", row[field_map[col]])
                                                item.append(value)
                                                item_target[col] = row[field_map[col]]
                                            data_source_target.append(tuple(item))
                                            array_target.append(item_target)

                                            if iseq % batch_size == 0:
                                                try:
                                                    target_elasticsearch(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT,
                                                                         _TARGET_DATABASE_USER,
                                                                         _TARGET_DATABASE_PASSWORD, _TARGET_STATIC,
                                                                         str(field_source_group), array_target)
                                                    print("commit {0} rows.".format(str(iseq)))
                                                    iseq = 0
                                                    # establish a new connection
                                                except cx.Error as error:
                                                    print('Error occurred:')
                                                    print(error)
                                                finally:
                                                    data_source_target = []
                                                    array_target = []
                                                    gc.collect()
                                try:
                                    target_elasticsearch(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT,
                                                         _TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                                                         _TARGET_STATIC, str(field_source_group), array_target)
                                    print("commit {0} rows.".format(str(iseq)))
                                    # establish a new connection
                                except cx.Error as error:
                                    print('Error occurred:')
                                    print(error)
                                finally:
                                    data_source_target = []
                                    array_target = []
                                    gc.collect()
                        elif _SOURCE_CONNECTION_TYPE.lower() == "hadoop":
                            dsn_tns_source = cx.makedsn(_SOURCE_PARAM_HOST, _SOURCE_PARAM_PORT,
                                                        _SOURCE_PARAM_SERVICE_NAME)
                            source_connection = cx.connect(_SOURCE_DATABASE_USER, _SOURCE_DATABASE_PASSWORD,
                                                           dsn=dsn_tns_source)
                        elif _SOURCE_CONNECTION_TYPE.lower() == "kafka":
                            if _SOURCE_CHECK_SECURE == "0":
                                try:
                                    print("Creating new kafka consumer using brokers: " +
                                          str(_SOURCE_PARAM_HOST) + ' and topic ' +
                                          _SOURCE_STATIC)
                                    array_target = []
                                    consumer = KafkaConsumer(bootstrap_servers=[_SOURCE_PARAM_HOST],
                                                             auto_offset_reset='earliest')
                                    print(consumer)
                                    if consumer:
                                        consumer.subscribe([_SOURCE_STATIC])
                                        nbr_msg = 100
                                        counter = 0
                                        for message in consumer:
                                            print(message)
                                            print(message.topic)
                                            print(message.partition)
                                            print(message.offset)
                                            print(message.timestamp)
                                            my_bytes_value = message.value
                                            my_json = my_bytes_value.decode('utf8').replace("'", '"')
                                            item_source = json.loads(my_json)
                                            item_target = {}
                                            if item_source:
                                                for key in item_source:
                                                    print(key, ":", item_source[key])
                                                    item_target[key] = item_source[key]
                                            print(item_target)
                                            array_target.append(item_target)
                                            counter = counter + 1
                                            if counter == nbr_msg:
                                                break
                                except Exception as e:
                                    print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
                            elif _SOURCE_CHECK_SECURE == "1":
                                topic = _SOURCE_STATIC
                                ssl_context = ssl.create_default_context(cafile='kkts-ca.pem')
                                consumer = KafkaConsumer(bootstrap_servers=['kkts01:9094'],
                                                         ssl_context=ssl_context,
                                                         sasl_mechanism='GSSAPI',
                                                         enable_auto_commit=False,
                                                         ssl_check_hostname=True,
                                                         security_protocol='SASL_SSL',
                                                         auto_offset_reset='earliest',
                                                         sasl_kerberos_service_name='bigfoot',
                                                         api_version=(0, 10))
                                print(consumer)
                                if consumer:
                                    consumer.subscribe([_SOURCE_STATIC])
                                    print('*********************** ' + _SOURCE_STATIC + ' ***********')
                                    nbr_msg = 100
                                    counter = 0
                                    for message in consumer:
                                        print(message)
                                        print(message.topic)
                                        print(message.partition)
                                        print(message.offset)
                                        print(message.timestamp)
                                        my_bytes_value = message.value
                                        my_json = my_bytes_value.decode('utf8').replace("'", '"')
                                        item_source = json.loads(my_json)
                                        item_target = {}
                                        if item_source:
                                            for key in item_source:
                                                print(key, ":", item_source[key])
                                                item_target[key] = item_source[key]
                                        print(item_target)
                                        array_target.append(item_target)
                                        counter = counter + 1
                                        if counter == nbr_msg:
                                            break
                        elif _SOURCE_CONNECTION_TYPE.lower() == "elasticsearch":
                            dsn_tns_source = cx.makedsn(_SOURCE_PARAM_HOST, _SOURCE_PARAM_PORT,
                                                        _SOURCE_PARAM_SERVICE_NAME)
                            source_connection = cx.connect(_SOURCE_DATABASE_USER, _SOURCE_DATABASE_PASSWORD,
                                                           dsn=dsn_tns_source)
                        elif _SOURCE_CONNECTION_TYPE.lower() == "txtfile":
                            dsn_tns_source = cx.makedsn(_SOURCE_PARAM_HOST, _SOURCE_PARAM_PORT,
                                                        _SOURCE_PARAM_SERVICE_NAME)
                            source_connection = cx.connect(_SOURCE_DATABASE_USER, _SOURCE_DATABASE_PASSWORD,
                                                           dsn=dsn_tns_source)
                        elif _SOURCE_CONNECTION_TYPE.lower() == "csvfile":
                            dsn_tns_source = cx.makedsn(_SOURCE_PARAM_HOST, _SOURCE_PARAM_PORT,
                                                        _SOURCE_PARAM_SERVICE_NAME)
                            source_connection = cx.connect(_SOURCE_DATABASE_USER, _SOURCE_DATABASE_PASSWORD,
                                                           dsn=dsn_tns_source)

                    if is_target:
                        if _TARGET_CONNECTION_TYPE.lower() == "oracle":
                            print(_TARGET_STATIC)

                            dsn_tns_target = cx.makedsn(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT,
                                                        _TARGET_PARAM_SERVICE_NAME)
                            target_connection = cx.connect(_TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                                                           dsn=dsn_tns_target)
                            sql_validate_target = "select cname,coltype from col where lower(tname) = '{0}'".format(
                                _TARGET_STATIC)
                            sql_format_target = "insert into {0} ({1}) values ({2})"
                            sql_value_format = ""
                            sql_column_target = ""
                            sql_insert_target = ""
                            sql_value_target = ""
                            cursor_schema_oracle = target_connection.cursor()
                            cursor_schema_oracle.execute(sql_validate_target)
                            result_schema_oracle = cursor_schema_oracle.fetchall()

                            isfind_column = False
                            if len(array_target) > 0:
                                for item_target in array_target:
                                    for key in item_target:
                                        if len(result_schema_oracle) > 0:
                                            for itcolumn in result_schema_oracle:
                                                isfind_column = False
                                                if str(itcolumn[0]).lower() == key.lower():
                                                    isfind_column = True
                                                if isfind_column:

                                                    if sql_column_target != "":
                                                        sql_column_target += ","
                                                    sql_column_target += key

                                                    if sql_value_format != "":
                                                        sql_value_format += ","
                                                    if str(itcolumn[1]).lower() == "varchar2":
                                                        sql_value_format += "'{0}'".format(key)
                                                    else:
                                                        sql_value_format += "{0}".format(key)

                                    break

                            if len(array_target) > 0:
                                sql_value_target = sql_value_format
                                for item_target in array_target:
                                    for key in item_target:
                                        if sql_column_target.find(key) > -1:
                                            sql_value_target = str(sql_value_target).replace(key, str(item_target[key]))

                                    sql_insert_target = sql_format_target.format(_TARGET_STATIC, sql_column_target,
                                                                                 sql_value_target)
                                    print(sql_insert_target)
                                    cursor_execute_target = target_connection.cursor()
                                    cursor_execute_target.execute(sql_insert_target)
                                if target_connection:
                                    target_connection.commit()

                        elif _TARGET_CONNECTION_TYPE.lower() == "hadoop":
                            dsn_tns_target = cx.makedsn(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT,
                                                        _TARGET_PARAM_SERVICE_NAME)
                            target_connection = cx.connect(_TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                                                           dsn=dsn_tns_target)
                        elif _TARGET_CONNECTION_TYPE.lower() == "kafka":
                            dsn_tns_target = cx.makedsn(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT,
                                                        _TARGET_PARAM_SERVICE_NAME)
                            target_connection = cx.connect(_TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                                                           dsn=dsn_tns_target)
                        elif _TARGET_CONNECTION_TYPE.lower() == "txtfile":
                            dsn_tns_target = cx.makedsn(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT,
                                                        _TARGET_PARAM_SERVICE_NAME)
                            target_connection = cx.connect(_TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                                                           dsn=dsn_tns_target)
                        elif _TARGET_CONNECTION_TYPE.lower() == "csvfile":
                            dsn_tns_target = cx.makedsn(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT,
                                                        _TARGET_PARAM_SERVICE_NAME)
                            target_connection = cx.connect(_TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                                                           dsn=dsn_tns_target)

                        ####################################### PROCESS             #######################################

                        ####################################### TARGET CONNECTION    #######################################
                    # if target_connection:
    except Exception as e:
        print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # os.environ["APP_HOME"] = os.path.dirname(os.path.realpath(__file__))
    # os.environ["APP_PID"] = str(os.getpid())

    _host = "dbdwhdv1"
    _port = "1550"
    _service = "TEDWDEV"
    _user = "TEDWBORAPPO"
    _password = "bortedw123"
    process(_host, _port, _service, _user, _password)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/


"""
var config = new Dictionary<string, object>
{
    { "client.id", "clientid"},
    { "api.version.request", "true" },
    { "security.protocol","sasl_ssl"},
    { "ssl.ca.location","pemfilelocation.pem"},
    { "sasl.mechanisms", "GSSAPI"},
    { "sasl.kerberos.principal", "kafka"},
    { "bootstrap.servers", brokerList}
};


var config = new Dictionary<string, object>
{
    { "group.id", consumerGroup },
    { "client.id", "clientid"},
    { "enable.auto.commit", false },
    { "statistics.interval.ms", 60000 },
    { "bootstrap.servers", brokerList },
    { "security.protocol","sasl_ssl"},
    { "ssl.ca.location","pemfilelocation.pem"},
    { "sasl.mechanisms", "GSSAPI"},
    { "sasl.kerberos.principal", "kafka"},
    { "api.version.request", "true" },
    { "default.topic.config", new Dictionary<string, object>()
        {
            { "auto.offset.reset", "smallest" }
        }
    }
};

"""

