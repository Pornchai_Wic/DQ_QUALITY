import os
import sys
import argparse
import datetime

import gc
import cx_Oracle as cx
import pyodbc as hadoop
import pytz
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from datetime import datetime, timedelta
import time

import pandas as pd
import json

from elasticsearch.connection import create_ssl_context
import urllib3
import ssl
from cryptography.fernet import Fernet, InvalidToken
import logging

key_api = "8zNU418HIcCijV_KPfCm-OwNsVKGhMOxtl906nyzzmM="


def decryption(key: None, param: None):
    cipher_suite = None
    try:
        f = Fernet(key)
    except InvalidToken:
        f = None
    if not f is None:
        try:
            decrypted_bytes = f.decrypt(param.encode("utf-8"))
        except InvalidToken:
            decrypted_bytes = None  # TODO(kmullins): Shall we log this case? Is it expected?
        if not decrypted_bytes is None:
            decrypted_string = decrypted_bytes.decode()  # bytes -> str
        else:
            decrypted_string = "encryption is not support"
    else:
        key = "key is not support"
    return decrypted_string


tz = pytz.timezone('Asia/Bangkok')


def fields(cursor):
    """ Given a DB API 2.0 cursor object that has been executed, returns
    a dictionary that maps each field name to a column index; 0 and up. """
    results = {}
    column = 0
    for d in cursor.description:
        results[d[0]] = column
        column = column + 1

    return results


def target_elasticsearch(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT, _TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                         _TARGET_STATIC, _TARGET_FIELD_STATIC, array_target):
    urllib3.disable_warnings()

    host = _TARGET_PARAM_HOST
    port = _TARGET_PARAM_PORT
    user = _TARGET_DATABASE_USER
    password = _TARGET_DATABASE_PASSWORD
    now = datetime.today()

    ssl_context = create_ssl_context(cafile='cacert-esd.pem')
    ssl_context.check_hostname = False
    ssl_context.verify_mode = ssl.CERT_OPTIONAL
    es = Elasticsearch(hosts=[{'host': host, 'port': _TARGET_PARAM_PORT}],
                       scheme="https",
                       verify_certs=True,
                       ssl_context=ssl_context,
                       http_auth=(user, password))
    if _TARGET_FIELD_STATIC == "":
        document_id = str(now.strftime('%Y-%m-%d'))
    else:
        document_id = _TARGET_FIELD_STATIC

    index = _TARGET_STATIC + document_id
    print(index)
    doc_list = []
    now = datetime.today()
    now_timestatmp = now.strftime('%Y-%m-%dT%H:%M:%S') + ('-%02d' % (now.microsecond / 10000))
    ind = 0
    topic_name = ""
    if len(array_target) > 0:
        for item_target in array_target:
            doc_list += [item_target]

        print(index)
        if len(doc_list) > 0:
            try:
                print(
                    "\nAttempting to index =" + index + " the list of docs using helpers.bulk(" + str(
                        len(doc_list)) + ")")

                resp = helpers.bulk(es, doc_list, index=index, doc_type='_doc')
                print("helpers.bulk " + now_timestatmp + " = ", resp)
            except Exception as e:
                print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
            finally:
                es.close()


def is_connection_validate(HOST, PORT, USER, PASS):
    return len(HOST) > 0 and len(PORT) > 0 and len(USER) > 0 and len(PASS) > 0


def process(host, port, service, user, password, rule_id, transaction_id, _row_commit):
    try:
        _RULE_ID = ""
        _INGRESS_DATA_ID = ""
        _INGRESS_DATA_NAME = ""
        _INGRESS_CONNECTION_ID = ""
        _INGRESS_SQL_COMMAND = ""
        _INGRESS_TARGET_TABLE = ""
        _INGRESS_CONNECTION_NAME = ""
        _INGRESS_PARAM_HOST = ""
        _INGRESS_PARAM_PORT = ""
        _INGRESS_PARAM_SERVICE_NAME = ""
        _CDE_ID = ""
        _CDE_NAME = ""
        _REMARK = ""
        _INGRESS_CONNECTION_TYPE = ""
        _INGRESS_CONNECTION_STRING = ""
        _INGRESS_PARAM_USER = ""
        _INGRESS_PARAM_PASSWORD = ""
        _INGRESS_EXISTS_TABLE = "0"
        is_ingress_exists_table = False
        is_ingress_exec_table = False

        _BUSINESS_RULE_ID_DETAIL = ""
        _BUSINESS_RULE_NAME_DETAIL = ""
        _SQL_COMMAND_DETAIL = ""
        _CONNECTION_NAME_DETAIL = ""
        _CONNECTION_STRING_DETAIL = ""
        _QUALITY_INDEX_ID_DETAIL = ""
        _PARAM_HOST_DETAIL = ""
        _PARAM_PORT_DETAIL = ""
        _PARAM_SERVICE_NAME_DETAIL = ""
        _CONNECTION_TYPE_DETAIL = ""
        _CONNECTION_USER_DETAIL = ""
        _CONNECTION_PASSWORD_DETAIL = ""

        _is_transaction = False
        _is_transaction_detail = False
        _is_transaction_detail_delete = False
        _is_new_uuid = False
        _TRANSACTION_ID = ""
        _RECORD_QTY = 0
        if _row_commit == "":
            _row_ = 100

        sql_format_transaction_id = "select transaction_id from dq_transaction where transaction_id = '{" \
                                    "transaction_id}' and status='0' "
        sql_transaction_id = ""
        sql_new_uuid = "select lower(sys_guid()) as uuid from dual"

        sql_format_count = "select count(*) as qty from {0}"
        sql_count = ""

        sql_format_delete_transaction = "delete from DQ_TRANSACTION_DETAIL_VALUE " \
                                        "\n where TRANSACTION_ID='{0}'" \
                                        "\n and RULE_ID='{1}'" \
                                        "\n and BUSINESS_RULE_ID='{2}'" \
                                        "\n and QUALITY_INDEX_ID='{3}'"

        sql_config = ""
        sql_format_config = " SELECT A.RULE_ID, " \
                            "\n A.INGRESS_DATA_ID, " \
                            "\n C.INGRESS_DATA_NAME, " \
                            "\n A.CONNECTION_ID, " \
                            "\n A.DATA_SUBDOMAIN_ID, " \
                            "\n C.SQL_COMMAND AS INGRESS_SQL_COMMAND, " \
                            "\n C.INGRESS_TARGET AS INGRESS_TARGET_TABLE, " \
                            "\n A.REMARK, " \
                            "\n N.CONNECTION_NAME, " \
                            "\n N.PARAM_HOST, " \
                            "\n N.PARAM_PORT, " \
                            "\n N.PARAM_SERVICE_NAME, " \
                            "\n A.CDE_ID, " \
                            "\n E.CDE_NAME, " \
                            "\n N.CONNECTION_TYPE, " \
                            "\n N.DATABASE_USER," \
                            "\n N.DATABASE_PASSWORD," \
                            "\n A.INGRESS_EXISTS_TABLE," \
                            "\n N.CONNECTION_STRING" \
                            "\n FROM DQ_CONFIG_RULE A " \
                            "\n INNER JOIN DQ_CONFIG_RULE_DETAIL B ON (A.RULE_ID = B.RULE_ID) " \
                            "\n LEFT JOIN DQ_CONFIG_INGRESS_DATA C ON (A.INGRESS_DATA_ID = C.INGRESS_DATA_ID) " \
                            "\n LEFT JOIN DQ_CONFIG_CDE E ON (A.CDE_ID = E.CDE_ID)  " \
                            "\n LEFT JOIN DQ_CONFIG_DATA_DOMAIN F ON (A.DATA_SUBDOMAIN_ID = F.ID)  " \
                            "\n LEFT JOIN DWH_CONFIG_CONNECTION N ON (C.CONNECTION_ID = N.ID)  " \
                            "\n WHERE A.RULE_ID = '{rule_id}'"

        sql_config_detail = ""
        sql_format_config_detail = " SELECT B.BUSINESS_RULE_ID," \
                                   "\n D.BUSINESS_RULE_NAME," \
                                   "\n B.SQL_COMMAND," \
                                   "\n N.CONNECTION_NAME," \
                                   "\n N.PARAM_HOST," \
                                   "\n N.PARAM_PORT," \
                                   "\n N.PARAM_SERVICE_NAME," \
                                   "\n N.CONNECTION_TYPE," \
                                   "\n N.DATABASE_USER," \
                                   "\n N.DATABASE_PASSWORD," \
                                   "\n N.CONNECTION_STRING," \
                                   "\n B.QUALITY_INDEX_ID" \
                                   "\n FROM DQ_CONFIG_RULE A" \
                                   "\n INNER JOIN DQ_CONFIG_RULE_DETAIL B ON (A.RULE_ID = B.RULE_ID)" \
                                   "\n LEFT JOIN DQ_CONFIG_BUSINESS_RULE D ON (B.BUSINESS_RULE_ID = D.BUSINESS_RULE_ID)" \
                                   "\n LEFT JOIN DWH_CONFIG_CONNECTION N ON (A.CONNECTION_ID = N.ID)" \
                                   "\n WHERE A.RULE_ID = '{rule_id}'"

        sql_format_transaction = "Merge Into DQ_TRANSACTION t" \
                                 "\n Using (" \
                                 "\n select " \
                                 "\n  '{0}' as TRANSACTION_ID" \
                                 "\n ,'{1}' as RULE_ID " \
                                 "\n ,'{2}' as TARGET_TABLE " \
                                 "\n , {3}  as RECORD_QTY " \
                                 "\n , {4}  as STAT_ROWS " \
                                 "\n , {5}  as STAT_FILES " \
                                 "\n ,'{6}' as STAT_SIZE " \
                                 "\n ,'{7}' as STAT_LOCATION " \
                                 "\n ,'{8}' as USER_BY " \
                                 "\n ,to_date('{9}','dd/MM/yyyy HH24:mi:ss') as start_date " \
                                 "\n ,to_date('{10}','dd/MM/yyyy HH24:mi:ss') as end_date  " \
                                 "\n ,sysdate as TM " \
                                 "\n from dual ) s " \
                                 "\n on (t.transaction_id=s.transaction_id)" \
                                 "\n when Matched Then" \
                                 "\n UPDATE set  " \
                                 "\n t.RECORD_QTY       = s.RECORD_QTY " \
                                 "\n ,t.STAT_ROWS       = s.STAT_ROWS " \
                                 "\n ,t.STAT_FILES      = s.STAT_FILES " \
                                 "\n ,t.STAT_SIZE       = s.STAT_SIZE " \
                                 "\n ,t.STAT_LOCATION   = s.STAT_LOCATION " \
                                 "\n ,t.START_DATE      = s.START_DATE " \
                                 "\n ,t.END_DATE        = s.END_DATE " \
                                 "\n ,t.UPD_TM          = s.TM " \
                                 "\n ,t.UPD_BY          = s.USER_BY " \
                                 "\n when Not Matched Then" \
                                 "\n Insert (t.TRANSACTION_ID, t.RULE_ID, t.TARGET_TABLE, t.RECORD_QTY, t.STAT_ROWS, " \
                                 "t.STAT_FILES, t.STAT_SIZE, t.STAT_LOCATION,t.START_DATE,t.END_DATE, t.PPN_BY, t.PPN_TM)" \
                                 "\n Values (s.TRANSACTION_ID, s.RULE_ID, s.TARGET_TABLE, s.RECORD_QTY, s.STAT_ROWS, " \
                                 "s.STAT_FILES, s.STAT_SIZE, s.STAT_LOCATION,s.START_DATE,s.END_DATE, s.USER_BY, s.TM) "

        sql_format_transaction_detail = "Merge Into DQ_TRANSACTION_DETAIL t" \
                                        "\n Using (" \
                                        "\n select " \
                                        "\n  '{0}' as TRANSACTION_ID" \
                                        "\n ,'{1}' as RULE_ID " \
                                        "\n ,'{2}' as BUSINESS_RULE_ID " \
                                        "\n , {3}  as QUALITY_INDEX_ID " \
                                        "\n ,'{4}' as SQL_COMMAND " \
                                        "\n , {5}  as RECORD_QTY " \
                                        "\n , {6}  as VALIDATE_RECORD_QTY " \
                                        "\n ,'{7}' as USER_BY " \
                                        "\n ,to_date('{8}','dd/MM/yyyy HH24:mi:ss') as start_date " \
                                        "\n ,to_date('{9}','dd/MM/yyyy HH24:mi:ss') as end_date  " \
                                        "\n ,sysdate as TM " \
                                        "\n from dual ) s " \
                                        "\n on (" \
                                        "\n t.transaction_id=s.transaction_id" \
                                        "\n and t.RULE_ID=s.RULE_ID" \
                                        "\n and t.BUSINESS_RULE_ID=s.BUSINESS_RULE_ID" \
                                        "\n and t.QUALITY_INDEX_ID=s.QUALITY_INDEX_ID" \
                                        "\n )" \
                                        "\n when Matched Then" \
                                        "\n UPDATE set  " \
                                        "\n t.RECORD_QTY            = s.RECORD_QTY " \
                                        "\n ,t.VALIDATE_RECORD_QTY  = s.VALIDATE_RECORD_QTY " \
                                        "\n ,t.SQL_COMMAND          = s.SQL_COMMAND " \
                                        "\n ,t.START_DATE           = s.START_DATE " \
                                        "\n ,t.END_DATE             = s.END_DATE " \
                                        "\n ,t.UPD_TM               = s.TM " \
                                        "\n ,t.UPD_BY               = s.USER_BY " \
                                        "\n when Not Matched Then" \
                                        "\n Insert (" \
                                        "t.TRANSACTION_ID" \
                                        ",t.RULE_ID" \
                                        ",t.BUSINESS_RULE_ID " \
                                        ",t.QUALITY_INDEX_ID " \
                                        ",t.SQL_COMMAND" \
                                        ",t.RECORD_QTY" \
                                        ",t.START_DATE" \
                                        ",t.END_DATE" \
                                        ", t.PPN_BY" \
                                        ", t.PPN_TM)" \
                                        "\n Values (" \
                                        "s.TRANSACTION_ID" \
                                        ", s.RULE_ID" \
                                        ", s.BUSINESS_RULE_ID" \
                                        ", s.QUALITY_INDEX_ID " \
                                        ", s.SQL_COMMAND " \
                                        ", s.RECORD_QTY " \
                                        ", s.START_DATE " \
                                        ", s.END_DATE " \
                                        ", s.USER_BY" \
                                        ", s.TM) "

        sql_format_transaction_value = "Insert Into DQ_TRANSACTION_DETAIL_VALUE " \
                                       "\n  (" \
                                       "TRANSACTION_ID " \
                                       ",RULE_ID " \
                                       ",BUSINESS_RULE_ID " \
                                       ",QUALITY_INDEX_ID " \
                                       ",ROW_SEQ " \
                                       ",COL_SEQ " \
                                       ",COLUMN_NAME " \
                                       ",COLUMN_VALUE " \
                                       ",COLUMN_TYPE " \
                                       ",PPN_BY " \
                                       ") " \
                                       "\n values " \
                                       "\n (" \
                                       ":TRANSACTION_ID " \
                                       ",:RULE_ID " \
                                       ",:BUSINESS_RULE_ID " \
                                       ",:QUALITY_INDEX_ID " \
                                       ",:ROW_SEQ " \
                                       ",:COL_SEQ " \
                                       ",:COLUMN_NAME " \
                                       ",:COLUMN_VALUE " \
                                       ",:COLUMN_TYPE " \
                                       ",:PPN_BY " \
                                       ") "


        sql_config = sql_format_config.format(rule_id=rule_id)
        sql_config_detail = sql_format_config_detail.format(rule_id=rule_id)
        dsn_tns_config = cx.makedsn(host, port, service_name=service)
        connect_config = cx.connect(user, password, dsn=dsn_tns_config)
        if connect_config:
            if transaction_id != "":
                sql_transaction_id = sql_format_transaction_id.format(transaction_id=transaction_id)
                print(sql_transaction_id)
                cursor_uuid = connect_config.cursor()
                cursor_uuid.execute(sql_transaction_id)
                item_uuid = cursor_uuid.fetchone()
                if len(item_uuid) > 0:
                    _TRANSACTION_ID = str(item_uuid[0])
                    _is_new_uuid = False
            if _TRANSACTION_ID == "":
                cursor_new_uuid = connect_config.cursor()
                cursor_new_uuid.execute(sql_new_uuid)
                item_new_uuid = cursor_new_uuid.fetchone()
                if len(item_new_uuid) > 0:
                    _TRANSACTION_ID = str(item_new_uuid[0])
                    _is_new_uuid = True

            print("transaction id = {0} type = {1}".format(_TRANSACTION_ID, str(_is_new_uuid)))

            cursor_config = connect_config.cursor()
            cursor_config.execute(sql_config)
            item_config = cursor_config.fetchone()
            if len(item_config) > 0:
                _RULE_ID = str(item_config[0])
                _INGRESS_DATA_ID = str(item_config[1])
                _INGRESS_DATA_NAME = str(item_config[2])
                _INGRESS_CONNECTION_ID = str(item_config[3])
                _DATA_SUBDOMAIN_ID = str(item_config[4])
                _INGRESS_SQL_COMMAND = str(item_config[5])
                _INGRESS_TARGET_TABLE = str(item_config[6])
                _REMARK = str(item_config[7])
                _INGRESS_CONNECTION_NAME = str(item_config[8])
                _INGRESS_PARAM_HOST = str(item_config[9])
                _INGRESS_PARAM_PORT = str(item_config[10])
                _INGRESS_PARAM_SERVICE_NAME = str(item_config[11])
                _CDE_ID = str(item_config[12])
                _CDE_NAME = str(item_config[13])
                _INGRESS_CONNECTION_TYPE = str(item_config[14])
                _INGRESS_PARAM_USER = str(item_config[15])
                _INGRESS_PARAM_PASSWORD = str(item_config[16])
                _INGRESS_EXISTS_TABLE = str(item_config[17])
                _INGRESS_CONNECTION_STRING = str(item_config[18])

                is_ingress_exists_table = False
                is_ingress_exec_table = True
                if connect_config:
                    print(_INGRESS_CONNECTION_TYPE)
                    if _INGRESS_CONNECTION_TYPE != "":
                        if _INGRESS_CONNECTION_TYPE.lower() == "hadoop":
                            sql_exists_table = "describe  {0}"
                            if _INGRESS_CONNECTION_STRING != "":
                                data_ingress = hadoop.connect(_INGRESS_CONNECTION_STRING, autocommit=True)
                        elif _INGRESS_CONNECTION_TYPE.lower() == "oracle":
                            sql_exists_table = "describe {0}"
                            dsn_tns_ingress = cx.makedsn(_INGRESS_PARAM_HOST, _INGRESS_PARAM_PORT,
                                                         _INGRESS_PARAM_SERVICE_NAME)
                            data_ingress = cx.connect(_INGRESS_PARAM_USER, _INGRESS_PARAM_PASSWORD,
                                                      dsn=dsn_tns_ingress)

                        if data_ingress and _INGRESS_SQL_COMMAND != "":
                            print(_INGRESS_EXISTS_TABLE)
                            print(_INGRESS_SQL_COMMAND)
                            if _INGRESS_EXISTS_TABLE != "1":
                                try:
                                    cursor_rule = data_ingress.cursor()
                                    cursor_rule.execute(_INGRESS_SQL_COMMAND)
                                    is_ingress_exec_table = True
                                except Exception as e:
                                    print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
                                    is_ingress_exec_table = False

                            if is_ingress_exec_table:
                                try:
                                    cursor_exists_table = data_ingress.cursor()
                                    cursor_exists_table.execute(sql_exists_table.format(_INGRESS_TARGET_TABLE))
                                    item_exists_table = cursor_exists_table.fetchone()
                                    is_exists_table = True
                                except Exception as e:
                                    print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
                                    is_exists_table = False

                            if is_exists_table:
                                sql_format_stat = "show table stats {0} "
                                sql_stat = sql_format_stat.format(_INGRESS_TARGET_TABLE)
                                if sql_stat != "":
                                    cursor_table_stat = data_ingress.cursor()
                                    cursor_table_stat.execute(sql_stat)
                                    item_stat = cursor_table_stat.fetchone()
                                    if len(item_stat) > 0:
                                        _STAT_ROWS = str(item_stat[0])
                                        _STAT_FILES = str(item_stat[1])
                                        _STAT_SIZE = str(item_stat[2])
                                        _STAT_BYTES = str(item_stat[3])
                                        _STAT_REPLICATION = str(item_stat[4])
                                        _STAT_FORMAT = str(item_stat[5])
                                        _STAT_INCREMENTAL = str(item_stat[6])
                                        _STAT_LOCATION = str(item_stat[7])
                                sql_count = sql_format_count.format(_INGRESS_TARGET_TABLE)
                                if sql_count != "":
                                    t_start = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                    cursor_table_count = data_ingress.cursor()
                                    cursor_table_count.execute(sql_count)
                                    item_count = cursor_table_count.fetchone()
                                    if len(item_count) > 0:
                                        _RECORD_QTY = item_count[0]
                                    t_end = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                sql_transaction = sql_format_transaction.format(_TRANSACTION_ID, _RULE_ID,
                                                                                _INGRESS_TARGET_TABLE, _RECORD_QTY,
                                                                                _STAT_ROWS, _STAT_FILES, _STAT_SIZE,
                                                                                _STAT_LOCATION, 'Pornc25',t_start,t_end)
                                try:
                                    print(sql_transaction)
                                    cursor_transaction = connect_config.cursor()
                                    cursor_transaction.execute(sql_transaction)
                                    connect_config.commit()
                                    _is_transaction = True
                                except Exception as e:
                                    print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
                                    _is_transaction = False

                                if _is_transaction:
                                    cursor_config_detail = connect_config.cursor()
                                    cursor_config_detail.execute(sql_config_detail)
                                    result_config_detail = cursor_config_detail.fetchall()
                                    if len(result_config_detail) > 0:
                                        for item_config_dt in result_config_detail:
                                            _BUSINESS_RULE_ID_DETAIL = str(item_config_dt[0])
                                            _BUSINESS_RULE_NAME_DETAIL = str(item_config_dt[1])
                                            _SQL_COMMAND_DETAIL = str(item_config_dt[2])
                                            _CONNECTION_NAME_DETAIL = str(item_config_dt[3])
                                            _PARAM_HOST_DETAIL = str(item_config_dt[4])
                                            _PARAM_PORT_DETAIL = str(item_config_dt[5])
                                            _PARAM_SERVICE_NAME_DETAIL = str(item_config_dt[6])
                                            _CONNECTION_TYPE_DETAIL = str(item_config_dt[7])
                                            _CONNECTION_USER_DETAIL = str(item_config_dt[8])
                                            _CONNECTION_PASSWORD_DETAIL = str(item_config_dt[9])
                                            _CONNECTION_STRING_DETAIL = str(item_config_dt[10])
                                            _QUALITY_INDEX_ID_DETAIL = str(item_config_dt[11])

                                            if _CONNECTION_TYPE_DETAIL != "":
                                                if _CONNECTION_TYPE_DETAIL.lower() == "hadoop":
                                                    if _CONNECTION_STRING_DETAIL != "":
                                                        data_dq = hadoop.connect(_CONNECTION_STRING_DETAIL,
                                                                                 autocommit=True)
                                                elif _CONNECTION_TYPE_DETAIL.lower() == "oracle":
                                                    dsn_tns_dq = cx.makedsn(_PARAM_HOST_DETAIL, _PARAM_PORT_DETAIL,
                                                                            _PARAM_SERVICE_NAME_DETAIL)
                                                    data_dq = cx.connect(_CONNECTION_USER_DETAIL, _CONNECTION_PASSWORD_DETAIL,
                                                                         dsn=dsn_tns_dq)
                                                if data_dq:
                                                    t_start_dt = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                                    print(_SQL_COMMAND_DETAIL)
                                                    cursor_dq_detail = data_dq.cursor()
                                                    cursor_dq_detail.execute(_SQL_COMMAND_DETAIL)
                                                    result_dq_detail = cursor_dq_detail.fetchall()
                                                    _RECORD_QTY_DETAIL = 0
                                                    if len(result_dq_detail) > 0:
                                                        _RECORD_QTY_DETAIL = len(result_dq_detail)
                                                        _VALIDATE_RECORD_QTY_DETAIL = 0
                                                        t_end_dt = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                                        print(
                                                            '*************** validate {0} record ************** '.format(
                                                                len(result_dq_detail)))

                                                        sql_transaction_detail = sql_format_transaction_detail.format(
                                                            _TRANSACTION_ID
                                                            , _RULE_ID
                                                            , _BUSINESS_RULE_ID_DETAIL
                                                            , _QUALITY_INDEX_ID_DETAIL
                                                            , _SQL_COMMAND_DETAIL.replace("'", "''")
                                                            , _RECORD_QTY_DETAIL
                                                            , _VALIDATE_RECORD_QTY_DETAIL
                                                            , 'Pornc25',t_start_dt,t_end_dt)
                                                        try:
                                                            print(sql_transaction_detail)
                                                            cursor_transaction_detail = connect_config.cursor()
                                                            cursor_transaction_detail.execute(sql_transaction_detail)
                                                            connect_config.commit()
                                                            _is_transaction_detail = True
                                                        except Exception as e:
                                                            print('Error! Code: {c}, Message, {m}'.format(
                                                                c=type(e).__name__, m=str(e)))
                                                            _is_transaction_detail = False

                                                        if _is_transaction_detail:
                                                            sql_transaction_value_delete = sql_format_delete_transaction.format(
                                                                _TRANSACTION_ID, _RULE_ID, _BUSINESS_RULE_ID_DETAIL,
                                                                _QUALITY_INDEX_ID_DETAIL)

                                                            try:
                                                                print(sql_transaction_value_delete)

                                                                #field_map = fields(cursor_transaction_value_del)
                                                                cursor_transaction_value_del = connect_config.cursor()
                                                                cursor_transaction_value_del.execute(
                                                                    sql_transaction_value_delete)
                                                                connect_config.commit()
                                                                _is_transaction_detail_delete = True
                                                            except Exception as e:
                                                                print('Error! Code: {c}, Message, {m}'.format(
                                                                    c=type(e).__name__, m=str(e)))
                                                                _is_transaction_detail_delete = False
                                                            if _is_transaction_detail_delete:
                                                                sql_format_select_add_value = "SELECT " \
                                                                                              " '{0}' as TRANSACTION_ID" \
                                                                                              ",'{1}' as RULE_ID" \
                                                                                              ",'{2}' as BUSINESS_RULE_ID" \
                                                                                              ", {3} as QUALITY_INDEX_ID" \
                                                                                              ", {4} as ROW_SEQ" \
                                                                                              ", {5} as COL_SEQ" \
                                                                                              ",'{6}' as COLUMN_NAME" \
                                                                                              ",'{7}' as COLUMN_VALUE" \
                                                                                              ",'{8}' as COLUMN_TYPE" \
                                                                                              ",'{9}' as PPN_BY" \
                                                                                              ",sysdate as PPN_TM" \
                                                                                              " from dual"
                                                                iseq_row = 0
                                                                iseq_col = 0
                                                                data = []
                                                                for item_dq_detail in result_dq_detail:
                                                                    _value_type = ""
                                                                    _value_name = ""
                                                                    _value_detail = ""
                                                                    iseq_row += 1
                                                                    iseq_col = 0
                                                                    for column_dp_detail in item_dq_detail:
                                                                        #item_dq_detail[field_map[column_dp_detail]]
                                                                        iseq_col += 1
                                                                        _value_name = str(column_dp_detail)
                                                                        _value_detail = str(column_dp_detail)
                                                                        _value_type = str(type(column_dp_detail))
                                                                        item = (_TRANSACTION_ID, _RULE_ID,
                                                                                _BUSINESS_RULE_ID_DETAIL,
                                                                                _QUALITY_INDEX_ID_DETAIL,
                                                                                str(iseq_row),
                                                                                str(iseq_col),
                                                                                _value_name, _value_detail,
                                                                                _value_type, "Pornc25")

                                                                        data.append(item)
                                                                        if iseq_row % int(_row_commit) == 0:
                                                                            try:
                                                                                ddl_command = sql_format_transaction_value
                                                                                print(ddl_command)
                                                                                with cx.connect(_user_db, _password_db,
                                                                                                dsn_tns_config) as connection:
                                                                                    with connection.cursor() as cursor:
                                                                                        cursor.executemany(ddl_command,
                                                                                                           data)
                                                                                        connection.commit()
                                                                            except Exception as e:
                                                                                print(
                                                                                    'Error! Code: {c}, Message, {m}'.format(
                                                                                        c=type(e).__name__, m=str(e)))
                                                                            finally:
                                                                                data = []
                                                                                gc.collect()
                                                                try:
                                                                    ddl_command = sql_format_transaction_value
                                                                    print(data)
                                                                    with cx.connect(_user_db, _password_db,
                                                                                    dsn_tns_config) as connection:
                                                                        with connection.cursor() as cursor:
                                                                            cursor.executemany(ddl_command,
                                                                                               data)
                                                                            connection.commit()
                                                                except Exception as e:
                                                                    print(
                                                                        'Error! Code: {c}, Message, {m}'.format(
                                                                            c=type(e).__name__, m=str(e)))
                                                                finally:
                                                                    data = []
                                                                    gc.collect()

    except Exception as e:
        print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    os.environ["APP_HOME"] = os.path.dirname(os.path.realpath(__file__))
    os.environ["APP_PID"] = str(os.getpid())
    if "CDE" in os.environ:
        _cde_id = os.environ["CDE"]
    else:
        _cde_id = ""
    if "DB_HOST" in os.environ:
        _host_db = os.environ["DB_HOST"]
    else:
        _host_db = "dbdwhdv1"
    if "DB_PORT" in os.environ:
        _port_db = os.environ["DB_PORT"]
    else:
        _port_db = "1550"
    if "DB_SERVICE" in os.environ:
        _service_db = os.environ["DB_SERVICE"]
    else:
        _service_db = "TEDWDEV"
    if "UD" in os.environ:
        _user_db = os.environ["USER_DB"]
    else:
        _user_db = "TEDWBORAPPO"
    if "PD" in os.environ:
        _password_db = os.environ["PASSWORD"]
    else:
        _password_db = "bortedw123"
    if "RULE_ID" in os.environ:
        _rule_id = os.environ["RULE_ID"]
    else:
        _rule_id = "d5ff0698288d2d10e05329da13ac5e61" #"d6128c03b9927311e05329da13ac6980"
    if "ROW_COMMIT" in os.environ:
        _row_commit = os.environ["RULE_ID"]
    else:
        _row_commit = "100"
    if "TRANSACTION_ID" in os.environ:
        _transaction_id = os.environ["TRANSACTION_ID"]
    else:
        _transaction_id = ""
    process(_host_db, _port_db, _service_db, _user_db, _password_db, _rule_id, _transaction_id, _row_commit)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
