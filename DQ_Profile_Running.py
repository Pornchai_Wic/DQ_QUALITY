import os
import sys
import argparse
import datetime

import gc
import cx_Oracle as cx
import pyodbc as hadoop
import pytz
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from datetime import datetime, timedelta
import time

import pandas as pd
import json

from elasticsearch.connection import create_ssl_context
import urllib3
import ssl
from cryptography.fernet import Fernet, InvalidToken
import logging
import sys

key_api = "8zNU418HIcCijV_KPfCm-OwNsVKGhMOxtl906nyzzmM="


def decryption(key: None, param: None):
    cipher_suite = None
    try:
        f = Fernet(key)
    except InvalidToken:
        f = None
    if not f is None:
        try:
            decrypted_bytes = f.decrypt(param.encode("utf-8"))
        except InvalidToken:
            decrypted_bytes = None  # TODO(kmullins): Shall we log this case? Is it expected?
        if not decrypted_bytes is None:
            decrypted_string = decrypted_bytes.decode()  # bytes -> str
        else:
            decrypted_string = "encryption is not support"
    else:
        key = "key is not support"
    return decrypted_string


tz = pytz.timezone('Asia/Bangkok')


def fields(cursor):
    """ Given a DB API 2.0 cursor object that has been executed, returns
    a dictionary that maps each field name to a column index; 0 and up. """
    results = {}
    column = 0
    for d in cursor.description:
        results[d[0]] = column
        column = column + 1

    return results


def target_elasticsearch(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT, _TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                         _TARGET_STATIC, _TARGET_FIELD_STATIC, array_target):
    urllib3.disable_warnings()

    host = _TARGET_PARAM_HOST
    port = _TARGET_PARAM_PORT
    user = _TARGET_DATABASE_USER
    password = _TARGET_DATABASE_PASSWORD
    now = datetime.today()

    ssl_context = create_ssl_context(cafile='cacert-esd.pem')
    ssl_context.check_hostname = False
    ssl_context.verify_mode = ssl.CERT_OPTIONAL
    es = Elasticsearch(hosts=[{'host': host, 'port': _TARGET_PARAM_PORT}],
                       scheme="https",
                       verify_certs=True,
                       ssl_context=ssl_context,
                       http_auth=(user, password))
    if _TARGET_FIELD_STATIC == "":
        document_id = str(now.strftime('%Y-%m-%d'))
    else:
        document_id = _TARGET_FIELD_STATIC

    index = _TARGET_STATIC + document_id
    print(index)
    doc_list = []
    now = datetime.today()
    now_timestatmp = now.strftime('%Y-%m-%dT%H:%M:%S') + ('-%02d' % (now.microsecond / 10000))
    ind = 0
    topic_name = ""
    if len(array_target) > 0:
        for item_target in array_target:
            doc_list += [item_target]

        print(index)
        if len(doc_list) > 0:
            try:
                print(
                    "\nAttempting to index =" + index + " the list of docs using helpers.bulk(" + str(
                        len(doc_list)) + ")")

                resp = helpers.bulk(es, doc_list, index=index, doc_type='_doc')
                print("helpers.bulk " + now_timestatmp + " = ", resp)
            except Exception as e:
                print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
            finally:
                es.close()


def is_connection_validate(HOST, PORT, USER, PASS):
    return len(HOST) > 0 and len(PORT) > 0 and len(USER) > 0 and len(PASS) > 0


def process(host, port, service, user, password):
    global _is_transaction_detail_agg
    stdout_fileno = sys.stdout
    stderr_fileno = sys.stderr
    try:

        is_ingress_exists_table = False
        is_ingress_exec_table = False

        _is_transaction = False
        _is_transaction_detail = False
        _is_transaction_detail_delete = False
        _is_new_uuid = False
        _TRANSACTION_ID = ""
        _RECORD_QTY = 0
        sql_format_profile_id = "select profile_id,ingress_data_type_run from dq_profile where profile_id = '{profile_id}' and status='0' "
        sql_transaction_id = ""
        sql_new_uuid = "select lower(sys_guid()) as uuid from dual"
        sql_format_count_is_null = "select count(*) as qty from {0} and {1} is null "
        sql_format_count_is_null_table = "select count(*) as qty from {0} where {1} is null "
        sql_format_count_sql_is_null = "select count(*) as qty from ({0}) xx  where {1} is null  "
        sql_format_count_sql = "select count(*) as qty from ({0}) xx     "

        sql_format_count = "select count(*) as qty {1} from {0}"
        sql_count = ""

        sql_format_delete_transaction = "delete from DQ_PROFILE_DETAIL " \
                                        "\n where PROFILE_ID='{0}'"

        sql_format_delete_transaction_value = "delete from DQ_PROFILE_DETAIL_VALUE " \
                                              "\n where PROFILE_ID='{0}'"

        sql_schedule = " SELECT " \
                       "\n   A.INGRESS_DATA_ID " \
                       "\n   ,A.PROFILE_ID " \
                       "\n   FROM DQ_PROFILE A " \
                       "\n   INNER JOIN DQ_CONFIG_INGRESS_DATA B ON (A.INGRESS_DATA_ID=B.INGRESS_DATA_ID) " \
                       "\n   WHERE  A.STATUS_RUNNING = '0'" \
                       "\n   AND A.STATUS = '0'" \
                       "\n   AND B.STATUS = '0'" \
                       "\n   AND B.IS_TYPE_RUNNING = '0'"

        sql_config = ""
        sql_format_config = " SELECT " \
                            "\n   A.INGRESS_DATA_ID, " \
                            "\n   A.INGRESS_DATA_NAME, " \
                            "\n   A.INGRESS_DATA_DESC, " \
                            "\n   A.CONNECTION_ID, " \
                            "\n   A.CONNECTION_NAME, " \
                            "\n   A.SQL_COMMAND, " \
                            "\n   case when '{type_run}' = '0' then A.INGRESS_TARGET || ' ' ||   nvl(A.INGRESS_TARGET_FILTER,'') when '{type_run}' = '1' then A.INGRESS_TARGET  when '{type_run}' = '2' then A.SQL_COMMAND end INGRESS_TARGET, " \
                            "\n   N.CONNECTION_NAME, " \
                            "\n   N.PARAM_HOST, " \
                            "\n   N.PARAM_PORT, " \
                            "\n   N.PARAM_SERVICE_NAME, " \
                            "\n   N.CONNECTION_TYPE, " \
                            "\n   N.DATABASE_USER, " \
                            "\n   N.DATABASE_PASSWORD, " \
                            "\n   N.CONNECTION_STRING, " \
                            "\n   case when '{type_run}' = '2' then 1 else 0 end USED_IS_COMMAND, " \
                            "\n   nvl(A.FIELD_DATA_AS_OF,'-')  as FIELD_DATA_AS_OF," \
                            "\n   A.INGRESS_TARGET as INGRESS_TARGET_SPECIFIC" \
                            "\n   FROM DQ_CONFIG_INGRESS_DATA A " \
                            "\n   INNER JOIN DWH_CONFIG_CONNECTION  N ON(A.CONNECTION_ID = N.ID) " \
                            "\n   WHERE A.INGRESS_DATA_ID = '{ingress_id}'" \
                            "\n   AND A.IS_TYPE_RUNNING = '0'"

        sql_config_detail = ""
        sql_format_config_detail = "SELECT A.DATA_TYPE," \
                                   "\n A.PROFILE_ATTRIBUTE_ID," \
                                   "\n B.PROFILE_ATTRIBUTE_NAME," \
                                   "\n B.PROFILE_ATTRIBUTE_DESC," \
                                   "\n B.AGGREGATE_FUNCTION," \
                                   "\n B.VAR_FIELD," \
                                   "\n B.VAR_TABLE," \
                                   "\n B.AGGREGATE_FUNCTION_SQL" \
                                   "\n FROM DQ_CONFIG_PROFILE_TYPE_MAPPING A" \
                                   "\n INNER JOIN DQ_CONFIG_PROFILE_ATTRIBUTE B ON (A.PROFILE_ATTRIBUTE_ID = B.PROFILE_ATTRIBUTE_ID)" \
                                   "\n WHERE A.CONNECTION_ID = {connection_id} AND DATA_TYPE = '{data_type}'" \
                                   "\n ORDER BY 1,2"

        sql_format_transaction = "Merge Into DQ_PROFILE t" \
                                 "\n Using (" \
                                 "\n select " \
                                 "\n  '{0}' as PROFILE_ID" \
                                 "\n ,'{1}' as INGRESS_DATA_ID " \
                                 "\n , {2}  as RECORD_QTY " \
                                 "\n ,'{3}' as USER_BY " \
                                 "\n ,to_date('{4}','dd/MM/yyyy HH24:mi:ss') as start_date " \
                                 "\n ,to_date('{5}','dd/MM/yyyy HH24:mi:ss') as end_date  " \
                                 "\n ,'{6}' as DATA_AS_OF " \
                                 "\n ,'{7}' as STATUS_RUNNING " \
                                 "\n ,sysdate as TM " \
                                 "\n from dual ) s " \
                                 "\n on (t.PROFILE_ID=s.PROFILE_ID)" \
                                 "\n when Matched Then" \
                                 "\n UPDATE set  " \
                                 "\n t.RECORD_QTY       = s.RECORD_QTY " \
                                 "\n ,t.DATA_AS_OF       = s.DATA_AS_OF " \
                                 "\n ,t.START_DATE      = s.START_DATE " \
                                 "\n ,t.END_DATE        = s.END_DATE " \
                                 "\n ,t.UPD_TM          = s.TM " \
                                 "\n ,t.UPD_BY          = s.USER_BY " \
                                 "\n ,t.STATUS_RUNNING          = s.STATUS_RUNNING " \
                                 "\n when Not Matched Then" \
                                 "\n Insert (t.PROFILE_ID, t.INGRESS_DATA_ID, t.RECORD_QTY,t.STATUS_RUNNING,t.DATA_AS_OF, t.START_DATE, t.END_DATE, t.PPN_BY, t.PPN_TM)" \
                                 "\n Values (s.PROFILE_ID, s.INGRESS_DATA_ID, s.RECORD_QTY,s.STATUS_RUNNING,s.DATA_AS_OF, s.START_DATE, s.END_DATE, s.USER_BY, s.TM) "

        sql_format_transaction_failed = "Update DQ_PROFILE t set t.STATUS_RUNNING ='3',t.ERROR_MSG='{1}' where t.PROFILE_ID='{0}'"

        sql_format_transaction_detail = "Merge Into DQ_PROFILE_DETAIL t" \
                                        "\n Using (" \
                                        "\n select " \
                                        "\n  '{0}' as PROFILE_ID" \
                                        "\n ,'{1}' as SEQ " \
                                        "\n ,'{2}' as COLUMN_NAME " \
                                        "\n ,'{3}' as COLUMN_TYPE " \
                                        "\n ,'{4}' as COLUMN_IS_NULL " \
                                        "\n , {5}  as RECORD_NULL_QTY " \
                                        "\n ,'{6}' as USER_BY " \
                                        "\n ,to_date('{7}','dd/MM/yyyy HH24:mi:ss') as start_date " \
                                        "\n ,to_date('{8}','dd/MM/yyyy HH24:mi:ss') as end_date  " \
                                        "\n ,'{9}' as SQL_COMMAND_NULL_QTY " \
                                        "\n ,'{10}' as PERCENT_NULL_RECORD " \
                                        "\n ,sysdate as TM " \
                                        "\n from dual ) s " \
                                        "\n on (" \
                                        "\n t.PROFILE_ID=s.PROFILE_ID" \
                                        "\n and t.SEQ=s.SEQ" \
                                        "\n and upper(t.COLUMN_NAME)=upper(s.COLUMN_NAME)" \
                                        "\n )" \
                                        "\n when Matched Then" \
                                        "\n UPDATE set  " \
                                        "\n t.RECORD_NULL_QTY      = s.RECORD_NULL_QTY " \
                                        "\n ,t.START_DATE           = s.START_DATE " \
                                        "\n ,t.END_DATE             = s.END_DATE " \
                                        "\n ,t.SQL_COMMAND_NULL_QTY = s.SQL_COMMAND_NULL_QTY " \
                                        "\n ,t.PERCENT_NULL_RECORD  = s.PERCENT_NULL_RECORD " \
                                        "\n ,t.UPD_TM               = s.TM " \
                                        "\n ,t.UPD_BY               = s.USER_BY " \
                                        "\n when Not Matched Then" \
                                        "\n Insert (t.PROFILE_ID, t.SEQ, t.COLUMN_NAME, t.COLUMN_TYPE, t.COLUMN_IS_NULL, t.RECORD_NULL_QTY,t.PERCENT_NULL_RECORD, t.SQL_COMMAND_NULL_QTY, t.START_DATE, t.END_DATE, t.PPN_BY, t.PPN_TM)" \
                                        "\n Values (s.PROFILE_ID, s.SEQ, s.COLUMN_NAME, s.COLUMN_TYPE, s.COLUMN_IS_NULL, s.RECORD_NULL_QTY,s.PERCENT_NULL_RECORD, s.SQL_COMMAND_NULL_QTY, s.START_DATE, s.END_DATE, s.USER_BY, s.TM)"

        sql_format_transaction_value = "Insert Into DQ_PROFILE_DETAIL_VALUE " \
                                       "\n  (" \
                                       "PROFILE_ID " \
                                       ",SEQ " \
                                       ",PROFILE_ATTRIBUTE_ID " \
                                       ",COLUMN_VALUE " \
                                       ",COLUMN_NAME " \
                                       ",COLUMN_TYPE " \
                                       ",SQL_COMMAND " \
                                       ",PPN_BY " \
                                       ") " \
                                       "\n values " \
                                       "\n (" \
                                       ":PROFILE_ID " \
                                       ",:SEQ " \
                                       ",:PROFILE_ATTRIBUTE_ID " \
                                       ",:COLUMN_VALUE " \
                                       ",:COLUMN_NAME " \
                                       ",:COLUMN_TYPE " \
                                       ",:SQL_COMMAND " \
                                       ",:PPN_BY " \
                                       ") "

        sql_format_delete_group_by = "DELETE FROM DQ_PROFILE_DETAIL_GROUP_VALUE WHERE PROFILE_ID='{0}'"

        sql_format_group_by = "Insert Into DQ_PROFILE_DETAIL_GROUP_VALUE t" \
                              "\n  (" \
                              "\n  PROFILE_ID" \
                              "\n ,SEQ " \
                              "\n ,ROW_SEQ " \
                              "\n ,COLUMN_DATA " \
                              "\n ,COUNT_QTY " \
                              "\n ,COUNT_DISTINCT_QTY " \
                              "\n ,PPN_BY " \
                              "\n ) " \
                              "\n values" \
                              "\n  (" \
                              "\n  :PROFILE_ID" \
                              "\n ,:SEQ " \
                              "\n ,:ROW_SEQ " \
                              "\n ,:COLUMN_DATA " \
                              "\n ,:COUNT_QTY " \
                              "\n ,:COUNT_DISTINCT_QTY " \
                              "\n ,:PPN_BY " \
                              "\n ) "
        _is_transaction_detail_agg = False
        dsn_tns_config = cx.makedsn(host, port, service_name=service)
        connect_config = cx.connect(user, password, dsn=dsn_tns_config)
        if connect_config:
            print(sql_schedule)
            cursor_schedule = connect_config.cursor()
            cursor_schedule.execute(sql_schedule)
            result_schedule = cursor_schedule.fetchall()
            if len(result_schedule) > 0:
                for item_schedule in result_schedule:
                    _input_ingress_id = str(item_schedule[0])
                    _input_profile_id = str(item_schedule[1])
                    print(_input_ingress_id)
                    if _input_profile_id != "":
                        sql_profile_id = sql_format_profile_id.format(profile_id=_input_profile_id)
                        print(sql_profile_id)
                        cursor_uuid = connect_config.cursor()
                        cursor_uuid.execute(sql_profile_id)
                        item_uuid = cursor_uuid.fetchone()
                        if len(item_uuid) > 0:
                            _PROFILE_ID = str(item_uuid[0])
                            _TYPE_RUN = str(item_uuid[1])
                            _is_new_uuid = False
                    if _input_profile_id == "":
                        cursor_new_uuid = connect_config.cursor()
                        cursor_new_uuid.execute(sql_new_uuid)
                        item_new_uuid = cursor_new_uuid.fetchone()
                        if len(item_new_uuid) > 0:
                            _PROFILE_ID = str(item_new_uuid[0])
                            _TYPE_RUN = "0"
                            _is_new_uuid = True

                    print("profile id = {0} type = {1}".format(_PROFILE_ID, str(_is_new_uuid)))
                    sql_config = sql_format_config.format(ingress_id=_input_ingress_id, type_run=_TYPE_RUN)
                    print(sql_config)
                    cursor_config = connect_config.cursor()
                    cursor_config.execute(sql_config)
                    item_config = cursor_config.fetchone()
                    if len(item_config) > 0:
                        _INGRESS_DATA_ID = str(item_config[0])
                        _INGRESS_DATA_NAME = str(item_config[1])
                        _INGRESS_DATA_DESC = str(item_config[2])
                        _INGRESS_CONNECTION_ID = str(item_config[3])
                        _INGRESS_CONNECTION_NAME = str(item_config[4])
                        _INGRESS_SQL_COMMAND = str(item_config[5])
                        _INGRESS_TARGET_TABLE = str(item_config[6])
                        _INGRESS_CONNECTION_NAME = str(item_config[7])
                        _INGRESS_PARAM_HOST = str(item_config[8])
                        _INGRESS_PARAM_PORT = str(item_config[9])
                        _INGRESS_PARAM_SERVICE_NAME = str(item_config[10])
                        _INGRESS_CONNECTION_TYPE = str(item_config[11])
                        _INGRESS_PARAM_USER = str(item_config[12])
                        _INGRESS_PARAM_PASSWORD = str(item_config[13])
                        _INGRESS_CONNECTION_STRING = str(item_config[14])
                        _INGRESS_USED_IS_COMMAND = str(item_config[15])
                        _INGRESS_FIELD_DATA_AS_OF = item_config[16]
                        _INGRESS_TARGET_TABLE_SPECIFIC = str(item_config[17])

                        is_ingress_exists_table = False
                        is_ingress_exec_table = True
                        if connect_config:
                            if _INGRESS_CONNECTION_TYPE != "":
                                if _INGRESS_CONNECTION_TYPE.lower() == "hadoop":
                                    sql_exists_table = "describe {0}"
                                    if _INGRESS_CONNECTION_STRING != "":
                                        data_ingress = hadoop.connect(_INGRESS_CONNECTION_STRING, autocommit=True)
                                elif _INGRESS_CONNECTION_TYPE.lower() == "oracle":
                                    sql_exists_table = "select COLUMN_NAME,DATA_TYPE from ALL_TAB_COLUMNS  where upper(owner ||'.'||table_name)=upper('{0}')"
                                    dsn_tns_ingress = cx.makedsn(_INGRESS_PARAM_HOST, _INGRESS_PARAM_PORT,
                                                                 service_name=_INGRESS_PARAM_SERVICE_NAME)
                                    data_ingress = cx.connect(_INGRESS_PARAM_USER, _INGRESS_PARAM_PASSWORD,
                                                              dsn=dsn_tns_ingress)

                                if data_ingress and _INGRESS_SQL_COMMAND != "":
                                    try:
                                        print(_INGRESS_USED_IS_COMMAND)
                                        if _INGRESS_USED_IS_COMMAND == "1":
                                            try:
                                                print('======================== USED QUERY =======================')
                                                cursor_sql_command = data_ingress.cursor()
                                                cursor_sql_command.execute(_INGRESS_SQL_COMMAND)
                                                result_table = cursor_sql_command.fetchall()
                                                is_exists_table = False
                                            except Exception as e:
                                                print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__,
                                                                                              m=str(e)))
                                                is_exists_table = False
                                        else:
                                            try:
                                                print('======================== TABLE FILTER =======================')
                                                cursor_exists_table = data_ingress.cursor()
                                                cursor_exists_table.execute(
                                                    sql_exists_table.format(_INGRESS_TARGET_TABLE_SPECIFIC))
                                                result_table = cursor_exists_table.fetchall()
                                                is_exists_table = True
                                            except Exception as e:
                                                print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__,
                                                                                              m=str(e)))
                                                is_exists_table = True

                                        if is_exists_table:
                                            field_query_data_as_of = ""
                                            if _INGRESS_FIELD_DATA_AS_OF != "-":
                                                field_query_data_as_of = ",MAX({0}) as MAX_VALUE".format(
                                                    _INGRESS_FIELD_DATA_AS_OF)

                                            sql_count = sql_format_count.format(_INGRESS_TARGET_TABLE,
                                                                                field_query_data_as_of)
                                            _MAX_VALUE = ""
                                            if sql_count != "":
                                                t_start = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                                cursor_table_count = data_ingress.cursor()
                                                cursor_table_count.execute(sql_count)
                                                item_count = cursor_table_count.fetchone()
                                                if len(item_count) > 0:
                                                    _RECORD_QTY = item_count[0]
                                                if len(item_count) > 1:
                                                    _MAX_VALUE = str(item_count[1])
                                                t_end = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                                sql_transaction = sql_format_transaction.format(_PROFILE_ID,
                                                                                                _INGRESS_DATA_ID,
                                                                                                _RECORD_QTY, 'Pornc25',
                                                                                                t_start,
                                                                                                t_end, _MAX_VALUE, "1")

                                            try:
                                                print(sql_transaction)
                                                cursor_transaction = connect_config.cursor()
                                                cursor_transaction.execute(sql_transaction)
                                                connect_config.commit()
                                                _is_transaction = True
                                            except Exception as e:
                                                print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__,
                                                                                              m=str(e)))
                                                _is_transaction = False
                                            iseq_dt = 0
                                            if _is_transaction:
                                                sql_transaction_delete = sql_format_delete_transaction.format(
                                                    _PROFILE_ID)
                                                try:
                                                    print(sql_transaction_delete)

                                                    # field_map = fields(cursor_transaction_value_del)
                                                    cursor_transaction_del = connect_config.cursor()
                                                    cursor_transaction_del.execute(sql_transaction_delete)
                                                    connect_config.commit()
                                                    _is_transaction_detail_delete = True
                                                except Exception as e:
                                                    print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__,
                                                                                                  m=str(e)))
                                                    _is_transaction_detail_delete = False
                                                sql_delete_transaction_value = sql_format_delete_transaction_value.format(
                                                    _PROFILE_ID)
                                                try:
                                                    print(sql_delete_transaction_value)

                                                    # field_map = fields(cursor_transaction_value_del)
                                                    cursor_transaction_del_value = connect_config.cursor()
                                                    cursor_transaction_del_value.execute(sql_delete_transaction_value)
                                                    connect_config.commit()
                                                    _is_transaction_detail_delete_value = True
                                                except Exception as e:
                                                    print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__,
                                                                                                  m=str(e)))
                                                    _is_transaction_detail_delete_value = False
                                            if _is_transaction_detail_delete and _is_transaction_detail_delete_value:
                                                if len(result_table) > 0:
                                                    for item_column in result_table:
                                                        iseq_dt += 1
                                                        dt_column_name = str(item_column[0])
                                                        dt_column_type = str(item_column[1])
                                                        print(dt_column_name)
                                                        print(dt_column_type)
                                                        print(_TYPE_RUN)
                                                        print(_INGRESS_TARGET_TABLE)
                                                        # O WHERE FILTER , 1 NO FILTER
                                                        if _TYPE_RUN == "0":
                                                            sql_count_is_null = sql_format_count_is_null.format(
                                                                _INGRESS_TARGET_TABLE,
                                                                dt_column_name)
                                                        elif _TYPE_RUN == "1":
                                                            sql_count_is_null = sql_format_count_is_null_table.format(
                                                                _INGRESS_TARGET_TABLE_SPECIFIC,
                                                                dt_column_name)
                                                        elif _TYPE_RUN == "2":
                                                            sql_count_is_null = sql_format_count_sql_is_null.format(
                                                                _INGRESS_SQL_COMMAND,
                                                                dt_column_name)

                                                        _RECORD_NULL_DT_QTY = 0
                                                        if data_ingress:
                                                            t_start_dt = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                                            print(sql_count_is_null)
                                                            cursor_count_null = data_ingress.cursor()
                                                            cursor_count_null.execute(sql_count_is_null)
                                                            item_count_null = cursor_count_null.fetchone()
                                                            if len(item_count_null) > 0:
                                                                _RECORD_NULL_DT_QTY = item_count_null[0]
                                                                t_end_dt = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                                            _RECORD_PERCENT = _RECORD_NULL_DT_QTY / _RECORD_QTY
                                                            sql_transaction_detail = sql_format_transaction_detail.format(
                                                                _PROFILE_ID
                                                                , str(iseq_dt)
                                                                , dt_column_name
                                                                , dt_column_type.replace("'", "''")
                                                                , '0', _RECORD_NULL_DT_QTY, 'Pornc25', t_start_dt,
                                                                t_end_dt
                                                                , sql_count_is_null.replace("'", "''"),
                                                                str(round(_RECORD_PERCENT, 2))
                                                            )
                                                            print(sql_transaction_detail)
                                                            try:
                                                                cursor_transaction_detail = connect_config.cursor()
                                                                cursor_transaction_detail.execute(
                                                                    sql_transaction_detail)
                                                                connect_config.commit()
                                                                _is_transaction_detail = True
                                                            except Exception as e:
                                                                print('Error! Code: {c}, Message, {m}'.format(
                                                                    c=type(e).__name__, m=str(e)))
                                                                _is_transaction_detail = False

                                                            if _is_transaction_detail:
                                                                DATATYPE = "STR"
                                                                isINT = str(dt_column_type).lower().find(
                                                                    "int") > -1 or str(
                                                                    dt_column_type).lower().find("decimal") > -1 or str(
                                                                    dt_column_type).lower().find("number") > -1
                                                                isDouble = str(dt_column_type).lower().find(
                                                                    "double") > -1 or str(dt_column_type).lower().find(
                                                                    "float") > -1
                                                                isSTR = str(dt_column_type).lower().find("varchar") > -1
                                                                isDate = str(dt_column_type).lower().find(
                                                                    "date") > -1 or str(
                                                                    dt_column_type).lower().find("time") > -1

                                                                if isINT:
                                                                    DATATYPE = "INT"
                                                                elif isDate:
                                                                    DATATYPE = "DATE"
                                                                elif isDouble:
                                                                    DATATYPE = "DOUBLE"

                                                                sql_config_detail = sql_format_config_detail.format(
                                                                    connection_id=_INGRESS_CONNECTION_ID,
                                                                    data_type=DATATYPE)
                                                                print(sql_config_detail)
                                                                cursor_config_agg = connect_config.cursor()
                                                                cursor_config_agg.execute(sql_config_detail)
                                                                result_agg = cursor_config_agg.fetchall()
                                                                if len(result_agg) > 0:
                                                                    data = []
                                                                    for item_agg in result_agg:
                                                                        _DATA_TYPE = str(item_agg[0])
                                                                        _PROFILE_ATTRIBUTE_ID = str(item_agg[1])
                                                                        _PROFILE_ATTRIBUTE_NAME = str(item_agg[2])
                                                                        _PROFILE_ATTRIBUTE_DESC = str(item_agg[3])
                                                                        _AGGREGATE_FUNCTION = str(item_agg[4])
                                                                        _VAR_FIELD = str(item_agg[5])
                                                                        _VAR_TABLE = str(item_agg[6])
                                                                        _AGGREGATE_FUNCTION_SQL = str(item_agg[7])
                                                                        sql_aggregate = _AGGREGATE_FUNCTION.replace(
                                                                            _VAR_FIELD,
                                                                            dt_column_name).replace(
                                                                            _VAR_TABLE, _INGRESS_TARGET_TABLE)

                                                                        print(sql_aggregate)
                                                                        _RECORD_AGG_VALUE = ""
                                                                        if data_ingress:
                                                                            t_start_agg_dt = datetime.now().strftime(
                                                                                '%d/%m/%Y %H:%M:%S')
                                                                            cursor_count_agg = data_ingress.cursor()
                                                                            cursor_count_agg.execute(sql_aggregate)
                                                                            item_count_agg = cursor_count_agg.fetchone()
                                                                            if len(item_count_agg) > 0:
                                                                                _RECORD_AGG_VALUE = str(
                                                                                    item_count_agg[0])
                                                                                t_end_agg_dt = datetime.now().strftime(
                                                                                    '%d/%m/%Y %H:%M:%S')

                                                                            item = (_PROFILE_ID, iseq_dt,
                                                                                    _PROFILE_ATTRIBUTE_ID,
                                                                                    _RECORD_AGG_VALUE,
                                                                                    dt_column_name,
                                                                                    dt_column_type.replace("'", "''"),
                                                                                    sql_aggregate.replace("'", "''"),
                                                                                    "Pornc25")
                                                                            data.append(item)
                                                                    try:
                                                                        print(data)
                                                                        ddl_command = sql_format_transaction_value
                                                                        print(ddl_command)
                                                                        with cx.connect(_user_db, _password_db,
                                                                                        dsn_tns_config) as connection:
                                                                            with connection.cursor() as cursor:
                                                                                cursor.executemany(ddl_command,
                                                                                                   data)
                                                                                connection.commit()
                                                                                _is_transaction_detail_agg = True
                                                                    except Exception as e:
                                                                        print('Error! Code: {c}, Message, {m}'.format(
                                                                            c=type(e).__name__, m=str(e)))
                                                                        _is_transaction_detail_agg = False

                                                                    finally:
                                                                        data = []
                                                                        gc.collect()

                                                if _is_transaction_detail_agg:
                                                    sql_delete_group_by = sql_format_delete_group_by.format(
                                                        _PROFILE_ID)
                                                    try:
                                                        print(sql_delete_group_by)

                                                        # field_map = fields(cursor_transaction_value_del)
                                                        cursor_group_del = connect_config.cursor()
                                                        cursor_group_del.execute(sql_delete_group_by)
                                                        connect_config.commit()
                                                        _is_group_delete = True
                                                    except Exception as e:
                                                        print(
                                                            'Error! Code: {c}, Message, {m}'.format(c=type(e).__name__,
                                                                                                    m=str(e)))
                                                        _is_group_delete = False
                                                    time.sleep(10)
                                                    sql_format_pull_group_100 = "select  a.seq,b.column_name,b.column_type,b.column_value,a.record_null_qty,pd.record_qty " \
                                                                                " from dq_profile pd " \
                                                                                " inner join dq_profile_detail a on (pd.profile_id=a.profile_id) " \
                                                                                " inner join dq_profile_detail_value b on(a.PROFILE_ID=b.PROFILE_ID and a.COLUMN_NAME=b.COLUMN_NAME) " \
                                                                                " inner join dq_config_profile_attribute c on (b.profile_attribute_id=c.profile_attribute_id and c.profile_attribute_id = 5) " \
                                                                                " where pd.profile_id='{0}' " \
                                                                                " and to_number(b.column_value) <> to_number(pd.record_qty)" \
                                                                                " and to_number(b.column_value) > 1 " \
                                                                                " and (to_number(b.column_value)/to_number(pd.record_qty)) <= 0.001"

                                                    sql_format_count_group = "select {1},count({1}) as qty ,count(distinct {1}) as distinct_qty" \
                                                                             " from {0}" \
                                                                             " group by {1}"
                                                    if connect_config:
                                                        sql_pull_group_100 = sql_format_pull_group_100.format(
                                                            _PROFILE_ID)
                                                        t_start_pull_100 = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                                        cursor_pull_group_100 = connect_config.cursor()
                                                        cursor_pull_group_100.execute(sql_pull_group_100)
                                                        result_pull_group_100 = cursor_pull_group_100.fetchall()
                                                        t_end_pull_100 = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                                        for item_pull_100 in result_pull_group_100:
                                                            _PULL_100_SEQ = item_pull_100[0]
                                                            _PULL_100_COLUMN_NAME = str(item_pull_100[1])
                                                            _PULL_100_COLUMN_TYPE = str(item_pull_100[2])
                                                            _PULL_100_COLUMN_VALUE = str(item_pull_100[3])
                                                            _PULL_100_RECORD_NULL_QTY = item_pull_100[4]
                                                            _PULL_100_RECORD_QTY = item_pull_100[5]

                                                            sql_count_group = sql_format_count_group.format(
                                                                _INGRESS_TARGET_TABLE,
                                                                _PULL_100_COLUMN_NAME)

                                                            t_start_agg_dt_group = datetime.now().strftime(
                                                                '%d/%m/%Y %H:%M:%S')
                                                            cursor_count_group = data_ingress.cursor()
                                                            cursor_count_group.execute(sql_count_group)
                                                            result_count_group = cursor_count_group.fetchall()
                                                            t_end_agg_dt_group = datetime.now().strftime(
                                                                '%d/%m/%Y %H:%M:%S')
                                                            data_dt = []
                                                            if len(result_count_group) > 0:
                                                                iseq_dt_value = 0
                                                                for item_count_agg in result_count_group:
                                                                    iseq_dt_value += 1
                                                                    _FIELD_GROUP_BY = str(item_count_agg[0])
                                                                    _FIELD_COUNT = item_count_agg[1]
                                                                    _FIELD_COUNT_DISTINCT = item_count_agg[2]
                                                                    item_dt_count = (
                                                                        _PROFILE_ID, _PULL_100_SEQ, iseq_dt_value,
                                                                        _FIELD_GROUP_BY,
                                                                        _FIELD_COUNT,
                                                                        _FIELD_COUNT_DISTINCT,
                                                                        "Pornc25")
                                                                    data_dt.append(item_dt_count)
                                                                try:
                                                                    ddl_command_group = sql_format_group_by
                                                                    print(ddl_command_group)
                                                                    with cx.connect(_user_db, _password_db,
                                                                                    dsn_tns_config) as connection:
                                                                        with connection.cursor() as cursor:
                                                                            cursor.executemany(ddl_command_group,
                                                                                               data_dt)
                                                                            connection.commit()

                                                                            _is_transaction_detail_group = True
                                                                except Exception as e:
                                                                    print('Error! Code: {c}, Message, {m}'.format(
                                                                        c=type(e).__name__, m=str(e)))
                                                                    _is_transaction_detail_group = False
                                                                finally:
                                                                    data_dt = []
                                                                    gc.collect()

                                                    sql_format_pull_group_100_over = "select  a.seq,b.column_name,b.column_type,b.column_value,a.record_null_qty,pd.record_qty " \
                                                                                     " from dq_profile pd " \
                                                                                     " inner join dq_profile_detail a on (pd.profile_id=a.profile_id) " \
                                                                                     " inner join dq_profile_detail_value b on(a.PROFILE_ID=b.PROFILE_ID and a.COLUMN_NAME=b.COLUMN_NAME) " \
                                                                                     " inner join dq_config_profile_attribute c on (b.profile_attribute_id=c.profile_attribute_id and c.profile_attribute_id = 5) " \
                                                                                     " where pd.profile_id='{0}' " \
                                                                                     " and to_number(b.column_value) <> to_number(pd.record_qty)" \
                                                                                     " and to_number(b.column_value) > 1 " \
                                                                                     " and (to_number(b.column_value)/to_number(pd.record_qty)) > 0.001"

                                                    if _INGRESS_CONNECTION_TYPE.lower() == "hadoop":
                                                        sql_format_count_group_limit = "select {1},count({1}) as qty ,count(distinct {1}) as distinct_qty" \
                                                                                       " from {0}" \
                                                                                       " group by {1}" \
                                                                                       " order by count({1}) desc" \
                                                                                       " limit 10 "
                                                    elif _INGRESS_CONNECTION_TYPE.lower() == "oracle":
                                                        sql_format_count_group_limit = "select {1},count({1}) as qty ,count(distinct {1}) as distinct_qty" \
                                                                                       " from {0}" \
                                                                                       " where rownum <= 100" \
                                                                                       " group by {1}" \
                                                                                       " order by count({1}) desc"
                                                    if connect_config:
                                                        sql_pull_group_100_over = sql_format_pull_group_100_over.format(
                                                            _PROFILE_ID)
                                                        t_start_pull_100_over = datetime.now().strftime(
                                                            '%d/%m/%Y %H:%M:%S')
                                                        cursor_pull_group_100_over = connect_config.cursor()
                                                        cursor_pull_group_100_over.execute(sql_pull_group_100_over)
                                                        result_pull_group_100_over = cursor_pull_group_100_over.fetchall()
                                                        t_end_pull_100_over = datetime.now().strftime(
                                                            '%d/%m/%Y %H:%M:%S')
                                                        for item_pull_100_over in result_pull_group_100_over:
                                                            _PULL_100_SEQ_over = item_pull_100_over[0]
                                                            _PULL_100_COLUMN_NAME_over = str(item_pull_100_over[1])
                                                            _PULL_100_COLUMN_TYPE_over = str(item_pull_100_over[2])
                                                            _PULL_100_COLUMN_VALUE_over = str(item_pull_100_over[3])
                                                            _PULL_100_RECORD_NULL_QTY_over = item_pull_100_over[4]
                                                            _PULL_100_RECORD_QTY_over = item_pull_100_over[5]

                                                            sql_count_group_limit = sql_format_count_group_limit.format(
                                                                _INGRESS_TARGET_TABLE,
                                                                _PULL_100_COLUMN_NAME_over)

                                                            t_start_agg_dt_group_limit = datetime.now().strftime(
                                                                '%d/%m/%Y %H:%M:%S')
                                                            cursor_count_group_limit = data_ingress.cursor()
                                                            cursor_count_group_limit.execute(sql_count_group_limit)
                                                            result_count_group_limit = cursor_count_group.fetchall()
                                                            t_start_agg_dt_group_limit = datetime.now().strftime(
                                                                '%d/%m/%Y %H:%M:%S')
                                                            data_dt = []
                                                            if len(result_count_group_limit) > 0:
                                                                iseq_dt_value = 0
                                                                for item_count_agg_limit in result_count_group_limit:
                                                                    iseq_dt_value += 1
                                                                    _FIELD_GROUP_BY = str(item_count_agg_limit[0])
                                                                    _FIELD_COUNT = item_count_agg_limit[1]
                                                                    _FIELD_COUNT_DISTINCT = item_count_agg_limit[2]
                                                                    item_dt_count = (
                                                                        _PROFILE_ID, _PULL_100_SEQ, iseq_dt_value,
                                                                        _FIELD_GROUP_BY,
                                                                        _FIELD_COUNT,
                                                                        _FIELD_COUNT_DISTINCT,
                                                                        "Pornc25")
                                                                    data_dt.append(item_dt_count)
                                                                try:
                                                                    ddl_command_group = sql_format_group_by
                                                                    print(ddl_command_group)
                                                                    with cx.connect(_user_db, _password_db,
                                                                                    dsn_tns_config) as connection:
                                                                        with connection.cursor() as cursor:
                                                                            cursor.executemany(ddl_command_group,
                                                                                               data_dt)
                                                                            connection.commit()

                                                                            _is_transaction_detail_group = True
                                                                except Exception as e:
                                                                    print('Error! Code: {c}, Message, {m}'.format(
                                                                        c=type(e).__name__, m=str(e)))
                                                                    _is_transaction_detail_group = False
                                                                finally:
                                                                    data_dt = []
                                                                    gc.collect()

                                                sql_transaction = sql_format_transaction.format(_PROFILE_ID,
                                                                                                _INGRESS_DATA_ID,
                                                                                                _RECORD_QTY, 'Pornc25',
                                                                                                t_start,
                                                                                                t_end, _MAX_VALUE, "2")
                                                try:
                                                    print(sql_transaction)
                                                    cursor_transaction = connect_config.cursor()
                                                    cursor_transaction.execute(sql_transaction)
                                                    connect_config.commit()
                                                    _is_transaction = True
                                                except Exception as e:
                                                    print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__,
                                                                                                  m=str(e)))
                                                    _is_transaction = False
                                        else:
                                            field_query_data_as_of = ""
                                            if _INGRESS_FIELD_DATA_AS_OF != "-":
                                                field_query_data_as_of = ",MAX({0}) as MAX_VALUE".format(
                                                    _INGRESS_FIELD_DATA_AS_OF)

                                            sql_count = sql_format_count.format(_INGRESS_TARGET_TABLE,
                                                                                field_query_data_as_of)
                                            _MAX_VALUE = ""
                                            if sql_count != "":
                                                sql_count = sql_format_count_sql.format(_INGRESS_SQL_COMMAND)
                                                t_start = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                                cursor_table_count = data_ingress.cursor()
                                                cursor_table_count.execute(sql_count)
                                                item_count = cursor_table_count.fetchone()
                                                if len(item_count) > 0:
                                                    _RECORD_QTY = item_count[0]
                                                t_end = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                                sql_transaction = sql_format_transaction.format(_PROFILE_ID,
                                                                                                _INGRESS_DATA_ID,
                                                                                                _RECORD_QTY,
                                                                                                'Pornc25',
                                                                                                t_start,
                                                                                                t_end, _RECORD_QTY,
                                                                                                "1")

                                            try:
                                                print(sql_transaction)
                                                cursor_transaction = connect_config.cursor()
                                                cursor_transaction.execute(sql_transaction)
                                                connect_config.commit()
                                                _is_transaction = True
                                            except Exception as e:
                                                print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__,
                                                                                              m=str(e)))
                                                _is_transaction = False
                                            iseq_dt = 0
                                            if _is_transaction:
                                                sql_transaction_delete = sql_format_delete_transaction.format(
                                                    _PROFILE_ID)
                                                try:
                                                    print(sql_transaction_delete)

                                                    # field_map = fields(cursor_transaction_value_del)
                                                    cursor_transaction_del = connect_config.cursor()
                                                    cursor_transaction_del.execute(sql_transaction_delete)
                                                    connect_config.commit()
                                                    _is_transaction_detail_delete = True
                                                except Exception as e:
                                                    print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__,
                                                                                                  m=str(e)))
                                                    _is_transaction_detail_delete = False
                                                sql_delete_transaction_value = sql_format_delete_transaction_value.format(
                                                    _PROFILE_ID)
                                                try:
                                                    print(sql_delete_transaction_value)

                                                    # field_map = fields(cursor_transaction_value_del)
                                                    cursor_transaction_del_value = connect_config.cursor()
                                                    cursor_transaction_del_value.execute(sql_delete_transaction_value)
                                                    connect_config.commit()
                                                    _is_transaction_detail_delete_value = True
                                                except Exception as e:
                                                    print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__,
                                                                                                  m=str(e)))
                                                    _is_transaction_detail_delete_value = False
                                            if _is_transaction_detail_delete and _is_transaction_detail_delete_value:
                                                if len(result_table) > 0:
                                                    for i in range(len(cursor_sql_command.description)):
                                                        print("Column {}:".format(i + 1))
                                                        item_column = cursor_sql_command.description[i]
                                                        # for item_column in result_table.description:
                                                        iseq_dt += 1
                                                        dt_column_name = str(item_column[0])
                                                        dt_column_type = str(item_column[1])
                                                        print(dt_column_name)
                                                        print(dt_column_type)
                                                        print(_TYPE_RUN)
                                                        print(_INGRESS_TARGET_TABLE)
                                                        # O WHERE FILTER , 1 NO FILTER
                                                        if _TYPE_RUN == "0":
                                                            sql_count_is_null = sql_format_count_is_null.format(
                                                                _INGRESS_TARGET_TABLE,
                                                                dt_column_name)
                                                        elif _TYPE_RUN == "1":
                                                            sql_count_is_null = sql_format_count_is_null_table.format(
                                                                _INGRESS_TARGET_TABLE_SPECIFIC,
                                                                dt_column_name)
                                                        elif _TYPE_RUN == "2":
                                                            sql_count_is_null = sql_format_count_sql_is_null.format(
                                                                _INGRESS_SQL_COMMAND,
                                                                dt_column_name)

                                                        _RECORD_NULL_DT_QTY = 0
                                                        if data_ingress:
                                                            t_start_dt = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                                            print(sql_count_is_null)
                                                            cursor_count_null = data_ingress.cursor()
                                                            cursor_count_null.execute(sql_count_is_null)
                                                            item_count_null = cursor_count_null.fetchone()
                                                            if len(item_count_null) > 0:
                                                                _RECORD_NULL_DT_QTY = item_count_null[0]
                                                                t_end_dt = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                                            _RECORD_PERCENT = _RECORD_NULL_DT_QTY / _RECORD_QTY
                                                            sql_transaction_detail = sql_format_transaction_detail.format(
                                                                _PROFILE_ID
                                                                , str(iseq_dt)
                                                                , dt_column_name
                                                                , dt_column_type.replace("'", "''")
                                                                , '0', _RECORD_NULL_DT_QTY, 'Pornc25', t_start_dt,
                                                                t_end_dt
                                                                , sql_count_is_null.replace("'", "''"),
                                                                str(round(_RECORD_PERCENT, 2))
                                                            )
                                                            print(sql_transaction_detail)
                                                            try:
                                                                cursor_transaction_detail = connect_config.cursor()
                                                                cursor_transaction_detail.execute(
                                                                    sql_transaction_detail)
                                                                connect_config.commit()
                                                                _is_transaction_detail = True
                                                            except Exception as e:
                                                                print('Error! Code: {c}, Message, {m}'.format(
                                                                    c=type(e).__name__, m=str(e)))
                                                                _is_transaction_detail = False

                                                            if _is_transaction_detail:
                                                                DATATYPE = "STR"
                                                                isINT = str(dt_column_type).lower().find(
                                                                    "int") > -1 or str(
                                                                    dt_column_type).lower().find("decimal") > -1 or str(
                                                                    dt_column_type).lower().find("number") > -1
                                                                isDouble = str(dt_column_type).lower().find(
                                                                    "double") > -1 or str(dt_column_type).lower().find(
                                                                    "float") > -1
                                                                isSTR = str(dt_column_type).lower().find("varchar") > -1
                                                                isDate = str(dt_column_type).lower().find(
                                                                    "date") > -1 or str(
                                                                    dt_column_type).lower().find("time") > -1

                                                                if isINT:
                                                                    DATATYPE = "INT"
                                                                elif isDate:
                                                                    DATATYPE = "DATE"
                                                                elif isDouble:
                                                                    DATATYPE = "DOUBLE"

                                                                sql_config_detail = sql_format_config_detail.format(
                                                                    connection_id=_INGRESS_CONNECTION_ID,
                                                                    data_type=DATATYPE)
                                                                print(sql_config_detail)
                                                                cursor_config_agg = connect_config.cursor()
                                                                cursor_config_agg.execute(sql_config_detail)
                                                                result_agg = cursor_config_agg.fetchall()
                                                                if len(result_agg) > 0:
                                                                    data = []
                                                                    for item_agg in result_agg:
                                                                        _DATA_TYPE = str(item_agg[0])
                                                                        _PROFILE_ATTRIBUTE_ID = str(item_agg[1])
                                                                        _PROFILE_ATTRIBUTE_NAME = str(item_agg[2])
                                                                        _PROFILE_ATTRIBUTE_DESC = str(item_agg[3])
                                                                        _AGGREGATE_FUNCTION = str(item_agg[4])
                                                                        _VAR_FIELD = str(item_agg[5])
                                                                        _VAR_TABLE = str(item_agg[6])
                                                                        _AGGREGATE_FUNCTION_SQL = str(item_agg[7])

                                                                        sql_aggregate = _AGGREGATE_FUNCTION_SQL.replace(
                                                                            _VAR_FIELD,
                                                                            dt_column_name).replace(
                                                                            _VAR_TABLE, _INGRESS_SQL_COMMAND)

                                                                        print(sql_aggregate)
                                                                        _RECORD_AGG_VALUE = ""
                                                                        if data_ingress:
                                                                            t_start_agg_dt = datetime.now().strftime(
                                                                                '%d/%m/%Y %H:%M:%S')
                                                                            cursor_count_agg = data_ingress.cursor()
                                                                            cursor_count_agg.execute(sql_aggregate)
                                                                            item_count_agg = cursor_count_agg.fetchone()
                                                                            if len(item_count_agg) > 0:
                                                                                _RECORD_AGG_VALUE = str(
                                                                                    item_count_agg[0])
                                                                                t_end_agg_dt = datetime.now().strftime(
                                                                                    '%d/%m/%Y %H:%M:%S')

                                                                            item = (_PROFILE_ID, iseq_dt,
                                                                                    _PROFILE_ATTRIBUTE_ID,
                                                                                    _RECORD_AGG_VALUE,
                                                                                    dt_column_name,
                                                                                    dt_column_type.replace("'", "''"),
                                                                                    sql_aggregate.replace("'", "''"),
                                                                                    "Pornc25")
                                                                            data.append(item)
                                                                    try:
                                                                        print(data)
                                                                        ddl_command = sql_format_transaction_value
                                                                        print(ddl_command)
                                                                        with cx.connect(_user_db, _password_db,
                                                                                        dsn_tns_config) as connection:
                                                                            with connection.cursor() as cursor:
                                                                                cursor.executemany(ddl_command,
                                                                                                   data)
                                                                                connection.commit()
                                                                                _is_transaction_detail_agg = True
                                                                    except Exception as e:
                                                                        print('Error! Code: {c}, Message, {m}'.format(
                                                                            c=type(e).__name__, m=str(e)))
                                                                        _is_transaction_detail_agg = False

                                                                    finally:
                                                                        data = []
                                                                        gc.collect()

                                                if _is_transaction_detail_agg:
                                                    print(
                                                        '******************************* Group Insert ************************')
                                                    sql_delete_group_by = sql_format_delete_group_by.format(
                                                        _PROFILE_ID)
                                                    try:
                                                        print(sql_delete_group_by)

                                                        # field_map = fields(cursor_transaction_value_del)
                                                        cursor_group_del = connect_config.cursor()
                                                        cursor_group_del.execute(sql_delete_group_by)
                                                        connect_config.commit()
                                                        _is_group_delete = True
                                                    except Exception as e:
                                                        print(
                                                            'Error! Code: {c}, Message, {m}'.format(c=type(e).__name__,
                                                                                                    m=str(e)))
                                                        _is_group_delete = False

                                                    time.sleep(10)
                                                    sql_format_pull_group_100 = "select  a.seq,b.column_name,b.column_type,b.column_value,a.record_null_qty,pd.record_qty " \
                                                                                " from dq_profile pd " \
                                                                                " inner join dq_profile_detail a on (pd.profile_id=a.profile_id) " \
                                                                                " inner join dq_profile_detail_value b on(a.PROFILE_ID=b.PROFILE_ID and a.COLUMN_NAME=b.COLUMN_NAME) " \
                                                                                " inner join dq_config_profile_attribute c on (b.profile_attribute_id=c.profile_attribute_id and c.profile_attribute_id = 5) " \
                                                                                " where pd.profile_id='{0}' " \
                                                                                " and to_number(b.column_value) <> to_number(pd.record_qty)" \
                                                                                " and to_number(b.column_value) > 1 " \
                                                                                " and (to_number(b.column_value)/to_number(pd.record_qty)) <= 0.001"

                                                    sql_format_count_group = "select {1},count({1}) as qty ,count(distinct {1}) as distinct_qty" \
                                                                             " from {0}" \
                                                                             " group by {1}"
                                                    if connect_config:
                                                        sql_pull_group_100 = sql_format_pull_group_100.format(
                                                            _PROFILE_ID)
                                                        print(sql_pull_group_100)
                                                        t_start_pull_100 = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                                        cursor_pull_group_100 = connect_config.cursor()
                                                        cursor_pull_group_100.execute(sql_pull_group_100)
                                                        result_pull_group_100 = cursor_pull_group_100.fetchall()
                                                        t_end_pull_100 = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                                        for item_pull_100 in result_pull_group_100:
                                                            _PULL_100_SEQ = item_pull_100[0]
                                                            _PULL_100_COLUMN_NAME = str(item_pull_100[1])
                                                            _PULL_100_COLUMN_TYPE = str(item_pull_100[2])
                                                            _PULL_100_COLUMN_VALUE = str(item_pull_100[3])
                                                            _PULL_100_RECORD_NULL_QTY = item_pull_100[4]
                                                            _PULL_100_RECORD_QTY = item_pull_100[5]

                                                            sql_format_count_group = "select {1},count({1}) as qty ,count(distinct {1}) as distinct_qty" \
                                                                                     " from ({0}) dt " \
                                                                                     " group by {1}"

                                                            sql_count_group = sql_format_count_group.format(
                                                                _INGRESS_SQL_COMMAND,
                                                                _PULL_100_COLUMN_NAME)

                                                            t_start_agg_dt_group = datetime.now().strftime(
                                                                '%d/%m/%Y %H:%M:%S')
                                                            cursor_count_group = data_ingress.cursor()
                                                            cursor_count_group.execute(sql_count_group)
                                                            result_count_group = cursor_count_group.fetchall()
                                                            t_end_agg_dt_group = datetime.now().strftime(
                                                                '%d/%m/%Y %H:%M:%S')
                                                            data_dt = []
                                                            if len(result_count_group) > 0:
                                                                iseq_dt_value = 0
                                                                for item_count_agg in result_count_group:
                                                                    iseq_dt_value += 1
                                                                    _FIELD_GROUP_BY = str(item_count_agg[0])
                                                                    _FIELD_COUNT = item_count_agg[1]
                                                                    _FIELD_COUNT_DISTINCT = item_count_agg[2]
                                                                    item_dt_count = (
                                                                        _PROFILE_ID, _PULL_100_SEQ, iseq_dt_value,
                                                                        _FIELD_GROUP_BY,
                                                                        _FIELD_COUNT,
                                                                        _FIELD_COUNT_DISTINCT,
                                                                        "Pornc25")
                                                                    data_dt.append(item_dt_count)
                                                                try:
                                                                    ddl_command_group = sql_format_group_by
                                                                    print(ddl_command_group)
                                                                    with cx.connect(_user_db, _password_db,
                                                                                    dsn_tns_config) as connection:
                                                                        with connection.cursor() as cursor:
                                                                            cursor.executemany(ddl_command_group,
                                                                                               data_dt)
                                                                            connection.commit()

                                                                            _is_transaction_detail_group = True
                                                                except Exception as e:
                                                                    print('Error! Code: {c}, Message, {m}'.format(
                                                                        c=type(e).__name__, m=str(e)))
                                                                    _is_transaction_detail_group = False
                                                                finally:
                                                                    data_dt = []
                                                                    gc.collect()

                                                    sql_format_pull_group_100_over = "select  a.seq,b.column_name,b.column_type,b.column_value,a.record_null_qty,pd.record_qty " \
                                                                                     " from dq_profile pd " \
                                                                                     " inner join dq_profile_detail a on (pd.profile_id=a.profile_id) " \
                                                                                     " inner join dq_profile_detail_value b on(a.PROFILE_ID=b.PROFILE_ID and a.COLUMN_NAME=b.COLUMN_NAME) " \
                                                                                     " inner join dq_config_profile_attribute c on (b.profile_attribute_id=c.profile_attribute_id and c.profile_attribute_id = 5) " \
                                                                                     " where pd.profile_id='{0}' " \
                                                                                     " and to_number(b.column_value) <> to_number(pd.record_qty)" \
                                                                                     " and to_number(b.column_value) > 1 " \
                                                                                     " and (to_number(b.column_value)/to_number(pd.record_qty)) > 0.001"

                                                    if _INGRESS_CONNECTION_TYPE.lower() == "hadoop":
                                                        sql_format_count_group_limit = "select {1},count({1}) as qty ,count(distinct {1}) as distinct_qty" \
                                                                                       " from ({0}) dt " \
                                                                                       " group by {1}" \
                                                                                       " order by count({1}) desc" \
                                                                                       " limit 10 "
                                                    elif _INGRESS_CONNECTION_TYPE.lower() == "oracle":
                                                        sql_format_count_group_limit = "select {1},count({1}) as qty ,count(distinct {1}) as distinct_qty" \
                                                                                       " from ({0}) dt " \
                                                                                       " where rownum <= 100" \
                                                                                       " group by {1}" \
                                                                                       " order by count({1}) desc"
                                                    if connect_config:
                                                        sql_pull_group_100_over = sql_format_pull_group_100_over.format(
                                                            _PROFILE_ID)
                                                        t_start_pull_100_over = datetime.now().strftime(
                                                            '%d/%m/%Y %H:%M:%S')
                                                        cursor_pull_group_100_over = connect_config.cursor()
                                                        cursor_pull_group_100_over.execute(sql_pull_group_100_over)
                                                        result_pull_group_100_over = cursor_pull_group_100_over.fetchall()
                                                        t_end_pull_100_over = datetime.now().strftime(
                                                            '%d/%m/%Y %H:%M:%S')
                                                        for item_pull_100_over in result_pull_group_100_over:
                                                            _PULL_100_SEQ_over = item_pull_100_over[0]
                                                            _PULL_100_COLUMN_NAME_over = str(item_pull_100_over[1])
                                                            _PULL_100_COLUMN_TYPE_over = str(item_pull_100_over[2])
                                                            _PULL_100_COLUMN_VALUE_over = str(item_pull_100_over[3])
                                                            _PULL_100_RECORD_NULL_QTY_over = item_pull_100_over[4]
                                                            _PULL_100_RECORD_QTY_over = item_pull_100_over[5]

                                                            sql_count_group_limit = sql_format_count_group_limit.format(
                                                                _INGRESS_SQL_COMMAND,
                                                                _PULL_100_COLUMN_NAME_over)

                                                            t_start_agg_dt_group_limit = datetime.now().strftime(
                                                                '%d/%m/%Y %H:%M:%S')
                                                            cursor_count_group_limit = data_ingress.cursor()
                                                            cursor_count_group_limit.execute(sql_count_group_limit)
                                                            result_count_group_limit = cursor_count_group.fetchall()
                                                            t_start_agg_dt_group_limit = datetime.now().strftime(
                                                                '%d/%m/%Y %H:%M:%S')
                                                            data_dt = []
                                                            if len(result_count_group_limit) > 0:
                                                                iseq_dt_value = 0
                                                                for item_count_agg_limit in result_count_group_limit:
                                                                    iseq_dt_value += 1
                                                                    _FIELD_GROUP_BY = str(item_count_agg_limit[0])
                                                                    _FIELD_COUNT = item_count_agg_limit[1]
                                                                    _FIELD_COUNT_DISTINCT = item_count_agg_limit[2]
                                                                    item_dt_count = (
                                                                        _PROFILE_ID, _PULL_100_SEQ, iseq_dt_value,
                                                                        _FIELD_GROUP_BY,
                                                                        _FIELD_COUNT,
                                                                        _FIELD_COUNT_DISTINCT,
                                                                        "Pornc25")
                                                                    data_dt.append(item_dt_count)
                                                                try:
                                                                    ddl_command_group = sql_format_group_by
                                                                    print(ddl_command_group)
                                                                    with cx.connect(_user_db, _password_db,
                                                                                    dsn_tns_config) as connection:
                                                                        with connection.cursor() as cursor:
                                                                            cursor.executemany(ddl_command_group,
                                                                                               data_dt)
                                                                            connection.commit()

                                                                            _is_transaction_detail_group = True
                                                                except Exception as e:
                                                                    print('Error! Code: {c}, Message, {m}'.format(
                                                                        c=type(e).__name__, m=str(e)))
                                                                    _is_transaction_detail_group = False
                                                                finally:
                                                                    data_dt = []
                                                                    gc.collect()

                                                sql_transaction = sql_format_transaction.format(_PROFILE_ID,
                                                                                                _INGRESS_DATA_ID,
                                                                                                _RECORD_QTY, 'Pornc25',
                                                                                                t_start,
                                                                                                t_end, _MAX_VALUE, "2")
                                                try:
                                                    print(sql_transaction)
                                                    cursor_transaction = connect_config.cursor()
                                                    cursor_transaction.execute(sql_transaction)
                                                    connect_config.commit()
                                                    _is_transaction = True
                                                except Exception as e:
                                                    print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__,
                                                                                                  m=str(e)))
                                                    _is_transaction = False

                                    except Exception as e:
                                        print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
                                        err_msg = 'Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e))
                                        sql_transaction_failed = sql_format_transaction_failed.format(_PROFILE_ID,
                                                                                                      err_msg.replace(
                                                                                                          "'", "''"))
                                        try:
                                            print(sql_transaction_failed)
                                            cursor_transaction = connect_config.cursor()
                                            cursor_transaction.execute(sql_transaction_failed)
                                            connect_config.commit()
                                            _is_transaction = True
                                        except Exception as e:
                                            print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__,
                                                                                          m=str(e)))
                                            _is_transaction = False

    except Exception as e:
        print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    os.environ["APP_HOME"] = os.path.dirname(os.path.realpath(__file__))
    if "DB_HOST" in os.environ:
        _host_db = os.environ["DB_HOST"]
    else:
        _host_db = "dbdwhdv1"
    if "DB_PORT" in os.environ:
        _port_db = os.environ["DB_PORT"]
    else:
        _port_db = "1550"
    if "DB_SERVICE" in os.environ:
        _service_db = os.environ["DB_SERVICE"]
    else:
        _service_db = "TEDWDEV"
    if "UD" in os.environ:
        _user_db = os.environ["USER_DB"]
    else:
        _user_db = "TEDWBORAPPO"
    if "PD" in os.environ:
        _password_db = os.environ["PASSWORD"]
    else:
        _password_db = "bortedw123"

    process(_host_db, _port_db, _service_db, _user_db, _password_db)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
