import os
import sys
import argparse
import datetime

import gc
import cx_Oracle as cx
import pyodbc as hadoop
import pytz
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from datetime import datetime, timedelta
import time

import pandas as pd
import json

from elasticsearch.connection import create_ssl_context
import urllib3
import ssl
from cryptography.fernet import Fernet, InvalidToken
import logging

key_api = "8zNU418HIcCijV_KPfCm-OwNsVKGhMOxtl906nyzzmM="


def decryption(key: None, param: None):
    cipher_suite = None
    try:
        f = Fernet(key)
    except InvalidToken:
        f = None
    if not f is None:
        try:
            decrypted_bytes = f.decrypt(param.encode("utf-8"))
        except InvalidToken:
            decrypted_bytes = None  # TODO(kmullins): Shall we log this case? Is it expected?
        if not decrypted_bytes is None:
            decrypted_string = decrypted_bytes.decode()  # bytes -> str
        else:
            decrypted_string = "encryption is not support"
    else:
        key = "key is not support"
    return decrypted_string


tz = pytz.timezone('Asia/Bangkok')


def fields(cursor):
    """ Given a DB API 2.0 cursor object that has been executed, returns
    a dictionary that maps each field name to a column index; 0 and up. """
    results = {}
    column = 0
    for d in cursor.description:
        results[d[0]] = column
        column = column + 1

    return results


def target_elasticsearch(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT, _TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                         _TARGET_STATIC, _TARGET_FIELD_STATIC, array_target):
    urllib3.disable_warnings()

    host = _TARGET_PARAM_HOST
    port = _TARGET_PARAM_PORT
    user = _TARGET_DATABASE_USER
    password = _TARGET_DATABASE_PASSWORD
    now = datetime.today()

    ssl_context = create_ssl_context(cafile='cacert-esd.pem')
    ssl_context.check_hostname = False
    ssl_context.verify_mode = ssl.CERT_OPTIONAL
    es = Elasticsearch(hosts=[{'host': host, 'port': _TARGET_PARAM_PORT}],
                       scheme="https",
                       verify_certs=True,
                       ssl_context=ssl_context,
                       http_auth=(user, password))
    if _TARGET_FIELD_STATIC == "":
        document_id = str(now.strftime('%Y-%m-%d'))
    else:
        document_id = _TARGET_FIELD_STATIC

    index = _TARGET_STATIC + document_id
    print(index)
    doc_list = []
    now = datetime.today()
    now_timestatmp = now.strftime('%Y-%m-%dT%H:%M:%S') + ('-%02d' % (now.microsecond / 10000))
    ind = 0
    topic_name = ""
    if len(array_target) > 0:
        for item_target in array_target:
            doc_list += [item_target]

        print(index)
        if len(doc_list) > 0:
            try:
                print(
                    "\nAttempting to index =" + index + " the list of docs using helpers.bulk(" + str(
                        len(doc_list)) + ")")

                resp = helpers.bulk(es, doc_list, index=index, doc_type='_doc')
                print("helpers.bulk " + now_timestatmp + " = ", resp)
            except Exception as e:
                print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
            finally:
                es.close()


def is_connection_validate(HOST, PORT, USER, PASS):
    return len(HOST) > 0 and len(PORT) > 0 and len(USER) > 0 and len(PASS) > 0


def percent(x, y):
    result = ""
    if not x and not y:
        result = ("x = 0%\ny = 0%")
    elif x < 0 or y < 0:
        result = ("The inputs can't be negative!")
    else:
        final = 100 / (x + y)
        x *= final
        y *= final
        result = ('x = {}%\ny = {}%'.format(x, y))
    return result


def process(host, port, service, user, password, _transaction_id):
    try:
        _RULE_ID = ""
        _INGRESS_DATA_ID = ""
        _INGRESS_DATA_NAME = ""
        _INGRESS_CONNECTION_ID = ""
        _INGRESS_SQL_COMMAND = ""
        _INGRESS_TARGET_TABLE = ""
        _INGRESS_CONNECTION_NAME = ""
        _INGRESS_PARAM_HOST = ""
        _INGRESS_PARAM_PORT = ""
        _INGRESS_PARAM_SERVICE_NAME = ""
        _CDE_ID = ""
        _CDE_NAME = ""
        _REMARK = ""
        _INGRESS_CONNECTION_TYPE = ""
        _INGRESS_CONNECTION_STRING = ""
        _INGRESS_PARAM_USER = ""
        _INGRESS_PARAM_PASSWORD = ""
        _INGRESS_EXISTS_TABLE = "0"
        is_ingress_exists_table = False
        is_ingress_exec_table = False

        _BUSINESS_RULE_ID_DETAIL = ""
        _BUSINESS_RULE_NAME_DETAIL = ""
        _SQL_COMMAND_DETAIL = ""
        _CONNECTION_NAME_DETAIL = ""
        _CONNECTION_STRING_DETAIL = ""
        _QUALITY_INDEX_ID_DETAIL = ""
        _PARAM_HOST_DETAIL = ""
        _PARAM_PORT_DETAIL = ""
        _PARAM_SERVICE_NAME_DETAIL = ""
        _CONNECTION_TYPE_DETAIL = ""
        _CONNECTION_USER_DETAIL = ""
        _CONNECTION_PASSWORD_DETAIL = ""

        _is_transaction = False
        _is_transaction_detail = False
        _is_transaction_detail_delete = False
        _is_new_uuid = False
        _TRANSACTION_ID = ""
        _RECORD_QTY = 0

        sql_running_reference = " select distinct reference_id " \
                                " from DQ_REFERENCE  " \
                                " where status ='0' " \
                                " and reference_id='{0}'"

        sql_format_reference_id = "select a.reference_id," \
                                  "\n b.INGRESS_DATA_ID," \
                                  "\n b.COLUMN_SEQ," \
                                  "\n b.COLUMN_NAME," \
                                  "\n b.COLUMN_TYPE ," \
                                  "\n b.RULE_DEFINITION_ID," \
                                  "\n b.SQL_COMMAND, " \
                                  "\n C.INGRESS_DATA_NAME, " \
                                  "\n C.SQL_COMMAND AS INGRESS_SQL_COMMAND, " \
                                  "\n C.INGRESS_TARGET || ' ' || nvl(C.INGRESS_TARGET_FILTER,'') AS INGRESS_TARGET_TABLE, " \
                                  "\n N.CONNECTION_NAME, " \
                                  "\n N.PARAM_HOST, " \
                                  "\n N.PARAM_PORT, " \
                                  "\n N.PARAM_SERVICE_NAME, " \
                                  "\n N.CONNECTION_TYPE, " \
                                  "\n N.DATABASE_USER," \
                                  "\n N.DATABASE_PASSWORD," \
                                  "\n C.USED_IS_COMMAND," \
                                  "\n N.CONNECTION_STRING," \
                                  "\n b.RULE_DEFINITION_NAME," \
                                  "\n nvl(b.SQL_COMMAND,R.SQL_COLUMN) as SQL_COLUMN," \
                                  "\n nvl(b.SEQ,1) SEQ," \
                                  "\n nvl(c.INGRESS_DATA_TYPE_RUN,1) INGRESS_DATA_TYPE_RUN," \
                                  "\n b.QUERY," \
                                  "\n b.SQL_COMMAND_OPPOSITE," \
                                  "\n b.QUERY_OPPOSITE," \
                                  "\n b.QUERY_RECORD" \
                                  "\n from DQ_REFERENCE a" \
                                  "\n inner join  DQ_REFERENCE_DETAIL b on (a.reference_id=b.reference_id) " \
                                  "\n inner join DQ_CONFIG_INGRESS_DATA C ON (b.INGRESS_DATA_ID = C.INGRESS_DATA_ID) " \
                                  "\n LEFT JOIN DWH_CONFIG_CONNECTION N ON (C.CONNECTION_ID = N.ID)  " \
                                  "\n LEFT JOIN DQ_CONFIG_RULE_DEFINITIONS R ON (b.RULE_DEFINITION_ID = R.RULE_DEFINITION_ID)  " \
                                  "\n where a.reference_id = '{reference_id}'" \
                                  "\n and a.status='0' " \
                                  "\n order by a.reference_id,b.INGRESS_DATA_ID,nvl(b.SEQ,1),b.COLUMN_SEQ"

        sql_reference = ""
        sql_format_update_reference_dt = "UPDATE DQ_REFERENCE_DETAIL t" \
                                         "\n set  " \
                                         "\n t.STATUS_RUNNING       = '{3}' " \
                                         "\n ,t.START_DATE          = '{4}' " \
                                         "\n ,t.END_DATE            = '{5}' " \
                                         "\n ,t.RECORD_QTY          =  {6} " \
                                         "\n ,t.VALIDATE_RECORD_QTY =  {7} " \
                                         "\n ,t.UPD_BY          = sysdate " \
                                         "\n WHERE t.REFERENCE_ID='{0}'" \
                                         "\n and t.SEQ={1}" \
                                         "\n and t.COLUMN_SEQ={2}"
        sql_update_reference_dt = ""

        sql_format_update_reference = "UPDATE DQ_REFERENCE t" \
                                      "\n set  " \
                                      "\n t.STATUS_RUNNING   = '{1}' " \
                                      "\n ,t.PPN_TM          = sysdate " \
                                      "\n WHERE t.REFERENCE_ID='{0}'"

        sql_format_update_reference_success = "UPDATE DQ_REFERENCE t" \
                                              "\n set  " \
                                              "\n t.STATUS_RUNNING   = '2' " \
                                              "\n ,t.UPD_TM          = sysdate " \
                                              "\n ,t.DQI_SCORE       = (select sum(nvl(a.dqi,0))/count(*) from DQ_REFERENCE_DETAIL a  where a.REFERENCE_ID='{0}'  and status='0' and nvl(is_active,'0') ='0' ) " \
                                              "\n WHERE t.REFERENCE_ID='{0}'"

        sql_format_update_reference_failed = "UPDATE DQ_REFERENCE t" \
                                             "\n set  " \
                                             "\n t.STATUS_RUNNING   = '3' " \
                                             "\n ,t.UPD_TM          = sysdate " \
                                             "\n ,t.ERROR_MSG      = '{1}' " \
                                             "\n WHERE t.REFERENCE_ID='{0}'"

        sql_format_update_reference_inprogress = "UPDATE DQ_REFERENCE t" \
                                                 "\n set  " \
                                                 "\n t.STATUS_RUNNING   = '4' " \
                                                 "\n ,t.UPD_TM          = sysdate " \
                                                 "\n WHERE t.REFERENCE_ID='{0}'"

        sql_update_reference = ""

        sql_format_transaction_detail = "Merge Into DQ_TRANSACTION_DETAIL t" \
                                        "\n Using (" \
                                        "\n select " \
                                        "\n  '{0}' as TRANSACTION_ID" \
                                        "\n ,'{1}' as RULE_ID " \
                                        "\n ,'{2}' as BUSINESS_RULE_ID " \
                                        "\n , {3}  as QUALITY_INDEX_ID " \
                                        "\n ,'{4}' as SQL_COMMAND " \
                                        "\n , {5}  as RECORD_QTY " \
                                        "\n , {6}  as VALIDATE_RECORD_QTY " \
                                        "\n ,'{7}' as USER_BY " \
                                        "\n ,to_date('{8}','dd/MM/yyyy HH24:mi:ss') as start_date " \
                                        "\n ,to_date('{9}','dd/MM/yyyy HH24:mi:ss') as end_date  " \
                                        "\n ,sysdate as TM " \
                                        "\n from dual ) s " \
                                        "\n on (" \
                                        "\n t.transaction_id=s.transaction_id" \
                                        "\n and t.RULE_ID=s.RULE_ID" \
                                        "\n and t.BUSINESS_RULE_ID=s.BUSINESS_RULE_ID" \
                                        "\n and t.QUALITY_INDEX_ID=s.QUALITY_INDEX_ID" \
                                        "\n )" \
                                        "\n when Matched Then" \
                                        "\n UPDATE set  " \
                                        "\n t.RECORD_QTY            = s.RECORD_QTY " \
                                        "\n ,t.VALIDATE_RECORD_QTY  = s.VALIDATE_RECORD_QTY " \
                                        "\n ,t.SQL_COMMAND          = s.SQL_COMMAND " \
                                        "\n ,t.START_DATE           = s.START_DATE " \
                                        "\n ,t.END_DATE             = s.END_DATE " \
                                        "\n ,t.UPD_TM               = s.TM " \
                                        "\n ,t.UPD_BY               = s.USER_BY " \
                                        "\n when Not Matched Then" \
                                        "\n Insert (" \
                                        "t.TRANSACTION_ID" \
                                        ",t.RULE_ID" \
                                        ",t.BUSINESS_RULE_ID " \
                                        ",t.QUALITY_INDEX_ID " \
                                        ",t.SQL_COMMAND" \
                                        ",t.RECORD_QTY" \
                                        ",t.START_DATE" \
                                        ",t.END_DATE" \
                                        ", t.PPN_BY" \
                                        ", t.PPN_TM)" \
                                        "\n Values (" \
                                        "s.TRANSACTION_ID" \
                                        ", s.RULE_ID" \
                                        ", s.BUSINESS_RULE_ID" \
                                        ", s.QUALITY_INDEX_ID " \
                                        ", s.SQL_COMMAND " \
                                        ", s.RECORD_QTY " \
                                        ", s.START_DATE " \
                                        ", s.END_DATE " \
                                        ", s.USER_BY" \
                                        ", s.TM) "

        sql_format_transaction_value = "Insert Into DQ_TRANSACTION_DETAIL_VALUE " \
                                       "\n  (" \
                                       "TRANSACTION_ID " \
                                       ",RULE_ID " \
                                       ",BUSINESS_RULE_ID " \
                                       ",QUALITY_INDEX_ID " \
                                       ",ROW_SEQ " \
                                       ",COL_SEQ " \
                                       ",COLUMN_NAME " \
                                       ",COLUMN_VALUE " \
                                       ",COLUMN_TYPE " \
                                       ",PPN_BY " \
                                       ") " \
                                       "\n values " \
                                       "\n (" \
                                       ":TRANSACTION_ID " \
                                       ",:RULE_ID " \
                                       ",:BUSINESS_RULE_ID " \
                                       ",:QUALITY_INDEX_ID " \
                                       ",:ROW_SEQ " \
                                       ",:COL_SEQ " \
                                       ",:COLUMN_NAME " \
                                       ",:COLUMN_VALUE " \
                                       ",:COLUMN_TYPE " \
                                       ",:PPN_BY " \
                                       ") "

        dsn_tns_config = cx.makedsn(host, port, service_name=service)
        connect_config = cx.connect(user, password, dsn=dsn_tns_config)
        if connect_config:

            cursor_loop = connect_config.cursor()
            cursor_loop.execute(sql_running_reference.format(_transaction_id))
            result_loop = cursor_loop.fetchall()
            if len(result_loop) > 0:
                for item_loop in result_loop:
                    _reference_id = item_loop[0]
                    if _reference_id != "":
                        sql_update_reference = sql_format_update_reference_inprogress.format(_reference_id)
                        print(sql_update_reference)
                        cursor_qa_set_running = connect_config.cursor()
                        cursor_qa_set_running.execute(sql_update_reference)
                        connect_config.commit()

                        sql_reference = sql_format_reference_id.format(reference_id=_reference_id)
                        cursor_qa_dt = connect_config.cursor()
                        cursor_qa_dt.execute(sql_reference)
                        result_qa_dt = cursor_qa_dt.fetchall()
                        if len(result_qa_dt) > 0:

                            sql_format_update_reference_dt = "UPDATE DQ_REFERENCE_DETAIL t" \
                                                             "\n set  " \
                                                             "\n t.STATUS_RUNNING       = '0',t.DQI = 0 " \
                                                             "\n ,t.UPD_BY          = sysdate " \
                                                             "\n WHERE t.REFERENCE_ID='{0}'"
                            sql_update_reference_dt = sql_format_update_reference_dt.format(
                                _reference_id)

                            cursor_qa_dt_running = connect_config.cursor()
                            print(sql_update_reference_dt)
                            cursor_qa_dt_running.execute(sql_update_reference_dt)
                            connect_config.commit()

                            for item_qa_dt in result_qa_dt:

                                _REFERENCE_ID = str(item_qa_dt[0])
                                _INGRESS_DATA_ID = str(item_qa_dt[1])
                                _COLUMN_SEQ = str(item_qa_dt[2])
                                _COLUMN_NAME = str(item_qa_dt[3])
                                _COLUMN_TYPE = str(item_qa_dt[4])
                                _RULE_DEFINITION_ID = str(item_qa_dt[5])
                                _SQL_COMMAND = str(item_qa_dt[6])
                                _INGRESS_DATA_NAME = str(item_qa_dt[7])
                                _INGRESS_SQL_COMMAND = str(item_qa_dt[8])
                                _INGRESS_TARGET_TABLE = str(item_qa_dt[9])
                                _CONNECTION_NAME = str(item_qa_dt[10])
                                _PARAM_HOST = str(item_qa_dt[11])
                                _PARAM_PORT = str(item_qa_dt[12])
                                _PARAM_SERVICE_NAME = str(item_qa_dt[13])
                                _CONNECTION_TYPE = str(item_qa_dt[14])
                                _DATABASE_USER = str(item_qa_dt[15])
                                _DATABASE_PASSWORD = str(item_qa_dt[16])
                                _USED_IS_COMMAND = str(item_qa_dt[17])
                                _CONNECTION_STRING = str(item_qa_dt[18])
                                _RULE_DEFINITION_NAME = str(item_qa_dt[19])
                                _SQL_COLUMN = item_qa_dt[20]
                                _SEQ = item_qa_dt[21]
                                _INGRESS_DATA_TYPE_RUN = item_qa_dt[22]
                                _SQL_COLUMN_QUERY = item_qa_dt[23]
                                _SQL_COLUMN_OPPOSITE = item_qa_dt[24]
                                _SQL_COLUMN_QUERY_OPPOSITE = item_qa_dt[25]
                                _QUERY_RECORD = item_qa_dt[26]

                                print(_CONNECTION_STRING)
                                is_user_is_command = (_INGRESS_DATA_TYPE_RUN == "2")
                                print(_CONNECTION_TYPE)
                                if _CONNECTION_TYPE != "":
                                    if _CONNECTION_TYPE.lower() == "hadoop":
                                        data_rule = hadoop.connect(_CONNECTION_STRING, autocommit=True)
                                    elif _CONNECTION_TYPE.lower() == "oracle":
                                        dsn_tns = cx.makedsn(_PARAM_HOST, _PARAM_PORT,
                                                             _PARAM_SERVICE_NAME)
                                        data_rule = cx.connect(_DATABASE_USER, _DATABASE_PASSWORD,
                                                               dsn=dsn_tns)
                                    if data_rule:
                                        try:
                                            sql_update_reference = sql_format_update_reference.format(_REFERENCE_ID,
                                                                                                      "1")

                                            cursor_qa_set_running = connect_config.cursor()
                                            cursor_qa_set_running.execute(sql_update_reference)
                                            connect_config.commit()
                                            sql_query_rule = ""
                                            sql_query_format_rule = ""
                                            sql_query_format = ""
                                            sql_query = ""
                                            print(_INGRESS_TARGET_TABLE)
                                            print(_INGRESS_SQL_COMMAND)
                                            # is_user_is_command = False
                                            if _SQL_COLUMN is not None:
                                                if not is_user_is_command:
                                                    sql_query_format = "select count(*) as qty from {0} "
                                                    sql_query = sql_query_format.format(_INGRESS_TARGET_TABLE)
                                                    if _SQL_COLUMN_QUERY is not None:
                                                        sql_query_rule = _SQL_COLUMN_QUERY
                                                    else:
                                                        if _SQL_COLUMN is not None:
                                                            sql_query_format_rule = "select count(*) as qty  from {table_name} and "
                                                            sql_query_rule = sql_query_format_rule.format(
                                                                field_name=_COLUMN_NAME,
                                                                table_name=_INGRESS_TARGET_TABLE) + _SQL_COLUMN

                                                        else:
                                                            sql_query_format_rule = "select count(*) as qty  from {table_name} "
                                                            sql_query_rule = sql_query_format_rule.format(
                                                                field_name=_COLUMN_NAME,
                                                                table_name=_INGRESS_TARGET_TABLE)
                                                else:
                                                    sql_query_format = "select count(*) as qty from ({0}) a "
                                                    sql_query = sql_query_format.format(_INGRESS_SQL_COMMAND)
                                                    if _SQL_COLUMN_QUERY is not None:
                                                        sql_query_rule = _SQL_COLUMN_QUERY
                                                    else:
                                                        if _SQL_COLUMN is not None:
                                                            sql_query_format_rule = "select count(*) as qty  from ({table_name}) a where "
                                                            sql_query_rule = sql_query_format_rule.format(
                                                                field_name=_COLUMN_NAME,
                                                                table_name=_INGRESS_SQL_COMMAND) + _SQL_COLUMN
                                                        else:
                                                            sql_query_format_rule = "select count(*) as qty  from ({table_name}) a "
                                                            sql_query_rule = sql_query_format_rule.format(
                                                                field_name=_COLUMN_NAME,
                                                                table_name=_INGRESS_SQL_COMMAND)

                                                if sql_query_rule != "":
                                                    try:
                                                        _RECORD_VALIDATE_DT_QTY = 0
                                                        _RECORD_DT_QTY = 0

                                                        sql_format_update_reference_dt = "UPDATE DQ_REFERENCE_DETAIL t" \
                                                                                         "\n set  " \
                                                                                         "\n t.STATUS_RUNNING       = '{3}' " \
                                                                                         "\n ,t.START_DATE          = sysdate " \
                                                                                         "\n ,t.UPD_BY          = sysdate " \
                                                                                         "\n ,t.QUERY          = '{4}' " \
                                                                                         "\n WHERE t.REFERENCE_ID='{0}'" \
                                                                                         "\n and t.SEQ ={1}" \
                                                                                         "\n and t.COLUMN_SEQ={2}"
                                                        sql_update_reference_dt = sql_format_update_reference_dt.format(
                                                            _REFERENCE_ID,
                                                            _SEQ,
                                                            _COLUMN_SEQ,
                                                            "1", sql_query_rule.replace("'", "''"))

                                                        cursor_qa_dt_running = connect_config.cursor()
                                                        print(sql_update_reference_dt)
                                                        cursor_qa_dt_running.execute(sql_update_reference_dt)
                                                        connect_config.commit()
                                                        if _QUERY_RECORD is not None:
                                                            if _QUERY_RECORD != "":
                                                                sql_query = _QUERY_RECORD

                                                        cursor_result_qa = data_rule.cursor()
                                                        cursor_result_qa.execute(sql_query)
                                                        result_qa = cursor_result_qa.fetchone()
                                                        _RECORD_DT_QTY = (result_qa[0])

                                                        sql_format_update_reference_dt = "UPDATE DQ_REFERENCE_DETAIL t" \
                                                                                         "\n set  " \
                                                                                         "\n t.RECORD_QTY          =  {3} " \
                                                                                         "\n ,t.UPD_BY          = sysdate " \
                                                                                         "\n WHERE t.REFERENCE_ID='{0}'" \
                                                                                         "\n and t.SEQ={1}" \
                                                                                         "\n and t.COLUMN_SEQ={2}"
                                                        sql_update_reference_dt = sql_format_update_reference_dt.format(
                                                            _REFERENCE_ID,
                                                            _SEQ,
                                                            _COLUMN_SEQ,
                                                            str(_RECORD_DT_QTY))

                                                        cursor_qa_dt_running = connect_config.cursor()
                                                        print(sql_update_reference_dt)
                                                        cursor_qa_dt_running.execute(sql_update_reference_dt)
                                                        connect_config.commit()

                                                        cursor_result_qa = data_rule.cursor()
                                                        print(sql_query_rule)
                                                        cursor_result_qa.execute(sql_query_rule)
                                                        result_qa = cursor_result_qa.fetchone()
                                                        _RECORD_VALIDATE_DT_QTY = (result_qa[0])
                                                        _DQI = (
                                                            round(_RECORD_VALIDATE_DT_QTY / _RECORD_DT_QTY * 100, 2))
                                                        # _DQI = (100 - round(_RECORD_VALIDATE_DT_QTY / _RECORD_DT_QTY * 100, 2))
                                                        # if _DQI == 0:
                                                        #    _DQI = 100
                                                        print('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX = ' + str(_DQI))
                                                        sql_format_update_reference_dt = "UPDATE DQ_REFERENCE_DETAIL t" \
                                                                                         "\n set  " \
                                                                                         "\n t.VALIDATE_RECORD_QTY  =  {3} " \
                                                                                         "\n ,t.DQI              = {4} " \
                                                                                         "\n ,t.UPD_BY              = sysdate " \
                                                                                         "\n WHERE t.REFERENCE_ID='{0}'" \
                                                                                         "\n and t.SEQ={1}" \
                                                                                         "\n and t.COLUMN_SEQ={2}"
                                                        sql_update_reference_dt = sql_format_update_reference_dt.format(
                                                            _REFERENCE_ID,
                                                            _SEQ,
                                                            _COLUMN_SEQ,
                                                            str(_RECORD_VALIDATE_DT_QTY),
                                                            str(_DQI)
                                                        )

                                                        cursor_qa_dt_running = connect_config.cursor()
                                                        print(sql_update_reference_dt)
                                                        cursor_qa_dt_running.execute(sql_update_reference_dt)
                                                        connect_config.commit()

                                                        if (_RECORD_DT_QTY == _RECORD_VALIDATE_DT_QTY) and (
                                                                _RECORD_DT_QTY > 0):
                                                            sql_format_update_reference_dt = "UPDATE DQ_REFERENCE_DETAIL t" \
                                                                                             "\n set  " \
                                                                                             "\n t.STATUS_RUNNING       = '{3}' " \
                                                                                             "\n ,t.END_DATE          = sysdate " \
                                                                                             "\n ,t.UPD_BY          = sysdate " \
                                                                                             "\n ,t.DQI          = 100 " \
                                                                                             "\n WHERE t.REFERENCE_ID='{0}'" \
                                                                                             "\n and t.SEQ={1}" \
                                                                                             "\n and t.COLUMN_SEQ={2}"
                                                            sql_update_reference_dt = sql_format_update_reference_dt.format(
                                                                _REFERENCE_ID,
                                                                _SEQ,
                                                                _COLUMN_SEQ,
                                                                "2")
                                                        else:
                                                            sql_format_update_reference_dt = "UPDATE DQ_REFERENCE_DETAIL t" \
                                                                                             "\n set  " \
                                                                                             "\n t.STATUS_RUNNING       = '{3}' " \
                                                                                             "\n ,t.END_DATE          = sysdate " \
                                                                                             "\n ,t.UPD_BY          = sysdate " \
                                                                                             "\n WHERE t.REFERENCE_ID='{0}'" \
                                                                                             "\n and t.SEQ={1}" \
                                                                                             "\n and t.COLUMN_SEQ={2}"
                                                            sql_update_reference_dt = sql_format_update_reference_dt.format(
                                                                _REFERENCE_ID,
                                                                _SEQ,
                                                                _COLUMN_SEQ,
                                                                "2")

                                                        cursor_qa_dt_running = connect_config.cursor()
                                                        print(sql_update_reference_dt)
                                                        cursor_qa_dt_running.execute(sql_update_reference_dt)
                                                        connect_config.commit()
                                                    except Exception as e:
                                                        print(
                                                            'Error! Code: {c}, Message, {m}'.format(c=type(e).__name__,
                                                                                                    m=str(e)))
                                                        err_msg = 'Error! Code: {c}, Message, {m}'.format(
                                                            c=type(e).__name__, m=str(e))
                                                        sql_format_update_reference_dt = "UPDATE DQ_REFERENCE_DETAIL t" \
                                                                                         "\n set  " \
                                                                                         "\n t.STATUS_RUNNING       = '{3}' " \
                                                                                         "\n ,t.END_DATE          = sysdate " \
                                                                                         "\n ,t.UPD_BY          = sysdate " \
                                                                                         "\n ,t.DQI             = 0 " \
                                                                                         "\n ,t.ERROR_MSG          = '{4}' " \
                                                                                         "\n WHERE t.REFERENCE_ID='{0}'" \
                                                                                         "\n and t.SEQ={1}" \
                                                                                         "\n and t.COLUMN_SEQ={2}"
                                                        sql_update_reference_dt = sql_format_update_reference_dt.format(
                                                            _REFERENCE_ID,
                                                            _SEQ,
                                                            _COLUMN_SEQ,
                                                            "3", err_msg.replace("'", "''"))

                                            sql_update_reference = sql_format_update_reference_success.format(
                                                _REFERENCE_ID)
                                            print(sql_update_reference)
                                            cursor_qa_set_running = connect_config.cursor()
                                            cursor_qa_set_running.execute(sql_update_reference)
                                            connect_config.commit()


                                        except Exception as e:
                                            print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
                                            err_msg = 'Error! Code: {c}, Message, {m}'.format(
                                                c=type(e).__name__, m=str(e))

                                            sql_update_reference = sql_format_update_reference_failed.format(
                                                _REFERENCE_ID
                                                , err_msg.replace("'", "''"))
                                            print(sql_update_reference)
                                            cursor_qa_set_running = connect_config.cursor()
                                            cursor_qa_set_running.execute(sql_update_reference)
                                            connect_config.commit()
            """

                is_ingress_exists_table = False
                is_ingress_exec_table = True
                if connect_config:
                    print(_INGRESS_CONNECTION_TYPE)
                    if _INGRESS_CONNECTION_TYPE != "":
                        if _INGRESS_CONNECTION_TYPE.lower() == "hadoop":
                            sql_exists_table = "describe  {0}"
                            if _INGRESS_CONNECTION_STRING != "":
                                data_ingress = hadoop.connect(_INGRESS_CONNECTION_STRING, autocommit=True)
                        elif _INGRESS_CONNECTION_TYPE.lower() == "oracle":
                            sql_exists_table = "describe {0}"
                            dsn_tns_ingress = cx.makedsn(_INGRESS_PARAM_HOST, _INGRESS_PARAM_PORT,
                                                         _INGRESS_PARAM_SERVICE_NAME)
                            data_ingress = cx.connect(_INGRESS_PARAM_USER, _INGRESS_PARAM_PASSWORD,
                                                      dsn=dsn_tns_ingress)

                        if data_ingress and _INGRESS_SQL_COMMAND != "":
                            print(_INGRESS_EXISTS_TABLE)
                            print(_INGRESS_SQL_COMMAND)
                            if _INGRESS_EXISTS_TABLE != "1":
                                try:
                                    cursor_rule = data_ingress.cursor()
                                    cursor_rule.execute(_INGRESS_SQL_COMMAND)
                                    is_ingress_exec_table = True
                                except Exception as e:
                                    print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
                                    is_ingress_exec_table = False

                            if is_ingress_exec_table:
                                try:
                                    cursor_exists_table = data_ingress.cursor()
                                    cursor_exists_table.execute(sql_exists_table.format(_INGRESS_TARGET_TABLE))
                                    item_exists_table = cursor_exists_table.fetchone()
                                    is_exists_table = True
                                except Exception as e:
                                    print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
                                    is_exists_table = False

                            if is_exists_table:
                                sql_format_stat = "show table stats {0} "
                                sql_stat = sql_format_stat.format(_INGRESS_TARGET_TABLE)
                                if sql_stat != "":
                                    cursor_table_stat = data_ingress.cursor()
                                    cursor_table_stat.execute(sql_stat)
                                    item_stat = cursor_table_stat.fetchone()
                                    if len(item_stat) > 0:
                                        _STAT_ROWS = str(item_stat[0])
                                        _STAT_FILES = str(item_stat[1])
                                        _STAT_SIZE = str(item_stat[2])
                                        _STAT_BYTES = str(item_stat[3])
                                        _STAT_REPLICATION = str(item_stat[4])
                                        _STAT_FORMAT = str(item_stat[5])
                                        _STAT_INCREMENTAL = str(item_stat[6])
                                        _STAT_LOCATION = str(item_stat[7])
                                sql_count = sql_format_count.format(_INGRESS_TARGET_TABLE)
                                if sql_count != "":
                                    t_start = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                    cursor_table_count = data_ingress.cursor()
                                    cursor_table_count.execute(sql_count)
                                    item_count = cursor_table_count.fetchone()
                                    if len(item_count) > 0:
                                        _RECORD_QTY = item_count[0]
                                    t_end = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                sql_transaction = sql_format_transaction.format(_TRANSACTION_ID, _RULE_ID,
                                                                                _INGRESS_TARGET_TABLE, _RECORD_QTY,
                                                                                _STAT_ROWS, _STAT_FILES, _STAT_SIZE,
                                                                                _STAT_LOCATION, 'Pornc25', t_start,
                                                                                t_end)
                                try:
                                    print(sql_transaction)
                                    cursor_transaction = connect_config.cursor()
                                    cursor_transaction.execute(sql_transaction)
                                    connect_config.commit()
                                    _is_transaction = True
                                except Exception as e:
                                    print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
                                    _is_transaction = False

                                if _is_transaction:
                                    cursor_config_detail = connect_config.cursor()
                                    cursor_config_detail.execute(sql_config_detail)
                                    result_config_detail = cursor_config_detail.fetchall()
                                    if len(result_config_detail) > 0:
                                        for item_config_dt in result_config_detail:
                                            _BUSINESS_RULE_ID_DETAIL = str(item_config_dt[0])
                                            _BUSINESS_RULE_NAME_DETAIL = str(item_config_dt[1])
                                            _SQL_COMMAND_DETAIL = str(item_config_dt[2])
                                            _CONNECTION_NAME_DETAIL = str(item_config_dt[3])
                                            _PARAM_HOST_DETAIL = str(item_config_dt[4])
                                            _PARAM_PORT_DETAIL = str(item_config_dt[5])
                                            _PARAM_SERVICE_NAME_DETAIL = str(item_config_dt[6])
                                            _CONNECTION_TYPE_DETAIL = str(item_config_dt[7])
                                            _CONNECTION_USER_DETAIL = str(item_config_dt[8])
                                            _CONNECTION_PASSWORD_DETAIL = str(item_config_dt[9])
                                            _CONNECTION_STRING_DETAIL = str(item_config_dt[10])
                                            _QUALITY_INDEX_ID_DETAIL = str(item_config_dt[11])

                                            if _CONNECTION_TYPE_DETAIL != "":
                                                if _CONNECTION_TYPE_DETAIL.lower() == "hadoop":
                                                    if _CONNECTION_STRING_DETAIL != "":
                                                        data_dq = hadoop.connect(_CONNECTION_STRING_DETAIL,
                                                                                 autocommit=True)
                                                elif _CONNECTION_TYPE_DETAIL.lower() == "oracle":
                                                    dsn_tns_dq = cx.makedsn(_PARAM_HOST_DETAIL, _PARAM_PORT_DETAIL,
                                                                            _PARAM_SERVICE_NAME_DETAIL)
                                                    data_dq = cx.connect(_CONNECTION_USER_DETAIL,
                                                                         _CONNECTION_PASSWORD_DETAIL,
                                                                         dsn=dsn_tns_dq)
                                                if data_dq:
                                                    t_start_dt = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                                    print(_SQL_COMMAND_DETAIL)
                                                    cursor_dq_detail = data_dq.cursor()
                                                    cursor_dq_detail.execute(_SQL_COMMAND_DETAIL)
                                                    result_dq_detail = cursor_dq_detail.fetchall()
                                                    _RECORD_QTY_DETAIL = 0
                                                    if len(result_dq_detail) > 0:
                                                        _RECORD_QTY_DETAIL = len(result_dq_detail)
                                                        _VALIDATE_RECORD_QTY_DETAIL = 0
                                                        t_end_dt = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
                                                        print(
                                                            '*************** validate {0} record ************** '.format(
                                                                len(result_dq_detail)))

                                                        sql_transaction_detail = sql_format_transaction_detail.format(
                                                            _TRANSACTION_ID
                                                            , _RULE_ID
                                                            , _BUSINESS_RULE_ID_DETAIL
                                                            , _QUALITY_INDEX_ID_DETAIL
                                                            , _SQL_COMMAND_DETAIL.replace("'", "''")
                                                            , _RECORD_QTY_DETAIL
                                                            , _VALIDATE_RECORD_QTY_DETAIL
                                                            , 'Pornc25', t_start_dt, t_end_dt)
                                                        try:
                                                            print(sql_transaction_detail)
                                                            cursor_transaction_detail = connect_config.cursor()
                                                            cursor_transaction_detail.execute(sql_transaction_detail)
                                                            connect_config.commit()
                                                            _is_transaction_detail = True
                                                        except Exception as e:
                                                            print('Error! Code: {c}, Message, {m}'.format(
                                                                c=type(e).__name__, m=str(e)))
                                                            _is_transaction_detail = False

                                                        if _is_transaction_detail:
                                                            sql_transaction_value_delete = sql_format_delete_transaction.format(
                                                                _TRANSACTION_ID, _RULE_ID, _BUSINESS_RULE_ID_DETAIL,
                                                                _QUALITY_INDEX_ID_DETAIL)

                                                            try:
                                                                print(sql_transaction_value_delete)

                                                                # field_map = fields(cursor_transaction_value_del)
                                                                cursor_transaction_value_del = connect_config.cursor()
                                                                cursor_transaction_value_del.execute(
                                                                    sql_transaction_value_delete)
                                                                connect_config.commit()
                                                                _is_transaction_detail_delete = True
                                                            except Exception as e:
                                                                print('Error! Code: {c}, Message, {m}'.format(
                                                                    c=type(e).__name__, m=str(e)))
                                                                _is_transaction_detail_delete = False
                                                            if _is_transaction_detail_delete:
                                                                sql_format_select_add_value = "SELECT " \
                                                                                              " '{0}' as TRANSACTION_ID" \
                                                                                              ",'{1}' as RULE_ID" \
                                                                                              ",'{2}' as BUSINESS_RULE_ID" \
                                                                                              ", {3} as QUALITY_INDEX_ID" \
                                                                                              ", {4} as ROW_SEQ" \
                                                                                              ", {5} as COL_SEQ" \
                                                                                              ",'{6}' as COLUMN_NAME" \
                                                                                              ",'{7}' as COLUMN_VALUE" \
                                                                                              ",'{8}' as COLUMN_TYPE" \
                                                                                              ",'{9}' as PPN_BY" \
                                                                                              ",sysdate as PPN_TM" \
                                                                                              " from dual"
                                                                iseq_row = 0
                                                                iseq_col = 0
                                                                data = []
                                                                for item_dq_detail in result_dq_detail:
                                                                    _value_type = ""
                                                                    _value_name = ""
                                                                    _value_detail = ""
                                                                    iseq_row += 1
                                                                    iseq_col = 0
                                                                    for column_dp_detail in item_dq_detail:
                                                                        # item_dq_detail[field_map[column_dp_detail]]
                                                                        iseq_col += 1
                                                                        _value_name = str(column_dp_detail)
                                                                        _value_detail = str(column_dp_detail)
                                                                        _value_type = str(type(column_dp_detail))
                                                                        item = (_TRANSACTION_ID, _RULE_ID,
                                                                                _BUSINESS_RULE_ID_DETAIL,
                                                                                _QUALITY_INDEX_ID_DETAIL,
                                                                                str(iseq_row),
                                                                                str(iseq_col),
                                                                                _value_name, _value_detail,
                                                                                _value_type, "Pornc25")

                                                                        data.append(item)
                                                                        if iseq_row % int(_row_commit) == 0:
                                                                            try:
                                                                                ddl_command = sql_format_transaction_value
                                                                                print(ddl_command)
                                                                                with cx.connect(_user_db, _password_db,
                                                                                                dsn_tns_config) as connection:
                                                                                    with connection.cursor() as cursor:
                                                                                        cursor.executemany(ddl_command,
                                                                                                           data)
                                                                                        connection.commit()
                                                                            except Exception as e:
                                                                                print(
                                                                                    'Error! Code: {c}, Message, {m}'.format(
                                                                                        c=type(e).__name__, m=str(e)))
                                                                            finally:
                                                                                data = []
                                                                                gc.collect()
                                                                try:
                                                                    ddl_command = sql_format_transaction_value
                                                                    print(data)
                                                                    with cx.connect(_user_db, _password_db,
                                                                                    dsn_tns_config) as connection:
                                                                        with connection.cursor() as cursor:
                                                                            cursor.executemany(ddl_command,
                                                                                               data)
                                                                            connection.commit()
                                                                except Exception as e:
                                                                    print(
                                                                        'Error! Code: {c}, Message, {m}'.format(
                                                                            c=type(e).__name__, m=str(e)))
                                                                finally:
                                                                    data = []
                                                                    gc.collect()
"""
    except Exception as e:
        print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    os.environ["APP_HOME"] = os.path.dirname(os.path.realpath(__file__))
    os.environ["APP_PID"] = str(os.getpid())
    if "CDE" in os.environ:
        _cde_id = os.environ["CDE"]
    else:
        _cde_id = ""
    if "DB_HOST" in os.environ:
        _host_db = os.environ["DB_HOST"]
    else:
        _host_db = "dbdwhdv1"
    if "DB_PORT" in os.environ:
        _port_db = os.environ["DB_PORT"]
    else:
        _port_db = "1550"
    if "DB_SERVICE" in os.environ:
        _service_db = os.environ["DB_SERVICE"]
    else:
        _service_db = "TEDWDEV"
    if "UD" in os.environ:
        _user_db = os.environ["USER_DB"]
    else:
        _user_db = "TEDWBORAPPO"
    if "PD" in os.environ:
        _password_db = os.environ["PASSWORD"]
    else:
        _password_db = "bortedw123"

    if len(sys.argv) > 0:
        _transaction_id = sys.argv[1]
    else:
        _transaction_id = ""

    process(_host_db, _port_db, _service_db, _user_db, _password_db, _transaction_id)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
