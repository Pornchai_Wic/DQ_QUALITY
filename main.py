import os
import sys
import argparse
import datetime
import time
import pytz
from types import SimpleNamespace

import gc
import cx_Oracle as cx
import pyodbc as hadoop
import xmltodict

from elasticsearch import Elasticsearch
from elasticsearch import helpers

from kafka import KafkaConsumer
from kafka import KafkaProducer
from kafka.errors import KafkaError
from datetime import datetime, timedelta
import time
import certifi

import json

from elasticsearch.connection import create_ssl_context
import urllib3
import ssl
import gssapi
import logging

from cryptography.fernet import Fernet, InvalidToken

tz = pytz.timezone('Asia/Bangkok')
from collections import defaultdict
from xml.etree import ElementTree as ET
def xmldict(t):
    d = {t.tag: {} if t.attrib else None}
    children = list(t)
    if children:
        dd = defaultdict(list)
        for dc in map(xmldict, children):
            for k, v in dc.items():
                dd[k].append(v)
        d = {t.tag: {k: v[0] if len(v) == 1 else v
                     for k, v in dd.items()}}
    if t.text:
        text = t.text.strip()
        if children or t.attrib:
            if text:
              d[t.tag]['#text'] = text
        else:
            d[t.tag] = text
    return d

def fields(cursor):
    """ Given a DB API 2.0 cursor object that has been executed, returns
    a dictionary that maps each field name to a column index; 0 and up. """
    results = {}
    column = 0
    for d in cursor.description:
        results[d[0]] = column
        column = column + 1

    return results


def intersection_column(lst1, lst2):
    return list(set(lst1) & set(lst2))


def decryption(param: None):
    cipher_suite = None
    key = "8zNU418HIcCijV_KPfCm-OwNsVKGhMOxtl906nyzzmM="
    try:
        f = Fernet(key)
    except InvalidToken:
        f = None
    if not f is None:
        try:
            decrypted_bytes = f.decrypt(param.encode("utf-8"))
        except InvalidToken:
            decrypted_bytes = None  # TODO(kmullins): Shall we log this case? Is it expected?
        if not decrypted_bytes is None:
            decrypted_string = decrypted_bytes.decode()  # bytes -> str
        else:
            decrypted_string = "encryption is not support"
    else:
        key = "key is not support"
    return decrypted_string


def target_oracle(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT, _TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                  _TARGET_PARAM_SERVICE_NAME, _TARGET_STATIC, _TARGET_FIELD_STATIC, array_target,
                  _option_remove_column):
    ddl_format_command = "insert into {0} ({1}) values ({2})"
    ddl_format_static = "select upper(cname) as column_name, coltype, width from col where upper(tname) = upper('{0}') order by colno "
    table_target = _TARGET_STATIC
    column_target = ""
    column_source = ""
    sql_target_query = ""
    try:
        ddl_structure_target = ddl_format_static.format(_TARGET_STATIC)
        print(ddl_structure_target)
        if ddl_structure_target != "":
            dsn_tns_config = cx.makedsn(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT, service_name=_TARGET_PARAM_SERVICE_NAME)
            connect_oracle_target = cx.connect(_TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD, dsn=dsn_tns_config)
            if len(array_target) > 0:
                a_dictionary = array_target[0]
                print(a_dictionary)
            cursor_target_oracle = connect_oracle_target.cursor()
            cursor_target_oracle.execute(ddl_structure_target)
            result_target_structure = cursor_target_oracle.fetchall()
            if len(result_target_structure) > 0:
                for item_target_structure in result_target_structure:
                    target_column_name = item_target_structure[0]
                    target_column_type = item_target_structure[1]
                    target_column_width = item_target_structure[2]
                    is_find = str(target_column_name) in a_dictionary
                    is_find_remove = True
                    if _option_remove_column is not None:
                        is_find_remove = str(target_column_name) in _option_remove_column

                    if is_find and not is_find_remove:
                        if column_source != "":
                            column_source += ","
                        column_source += ":" + target_column_name
                        if column_target != "":
                            column_target += ","
                        column_target += target_column_name.upper()

                    print('Cast({0} as {1}({2})) as {0}'.format(target_column_name, target_column_type,
                                                                target_column_width))
                    if sql_target_query != "":
                        sql_target_query += ","
                    sql_target_query += 'Cast({0} as {1}({2})) as {0}'.format(target_column_name, target_column_type,
                                                                              target_column_width)
        print(column_source)
        print(column_target)
        if len(_option_remove_column) > 0 and len(array_target) > 0:
            for element in array_target:
                for ii in _option_remove_column:
                    del element[ii]

        ddl_command = ddl_format_command.format(_TARGET_STATIC, column_target, column_source)
        with cx.connect(_TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD, dsn_tns_config) as connection:
            # create a cursor
            with connection.cursor() as cursor:
                # execute the insert statement
                cursor.executemany(ddl_command, array_target)
                # commit work
                connection.commit()
    except cx.Error as error:
        print('Error occurred:')
        print(error)
        logging.error(error)
    finally:
        array_target = []
        gc.collect()


def target_elasticsearch(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT, _TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                         _TARGET_STATIC, _TARGET_FIELD_STATIC, array_target):
    urllib3.disable_warnings()

    host = _TARGET_PARAM_HOST
    port = _TARGET_PARAM_PORT
    user = _TARGET_DATABASE_USER
    password = _TARGET_DATABASE_PASSWORD
    now = datetime.now(tz)

    ssl_context = create_ssl_context(cafile='cacert-esd.pem')
    ssl_context.check_hostname = False
    ssl_context.verify_mode = ssl.CERT_OPTIONAL
    es = Elasticsearch(hosts=[{'host': host, 'port': _TARGET_PARAM_PORT}],
                       scheme="https",
                       verify_certs=True,
                       ssl_context=ssl_context,
                       http_auth=(user, password))
    if _TARGET_FIELD_STATIC == "":
        document_id = str(now.strftime('%Y%m%d'))
    else:
        document_id = _TARGET_FIELD_STATIC

    index = _TARGET_STATIC + document_id
    print(index)
    doc_list = []
    now = datetime.now(tz)
    now_timestamp = now.strftime('%Y-%m-%dT%H:%M:%S') + ('-%02d' % (now.microsecond / 10000))
    ind = 0
    topic_name = ""
    if len(array_target) > 0:
        print(str(now.isoformat()) + "=" + str(len(array_target)))
        for item_target in array_target:
            item_target['@timestamp'] = now.isoformat()
            doc_list += [item_target]

        print(index)
        if len(doc_list) > 0:
            try:
                print(doc_list)
                print(
                    "\nAttempting to index =" + index + " the list of docs using helpers.bulk(" + str(
                        len(doc_list)) + ")")
                resp = helpers.bulk(es, doc_list, index=index, doc_type='_doc')
                print("helpers.bulk " + now_timestamp + " = ", resp)
            except Exception as e:
                print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
            finally:
                es.close()


# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
def read_config(id):
    return True;


def write_log():
    return True;


def is_connection_validate(HOST, PORT, USER, PASS):
    return len(HOST) > 0 and len(PORT) > 0 and len(USER) > 0 and len(PASS) > 0


def process(aux_connect, ods_connect, syn_id, time_refresh):
    try:
        _SYN_ID = ""
        _SOURCE_ID = ""
        _TARGET_ID = ""
        _SOURCE_STATIC = ""
        _SOURCE_CUSTOMIZE = ""
        _TARGET_STATIC = ""
        _TARGET_CUSTOMIZE = ""
        _SOURCE_CONNECTION_NAME = ""
        _SOURCE_PARAM_HOST = ""
        _SOURCE_PARAM_PORT = ""
        _SOURCE_PARAM_SERVICE_NAME = ""
        _SOURCE_CONNECTION_TYPE = ""
        _SOURCE_DATABASE_USER = ""
        _SOURCE_DATABASE_PASSWORD = ""
        _TARGET_CONNECTION_NAME = ""
        _TARGET_PARAM_HOST = ""
        _TARGET_PARAM_PORT = ""
        _TARGET_PARAM_SERVICE_NAME = ""
        _TARGET_CONNECTION_TYPE = ""
        _TARGET_DATABASE_USER = ""
        _TARGET_DATABASE_PASSWORD = ""
        _SOURCE_CHECK_SCHEMA = ""
        _TARGET_CHECK_SCHEMA = ""
        _SOURCE_CHECK_SECURE = ""
        _TARGET_CHECK_SECURE = ""
        _SOURCE_FIELD_STATIC = ""
        _TARGET_FIELD_STATIC = ""
        _DATA_TYPE = ""
        _FIELD_PARTNER = "0"
        _IS_PARTNER = False

        _REFRESH_HOUR = ""
        _REFRESH_MINUTE = ""
        _RF_HOUR = "00"
        _RF_MINUTE = "00"
        _FIELD_MAPPING = "0"
        _IS_MAPPING = False
        is_have_mapping_body = False
        is_have_mapping_header = False
        is_have_option_source = False
        is_have_option_target = False
        is_have_option_synchronize = False
        _ARRAY_SYNCHRONIZE_FILTER = []
        _ITEM_SYNCHRONIZE_FILTER = []

        _ARRAY_SYNCHRONIZE_OPTION = []
        _ITEM_SYNCHRONIZE_OPTION = []
        _ARRAY_TARGET_COLUMN_OPTION = []

        _ARRAY_SOURCE_OPTION = []
        _ITEM_SOURCE_OPTION = {}
        _ARRAY_TARGET_OPTION = []
        _ITEM_TARGET_OPTION = {}
        sql_config_field_body = " select " \
                                "\n ss.SYN_ID " \
                                "\n ,ss.TOPIC" \
                                "\n ,ss.COLUMN_SOURCE" \
                                "\n ,ss.COLUMN_TARGET" \
                                "\n from DWH_CONFIG_SYNCHRONIZE_MAPPING_FIELD ss " \
                                "\n where ss.status = '0' " \
                                "\n and ss.SYN_ID = '{0}' " \
                                "\n and ss.TYPE = 'Body' " \
                                "\n order by  ss.seq "
        sql_config_field_header = " select " \
                                  "\n ss.SYN_ID " \
                                  "\n ,ss.TOPIC" \
                                  "\n ,ss.COLUMN_SOURCE" \
                                  "\n ,ss.COLUMN_TARGET" \
                                  "\n from DWH_CONFIG_SYNCHRONIZE_MAPPING_FIELD ss " \
                                  "\n where ss.status = '0' " \
                                  "\n and ss.SYN_ID = '{0}' " \
                                  "\n and ss.TYPE = 'Header' " \
                                  "\n order by  ss.seq "
        sql_config_connection_option = " select " \
                                       "\n ss.KEY" \
                                       "\n ,ss.VALUE" \
                                       "\n ,ss.ID " \
                                       "\n from dwh_config_connection_option ss " \
                                       "\n where ss.status = '0' " \
                                       "\n and ss.ID = {0} " \
                                       "\n order by  ss.ID "

        sql_synchronize_option = " select " \
                                 "\n ss.KEY" \
                                 "\n ,ss.VALUE" \
                                 "\n ,ss.SYN_ID " \
                                 "\n from dwh_config_synchronize_optional ss " \
                                 "\n where ss.status = '0' " \
                                 "\n and ss.SYN_ID = '{0}' " \
                                 "\n order by  ss.KEY "
        sql_synchronize_filter = " select " \
                                 "\n ss.SOURCE_KEY" \
                                 "\n ,ss.SOURCE_VALUE" \
                                 "\n ,ss.SYN_ID " \
                                 "\n from DWH_CONFIG_SYNCHRONIZE_FILTER ss " \
                                 "\n where ss.status = '0' " \
                                 "\n and ss.SYN_ID = '{0}' " \
                                 "\n order by  ss.SOURCE_KEY "
        sql_config = " select " \
                     "\n ss.SOURCE_ID " \
                     "\n ,ss.TARGET_ID" \
                     "\n ,ss.SOURCE_STATIC" \
                     "\n ,ss.SOURCE_CUSTOMIZE" \
                     "\n ,ss.TARGET_STATIC" \
                     "\n ,ss.TARGET_CUSTOMIZE" \
                     "\n ,s.CONNECTION_NAME as SOURCE_CONNECTION_NAME" \
                     "\n ,s.PARAM_HOST as SOURCE_PARAM_HOST" \
                     "\n ,s.PARAM_PORT as SOURCE_PARAM_PORT" \
                     "\n ,s.PARAM_SERVICE_NAME as SOURCE_PARAM_SERVICE_NAME" \
                     "\n ,s.CONNECTION_TYPE as SOURCE_CONNECTION_TYPE" \
                     "\n ,s.DATABASE_USER as SOURCE_DATABASE_USER" \
                     "\n ,s.DATABASE_PASSWORD as SOURCE_DATABASE_PASSWORD" \
                     "\n ,t.CONNECTION_NAME as TARGET_CONNECTION_NAME" \
                     "\n ,t.PARAM_HOST as TARGET_PARAM_HOST" \
                     "\n ,t.PARAM_PORT as TARGET_PARAM_PORT" \
                     "\n ,t.PARAM_SERVICE_NAME as TARGET_PARAM_SERVICE_NAME" \
                     "\n ,t.CONNECTION_TYPE as TARGET_CONNECTION_TYPE" \
                     "\n ,t.DATABASE_USER as TARGET_DATABASE_USER" \
                     "\n ,t.DATABASE_PASSWORD as TARGET_DATABASE_PASSWORD" \
                     "\n ,s.CHECK_SCHEMA as SOURCE_CHECK_SCHEMA" \
                     "\n ,t.CHECK_SCHEMA as TARGET_CHECK_SCHEMA" \
                     "\n ,ss.SOURCE_FIELD_STATIC as SOURCE_FIELD_STATIC" \
                     "\n ,ss.TARGET_FIELD_STATIC as TARGET_FIELD_STATIC" \
                     "\n ,s.CHECK_SECURITY as SOURCE_CHECK_SECURITY" \
                     "\n ,t.CHECK_SECURITY as TARGET_CHECK_SECURITY" \
                     "\n ,ss.SYN_ID" \
                     "\n ,ss.PARTNER_MAPPING" \
                     "\n ,ss.REFRESH_TIME_HOUR" \
                     "\n ,ss.REFRESH_TIME_MINUTE" \
                     "\n ,ss.FIELD_MAPPING" \
                     "\n ,ss.DATA_TYPE" \
                     "\n from DWH_CONFIG_SYNCHRONIZE ss" \
                     "\n left join DWH_CONFIG_CONNECTION s on (ss.SOURCE_ID=s.ID)" \
                     "\n left join DWH_CONFIG_CONNECTION t on (ss.TARGET_ID=T.ID)" \
                     "\n where ss.status='0'" \
                     "\n and ss.SYN_ID = '{0}'" \
                     "\n and rownum = 1"

        dsn_tns_config = cx.makedsn(aux_connect.host, aux_connect.port, service_name=aux_connect.service)
        connect_config = cx.connect(aux_connect.user, aux_connect.password, dsn=dsn_tns_config)
        print(connect_config)
        print(sql_config.format(_syn_id))
        print(sql_config_field_body.format(_syn_id))
        print(sql_config_field_header.format(_syn_id))
        if connect_config:
            ods_connector = _ods_connector

            cursor_config_mapping = connect_config.cursor()
            cursor_config_mapping.execute(sql_config_field_body.format(_syn_id))
            result_config_mapping = cursor_config_mapping.fetchall()
            is_have_mapping_body = len(result_config_mapping) > 0

            cursor_config_mapping = connect_config.cursor()
            cursor_config_mapping.execute(sql_config_field_header.format(_syn_id))
            result_config_mapping_header = cursor_config_mapping.fetchall()
            is_have_mapping_header = len(result_config_mapping_header) > 0

            cursor_config = connect_config.cursor()
            cursor_config.execute(sql_config.format(_syn_id))
            result_config = cursor_config.fetchall()
            if len(result_config) > 0:
                for item_config in result_config:
                    _SOURCE_ID = str(item_config[0])
                    _TARGET_ID = str(item_config[1])
                    _SOURCE_STATIC = str(item_config[2])
                    _SOURCE_CUSTOMIZE = str(item_config[3])
                    _TARGET_STATIC = str(item_config[4])
                    _TARGET_CUSTOMIZE = str(item_config[5])
                    _SOURCE_CONNECTION_NAME = str(item_config[6])
                    _SOURCE_PARAM_HOST = str(item_config[7])
                    _SOURCE_PARAM_PORT = str(item_config[8])
                    _SOURCE_PARAM_SERVICE_NAME = str(item_config[9])
                    _SOURCE_CONNECTION_TYPE = str(item_config[10])
                    _SOURCE_DATABASE_USER = str(item_config[11])
                    _SOURCE_DATABASE_PASSWORD = str(item_config[12])
                    _TARGET_CONNECTION_NAME = str(item_config[13])
                    _TARGET_PARAM_HOST = str(item_config[14])
                    _TARGET_PARAM_PORT = str(item_config[15])
                    _TARGET_PARAM_SERVICE_NAME = str(item_config[16])
                    _TARGET_CONNECTION_TYPE = str(item_config[17])
                    _TARGET_DATABASE_USER = str(item_config[18])
                    _TARGET_DATABASE_PASSWORD = str(item_config[19])
                    _SOURCE_CHECK_SCHEMA = str(item_config[20])
                    _TARGET_CHECK_SCHEMA = str(item_config[21])
                    _SOURCE_FIELD_STATIC = str(item_config[22])
                    _TARGET_FIELD_STATIC = str(item_config[23])
                    _SOURCE_CHECK_SECURE = str(item_config[24])
                    _TARGET_CHECK_SECURE = str(item_config[25])
                    _SYN_ID = str(item_config[26])
                    _FIELD_PARTNER = str(item_config[27])
                    _REFRESH_HOUR = str(item_config[28])
                    _REFRESH_MINUTE = str(item_config[29])
                    _FIELD_MAPPING = str(item_config[30])
                    _DATA_TYPE = str(item_config[31])
                    if _FIELD_PARTNER.isnumeric():
                        _IS_PARTNER = int(_FIELD_PARTNER) == 1

                    if _FIELD_MAPPING.isnumeric():
                        _IS_MAPPING = int(_FIELD_MAPPING) == 1

                    if _REFRESH_HOUR.isnumeric():
                        if 0 < int(_REFRESH_HOUR) < 25:
                            _RF_HOUR = str(int(_REFRESH_HOUR)).zfill(2)
                        else:
                            _RF_HOUR = "00"

                    if _REFRESH_MINUTE.isnumeric():
                        if 0 < int(_REFRESH_MINUTE) < 60:
                            _RF_MINUTE = str(int(_REFRESH_MINUTE)).zfill(2)
                        else:
                            _RF_MINUTE = "00"

                    is_source = (is_connection_validate(_SOURCE_PARAM_HOST, _SOURCE_PARAM_PORT, _SOURCE_DATABASE_USER,
                                                        _SOURCE_DATABASE_PASSWORD))
                    is_target = (is_connection_validate(_SOURCE_PARAM_HOST, _SOURCE_PARAM_PORT, _SOURCE_DATABASE_USER,
                                                        _SOURCE_DATABASE_PASSWORD))
                    is_source_schema = _SOURCE_CHECK_SCHEMA == "1"
                    is_target_schema = _TARGET_CHECK_SCHEMA == "1"

                    cursor_config_connection_source = connect_config.cursor()
                    cursor_config_connection_source.execute(sql_config_connection_option.format(_SOURCE_ID))
                    result_config_source_option = cursor_config_connection_source.fetchall()
                    is_have_option_source = len(result_config_source_option) > 0
                    if len(result_config_source_option) > 0:
                        for item_source in result_config_source_option:
                            _ITEM_SOURCE_OPTION = {"key": str(item_source[0]), "value": str(item_source[1])}
                            _ARRAY_SOURCE_OPTION.append(_ITEM_SOURCE_OPTION)

                    cursor_config_connection_target = connect_config.cursor()
                    cursor_config_connection_target.execute(sql_config_connection_option.format(_TARGET_ID))
                    result_config_target_option = cursor_config_connection_target.fetchall()
                    is_have_option_target = len(result_config_target_option) > 0
                    if len(result_config_target_option) > 0:
                        for item_target in result_config_target_option:
                            _ITEM_TARGET_OPTION = {"key": str(item_target[0]), "value": str(item_target[1])}
                            _ARRAY_TARGET_OPTION.append(_ITEM_TARGET_OPTION)
                    print(sql_synchronize_option.format(_SYN_ID))

                    cursor_synchronize_option = connect_config.cursor()
                    cursor_synchronize_option.execute(sql_synchronize_option.format(_SYN_ID))
                    result_synchronize_option = cursor_synchronize_option.fetchall()
                    is_have_option_synchronize = len(result_synchronize_option) > 0
                    if len(result_synchronize_option) > 0:
                        for item_synchronize in result_synchronize_option:
                            _ITEM_SYNCHRONIZE_OPTION = {"key": str(item_synchronize[0]),
                                                        "value": str(item_synchronize[1])}
                            _ARRAY_SYNCHRONIZE_OPTION.append(_ITEM_SYNCHRONIZE_OPTION)
                    print(sql_synchronize_filter.format(_SYN_ID))

                    cursor_synchronize_filter = connect_config.cursor()
                    cursor_synchronize_filter.execute(sql_synchronize_filter.format(_SYN_ID))
                    result_synchronize_filter = cursor_synchronize_filter.fetchall()
                    is_have_option_filter = len(result_synchronize_filter) > 0
                    if len(result_synchronize_filter) > 0:
                        for item_synchronize_filter in result_synchronize_filter:
                            _ITEM_SYNCHRONIZE_FILTER = {"key": str(item_synchronize_filter[0]),
                                                        "value": str(item_synchronize_filter[1])}
                            _ARRAY_SYNCHRONIZE_FILTER.append(_ITEM_SYNCHRONIZE_FILTER)

                    if is_source:
                        if _SOURCE_CONNECTION_TYPE.lower() == "oracle":

                            dsn_tns_source = cx.makedsn(_SOURCE_PARAM_HOST, _SOURCE_PARAM_PORT,
                                                        service_name=_SOURCE_PARAM_SERVICE_NAME)
                            source_connection = cx.connect(_SOURCE_DATABASE_USER, _SOURCE_DATABASE_PASSWORD,
                                                           dsn=dsn_tns_source)
                            print(source_connection)
                            batch_size = 500
                            commit_row = 500
                            sql_format_source = ""
                            sql_source = ""
                            field_source_group = ""
                            field_source_qty = 0
                            data_source_target = []
                            array_target = []
                            iseq = 0
                            isNotGrouping = str(_SOURCE_FIELD_STATIC).upper() == "NONE"
                            if _SOURCE_STATIC is not None:
                                if isNotGrouping:
                                    sql_format_source = "select  count(*) as qty from {0}"
                                    sql_source_count = sql_format_source.format(_SOURCE_STATIC)
                                else:
                                    sql_format_source = "select {1},count(*) as qty from {0} group by {1} order by {" \
                                                        "1} desc "
                                    sql_source_count = sql_format_source.format(_SOURCE_STATIC, _SOURCE_FIELD_STATIC)
                            else:
                                sql_source_count = _SOURCE_CUSTOMIZE

                            if isNotGrouping:
                                sql_format_truncate = "truncate table {0}"
                                sql_truncate = ""

                                if len(_ARRAY_SYNCHRONIZE_FILTER) > 0:
                                    sql_format_source_where = "select * from {0} where 1=1 "
                                    for item_syn_filter in _ARRAY_SYNCHRONIZE_FILTER:
                                        key_filter = str(item_syn_filter["key"])  # KEY
                                        value_filter = str(item_syn_filter["value"])  # VALUE
                                        print('key = {0} , value = {1} '.format(key_filter, value_filter))
                                        sql_format_source_where += " and {0} = {1}".format(key_filter, value_filter)
                                    sql_source_where = sql_format_source_where.format(_SOURCE_STATIC)
                                else:
                                    sql_format_source_where = "select * from {0}"
                                    sql_source_where = sql_format_source_where.format(_SOURCE_STATIC)
                                print(sql_source_where)
                                _TRUNCATE_BEFORE_LOAD = False
                                if len(_ARRAY_SYNCHRONIZE_OPTION) > 0:
                                    for item_syn_option in _ARRAY_SYNCHRONIZE_OPTION:
                                        key = str(item_syn_option["key"])  # KEY
                                        value = str(item_syn_option["value"])  # VALUE
                                        print('key = {0} , value = {1} '.format(key, value))
                                        if key.upper() == "TRUNCATE":
                                            _TRUNCATE_BEFORE_LOAD = (value.lower() == "true")
                                            if _TRUNCATE_BEFORE_LOAD:
                                                sql_truncate = sql_format_truncate.format(_TARGET_STATIC)
                                                dsn_tns_config_target = cx.makedsn(_TARGET_PARAM_HOST,
                                                                                   _TARGET_PARAM_PORT,
                                                                                   service_name=_TARGET_PARAM_SERVICE_NAME)
                                                connect_oracle_target = cx.connect(_TARGET_DATABASE_USER,
                                                                                   _TARGET_DATABASE_PASSWORD,
                                                                                   dsn=dsn_tns_config_target)
                                                cursor_target_oracle = connect_oracle_target.cursor()
                                                cursor_target_oracle.execute(sql_truncate)
                                                connect_oracle_target.commit()
                                        elif key.upper() == "REMOVE_SOURCE":
                                            _ARRAY_TARGET_COLUMN_OPTION = value.split(",")

                                cursor_source_oracle = source_connection.cursor()
                                cursor_source_oracle.execute(sql_source_where)
                                field_map = fields(cursor_source_oracle)
                                rows = cursor_source_oracle.fetchmany(batch_size)
                                if not rows:
                                    break
                                for row in rows:
                                    iseq += 1
                                    item = []
                                    item_target = {}
                                    for col in field_map:
                                        value = row[field_map[col]]
                                        item.append(value)
                                        item_target[col] = row[field_map[col]]
                                    data_source_target.append(tuple(item))
                                    array_target.append(item_target)

                                    if iseq % commit_row == 0:
                                        if is_target:
                                            if _TARGET_CONNECTION_TYPE.lower() == "oracle":
                                                target_oracle(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT,
                                                              _TARGET_DATABASE_USER,
                                                              _TARGET_DATABASE_PASSWORD,
                                                              _TARGET_PARAM_SERVICE_NAME,
                                                              _TARGET_STATIC,
                                                              str(field_source_group), array_target,
                                                              _ARRAY_TARGET_COLUMN_OPTION)
                            else:
                                print(sql_source_count)
                                cursor_source_group_oracle = source_connection.cursor()
                                cursor_source_group_oracle.execute(sql_source_count)
                                result_source_group = cursor_source_group_oracle.fetchall()
                                for item_group in result_source_group:
                                    field_source_group = item_group[0]
                                    field_source_qty = item_group[1]

                                    sql_format_source_where = "select * from {0} where {1} = {2} "
                                    sql_source_where = " "

                                    while True:
                                        time.sleep(600)

                                        current_time = datetime.now(tz)
                                        _time_current = current_time.strftime('%Y%m%d')

                                        sql_source_where = sql_format_source_where.format(_SOURCE_STATIC,
                                                                                          _SOURCE_FIELD_STATIC,
                                                                                          _time_current)
                                        print(sql_source_where + "; quantity " + str(field_source_qty) + " rows.")
                                        cursor_source_oracle = source_connection.cursor()
                                        cursor_source_oracle.execute(sql_source_where)
                                        field_map = fields(cursor_source_oracle)

                                        current_time = datetime.now(tz)
                                        print('time.{0}  waiting message ...  '.format(str(current_time)))
                                        current_time_string = current_time.strftime('%Y%m%d%H%m')
                                        refresh_time_string = current_time.strftime('%Y%m%d') + _RF_HOUR + _RF_MINUTE
                                        if current_time_string == refresh_time_string:
                                            if not os.path.exists(current_time_string):
                                                ods_connector = ods_connection(_ods_connector.host, _ods_connector.port,
                                                                               _ods_connector.service,
                                                                               _ods_connector.user,
                                                                               _ods_connector.password)
                                                file = open(current_time_string, "w")
                                                file.write(str(ods_connector.len_record))
                                                file.close
                                        # fetch rows

                                        rows = cursor_source_oracle.fetchmany(batch_size)
                                        if not rows:
                                            break
                                        for row in rows:
                                            iseq += 1
                                            item = []
                                            item_target = {}
                                            for col in field_map:
                                                value = row[field_map[col]]
                                                # print(col, ":", row[field_map[col]])
                                                item.append(value)
                                                item_target[col] = row[field_map[col]]
                                            data_source_target.append(tuple(item))
                                            array_target.append(item_target)

                                            if iseq % commit_row == 0:
                                                if is_target:
                                                    if _TARGET_CONNECTION_TYPE.lower() == "elasticsearch":
                                                        try:
                                                            target_elasticsearch(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT,
                                                                                 _TARGET_DATABASE_USER,
                                                                                 _TARGET_DATABASE_PASSWORD,
                                                                                 _TARGET_STATIC,
                                                                                 str(field_source_group), array_target)
                                                            print("commit {0} rows.".format(str(iseq)))
                                                            iseq = 0
                                                            # establish a new connection
                                                        except cx.Error as error:
                                                            print('Error occurred:')
                                                            print(error)
                                                        finally:
                                                            data_source_target = []
                                                            array_target = []
                                                            gc.collect()
                                                    elif _TARGET_CONNECTION_TYPE.lower() == "oracle":
                                                        try:
                                                            target_oracle(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT,
                                                                          _TARGET_DATABASE_USER,
                                                                          _TARGET_DATABASE_PASSWORD,
                                                                          _TARGET_PARAM_SERVICE_NAME,
                                                                          _TARGET_STATIC,
                                                                          str(field_source_group), array_target)
                                                            print("commit {0} rows.".format(str(iseq)))
                                                            iseq = 0
                                                            # establish a new connection
                                                        except cx.Error as error:
                                                            print('Error occurred:')
                                                            print(error)
                                                        finally:
                                                            data_source_target = []
                                                            array_target = []
                                                            gc.collect()

                            if is_target:
                                if _TARGET_CONNECTION_TYPE.lower() == "elasticsearch":
                                    try:
                                        target_elasticsearch(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT,
                                                             _TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                                                             _TARGET_STATIC, str(field_source_group), array_target)
                                    except cx.Error as error:
                                        print('Error occurred:')
                                        print(error)
                                    finally:
                                        data_source_target = []
                                        array_target = []
                                        gc.collect()
                                        counter = 0
                                elif _TARGET_CONNECTION_TYPE.lower() == "oracle":
                                    try:
                                        target_oracle(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT,
                                                      _TARGET_DATABASE_USER,
                                                      _TARGET_DATABASE_PASSWORD,
                                                      _TARGET_PARAM_SERVICE_NAME,
                                                      _TARGET_STATIC,
                                                      str(field_source_group), array_target,
                                                      _ARRAY_TARGET_COLUMN_OPTION)
                                        print("commit {0} rows.".format(str(iseq)))
                                        iseq = 0
                                        # establish a new connection
                                    except cx.Error as error:
                                        print('Error occurred:')
                                        print(error)
                                    finally:
                                        data_source_target = []
                                        array_target = []
                                        gc.collect()

                        elif _SOURCE_CONNECTION_TYPE.lower() == "hadoop":
                            dsn_tns_source = cx.makedsn(_SOURCE_PARAM_HOST, _SOURCE_PARAM_PORT,
                                                        _SOURCE_PARAM_SERVICE_NAME)
                            source_connection = cx.connect(_SOURCE_DATABASE_USER, _SOURCE_DATABASE_PASSWORD,
                                                           dsn=dsn_tns_source)
                        elif _SOURCE_CONNECTION_TYPE.lower() == "kafka":
                            if _SOURCE_CHECK_SECURE == "0":
                                try:
                                    print("Creating new kafka consumer using brokers: " +
                                          str(_SOURCE_PARAM_HOST) + ' and topic ' +
                                          _SOURCE_STATIC)
                                    array_target = []
                                    _CA_FILE = 'tykb-ca.pem'
                                    # _GROUP_ID = 'geo-spc-sales'
                                    _SASL_MECHANISM = 'GSSAPI'
                                    _ENABLE_AUTO_COMMIT = False
                                    _SSL_CHECK_HOSTNAME = True
                                    _SECURITY_PROTOCOL = 'SASL_SSL'
                                    _AUTO_OFFSET_RESET = 'earliest'
                                    _SASL_KERBEROS_SERVICE_NAME = 'kafka'
                                    _CONSUMER_TIMEOUT_MS = 60000
                                    cipher = "DHE-DSS-AES256-GCM-SHA384"

                                    if is_have_option_source:
                                        key = ""
                                        value = ""
                                        for item_option in _ARRAY_SOURCE_OPTION:
                                            key = str(item_option["key"])  # KEY
                                            value = str(item_option["value"])  # VALUE
                                            if key.lower() == "cafile":
                                                _CA_FILE = value
                                            elif key.lower() == "sasl_kerberos_service_name":
                                                _SASL_KERBEROS_SERVICE_NAME = value
                                            elif key.lower() == "auto_offset_reset":
                                                _AUTO_OFFSET_RESET = value
                                            elif key.lower() == "enable_auto_commit":
                                                _ENABLE_AUTO_COMMIT = bool(value)
                                            elif key.lower() == "cipher":
                                                cipher = value
                                        print(_CA_FILE)
                                        print(_SASL_KERBEROS_SERVICE_NAME)
                                        print(_AUTO_OFFSET_RESET)
                                        print(_ENABLE_AUTO_COMMIT)

                                    consumer = KafkaConsumer(bootstrap_servers=_SOURCE_PARAM_HOST.split(","),enable_auto_commit=_ENABLE_AUTO_COMMIT,
                                                             auto_offset_reset=_AUTO_OFFSET_RESET,consumer_timeout_ms=_CONSUMER_TIMEOUT_MS,api_version=(0, 10))
                                    print(consumer)
                                    if consumer:
                                        consumer.subscribe(_SOURCE_STATIC.split(","))
                                        print('*********************** ' + _SOURCE_STATIC + ' ***********')
                                        while True:
                                            current_time = datetime.now(tz)
                                            print('time.{0}  waiting message ...  '.format(str(current_time)))
                                            current_time_string = current_time.strftime('%Y%m%d%H%M')
                                            refresh_time_string = current_time.strftime(
                                                '%Y%m%d') + _RF_HOUR + _RF_MINUTE
                                            if current_time_string == refresh_time_string:
                                                if not os.path.exists(current_time_string):
                                                    ods_connector = ods_connection(_ods_connector.host,
                                                                                   _ods_connector.port,
                                                                                   _ods_connector.service,
                                                                                   _ods_connector.user,
                                                                                   _ods_connector.password)
                                                    file = open(current_time_string, "w")
                                                    file.write(str(ods_connector.len_record))
                                                    file.close
                                            HEADERS = []
                                            for message in consumer:
                                                item_target = {}
                                                HEADERS = message.headers
                                                print('****************** header *******************')
                                                print(HEADERS)
                                                for item_header in HEADERS:
                                                    if is_have_mapping_header and _IS_MAPPING:
                                                        for ii_header in result_config_mapping_header:
                                                            _mapping_syn_id = item_field[0]
                                                            _mapping_topic = item_field[1]
                                                            _mapping_source_field = item_field[2]
                                                            _mapping_target_field = item_field[3]
                                                            if str(message.topic).lower() == str(
                                                                    _mapping_topic).lower():
                                                                if _mapping_source_field != "":
                                                                    if str(_mapping_source_field).lower() == str(
                                                                            item_header[0]).lower():
                                                                        item_target[_mapping_target_field] = \
                                                                            item_header[1].decode('UTF-8')
                                                print('****************** header *******************')
                                                item_target_header = {}
                                                my_bytes_value = message.value
                                                my_json = my_bytes_value.decode('utf8')
                                                item_source = json.loads(my_json)
                                                if item_source:
                                                    if is_have_mapping_body and _IS_MAPPING:
                                                        for item_field in result_config_mapping:
                                                            _mapping_syn_id = item_field[0]
                                                            _mapping_topic = item_field[1]
                                                            _mapping_source_field = item_field[2]
                                                            _mapping_target_field = item_field[3]
                                                            if str(message.topic).lower() == str(
                                                                    _mapping_topic).lower():
                                                                if _mapping_source_field != "":
                                                                    try:
                                                                        item_field_value = ""
                                                                        _exec_source_code = "item_source" + _mapping_source_field
                                                                        json_mapping_dynamic_key = ""
                                                                        local_dic = locals()
                                                                        exec(
                                                                            "json_mapping_dynamic_key =  " + _exec_source_code,
                                                                            globals(), local_dic)
                                                                        json_mapping_dynamic_key = local_dic[
                                                                            "json_mapping_dynamic_key"]
                                                                        item_target[
                                                                            _mapping_target_field] = json_mapping_dynamic_key
                                                                    except Exception as e:
                                                                        print('Error! Code: {c}, Message, {m}'.format(
                                                                            c=type(e).__name__, m=str(e)))

                                                    else:
                                                        for key in item_source:
                                                            print(key, ":", item_source[key])
                                                            item_target[key] = item_source[key]
                                                    print(item_target)
                                                    now = datetime.now(tz)
                                                    topic_time = now.strftime(_TARGET_FIELD_STATIC)
                                                    array_target.append(item_target)
                                                    with open('{0}.json'.format(message.topic), 'w') as f:
                                                        json.dump(json.dumps(array_target), f)
                                                    if is_target:
                                                        if _TARGET_CONNECTION_TYPE.lower() == "elasticsearch":
                                                            try:
                                                                target_elasticsearch(_TARGET_PARAM_HOST,
                                                                                     _TARGET_PARAM_PORT,
                                                                                     _TARGET_DATABASE_USER,
                                                                                     _TARGET_DATABASE_PASSWORD,
                                                                                     _TARGET_STATIC,
                                                                                     topic_time,
                                                                                     array_target)
                                                            except cx.Error as error:
                                                                print('Error occurred:')
                                                                print(error)
                                                            finally:
                                                                data_source_target = []
                                                                array_target = []
                                                                gc.collect()
                                except Exception as e:
                                    print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
                            elif _SOURCE_CHECK_SECURE == "1":
                                try:
                                    print("Creating new kafka consumer using brokers: " +
                                          str(_SOURCE_PARAM_HOST) + ' and topic ' +
                                          _SOURCE_STATIC)
                                    iseq = 0
                                    _CA_FILE = 'tykb-ca.pem'
                                    # _GROUP_ID = 'geo-spc-sales'
                                    _SASL_MECHANISM = 'GSSAPI'
                                    _ENABLE_AUTO_COMMIT = False
                                    _SSL_CHECK_HOSTNAME = True
                                    _SECURITY_PROTOCOL = 'SASL_SSL'
                                    _AUTO_OFFSET_RESET = 'earliest'
                                    _SASL_KERBEROS_SERVICE_NAME = 'kafka'
                                    _CONSUMER_TIMEOUT_MS = 60000
                                    cipher = "DHE-DSS-AES256-GCM-SHA384"
                                    if is_have_option_source:
                                        key = ""
                                        value = ""
                                        for item_option in _ARRAY_SOURCE_OPTION:
                                            key = str(item_option["key"])  # KEY
                                            value = str(item_option["value"])  # VALUE
                                            if key.lower() == "cafile":
                                                _CA_FILE = value
                                            elif key.lower() == "sasl_kerberos_service_name":
                                                _SASL_KERBEROS_SERVICE_NAME = value
                                            elif key.lower() == "auto_offset_reset":
                                                _AUTO_OFFSET_RESET = value
                                            elif key.lower() == "enable_auto_commit":
                                                _ENABLE_AUTO_COMMIT = bool(value)
                                            elif key.lower() == "cipher":
                                                cipher = value
                                        print(_CA_FILE)
                                        print(_SASL_KERBEROS_SERVICE_NAME)
                                        print(_AUTO_OFFSET_RESET)
                                        print(_ENABLE_AUTO_COMMIT)

                                    ssl_context = ssl.create_default_context(cafile=_CA_FILE)
                                    ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
                                    if cipher != "":
                                        ssl_context.set_ciphers(cipher)

                                    consumer = KafkaConsumer(bootstrap_servers=_SOURCE_PARAM_HOST.split(","),
                                                             ssl_context=ssl_context,
                                                             # group_id=_GROUP_ID,
                                                             sasl_mechanism=_SASL_MECHANISM,
                                                             enable_auto_commit=_ENABLE_AUTO_COMMIT,
                                                             ssl_check_hostname=_SSL_CHECK_HOSTNAME,
                                                             security_protocol=_SECURITY_PROTOCOL,
                                                             auto_offset_reset=_AUTO_OFFSET_RESET,
                                                             sasl_kerberos_service_name=_SASL_KERBEROS_SERVICE_NAME,
                                                             consumer_timeout_ms=_CONSUMER_TIMEOUT_MS,
                                                             # value_deserializer=lambda x: x.decode('utf-8'),
                                                             # value_deserializer=json.loads,
                                                             api_version=(0, 10))
                                    if consumer:
                                        consumer.subscribe(_SOURCE_STATIC.split(","))
                                        print('*********************** ' + _SOURCE_STATIC + ' ***********')
                                        while True:
                                            current_time = datetime.now(tz)
                                            print('time.{0}  waiting message ...  '.format(str(current_time)))
                                            current_time_string = current_time.strftime('%Y%m%d%H%M')
                                            refresh_time_string = current_time.strftime(
                                                '%Y%m%d') + _RF_HOUR + _RF_MINUTE
                                            if current_time_string == refresh_time_string:
                                                if not os.path.exists(current_time_string):
                                                    ods_connector = ods_connection(_ods_connector.host,
                                                                                   _ods_connector.port,
                                                                                   _ods_connector.service,
                                                                                   _ods_connector.user,
                                                                                   _ods_connector.password)
                                                    file = open(current_time_string, "w")
                                                    file.write(str(ods_connector.len_record))
                                                    file.close

                                            msg_pack = consumer.poll(timeout_ms=10)
                                            array_target = []
                                            array_target_header = []
                                            HEADERS = []
                                            for message in consumer:
                                                item_target = {}
                                                print('****************** header *******************')
                                                print(HEADERS)
                                                for item_header in HEADERS:
                                                    value_header = item_header[1].decode('UTF-8')
                                                    if len(_ARRAY_SYNCHRONIZE_FILTER) > 0:
                                                        for _item_filter in _ARRAY_SYNCHRONIZE_FILTER:
                                                            print(_item_filter)
                                                    if is_have_mapping_header and _IS_MAPPING:
                                                        for ii_header in result_config_mapping_header:
                                                            _mapping_syn_id = item_field[0]
                                                            _mapping_topic = item_field[1]
                                                            _mapping_source_field = item_field[2]
                                                            _mapping_target_field = item_field[3]
                                                            if str(message.topic).lower() == str(
                                                                    _mapping_topic).lower():
                                                                if _mapping_source_field != "":
                                                                    if str(_mapping_source_field).lower() == str(
                                                                            item_header[0]).lower():
                                                                        item_target[_mapping_target_field] = \
                                                                            item_header[1].decode('UTF-8')
                                                print('****************** header *******************')
                                                """
                                                if item_source_header:
                                                    for key_header in item_source_header:
                                                        item_target_header[key_header] = item_source_header[key_header]
                                                        if key_header.lower() == "dealercode":  # config database
                                                            print(' ******** dealrer ********')
                                                            print(ods_connector.lookup_partner(item_source_header[key_header]))
                                                    array_target_header.append(item_source_header)
                                                """
                                                my_bytes_value = message.value
                                                my_json = my_bytes_value.decode('utf8')
                                                if _DATA_TYPE == "XML":
                                                    print(_DATA_TYPE)
                                                    xml_data = ET.XML(my_json)
                                                    d = xmldict(xml_data)
                                                    item_xml = json.dumps(d)
                                                    item_source = item_xml.replace("{http://services.omx.truecorp.co.th/OrderStatusQRun}","").replace("{http://services.omx.truecorp.co.th/ProcessConfig}","")
                                                    json_dict = json.loads(item_source)
                                                    for key in json_dict:
                                                        item_target = json_dict[key]
                                                    array_target.append(item_target)
                                                    now = datetime.now(tz)
                                                    if _TARGET_FIELD_STATIC is not None:
                                                        topic_time = now.strftime(_TARGET_FIELD_STATIC)
                                                    else:
                                                        topic_time = now.strftime("%Y%m")

                                                    if is_target:
                                                        if _TARGET_CONNECTION_TYPE.lower() == "elasticsearch":
                                                            try:
                                                                target_elasticsearch(_TARGET_PARAM_HOST,
                                                                                     _TARGET_PARAM_PORT,
                                                                                     _TARGET_DATABASE_USER,
                                                                                     _TARGET_DATABASE_PASSWORD,
                                                                                     _TARGET_STATIC,
                                                                                     topic_time,
                                                                                     array_target)
                                                            except cx.Error as error:
                                                                print('Error occurred:')
                                                                print(error)
                                                            finally:
                                                                data_source_target = []
                                                                array_target = []
                                                                gc.collect()
                                                else:
                                                    item_source = json.loads(my_json)
                                                    if item_source:
                                                        if is_have_mapping_body and _IS_MAPPING:
                                                            for item_field in result_config_mapping:
                                                                _mapping_syn_id = item_field[0]
                                                                _mapping_topic = item_field[1]
                                                                _mapping_source_field = item_field[2]
                                                                _mapping_target_field = item_field[3]
                                                                if str(message.topic).lower() == str(
                                                                        _mapping_topic).lower():
                                                                    if _mapping_source_field != "":
                                                                        try:
                                                                            item_field_value = ""
                                                                            _exec_source_code = "item_source" + _mapping_source_field
                                                                            json_mapping_dynamic_key = ""
                                                                            local_dic = locals()
                                                                            exec(
                                                                                "json_mapping_dynamic_key =  " + _exec_source_code,
                                                                                globals(), local_dic)
                                                                            json_mapping_dynamic_key = local_dic[
                                                                                "json_mapping_dynamic_key"]
                                                                            item_target[
                                                                                _mapping_target_field] = json_mapping_dynamic_key
                                                                        except Exception as e:
                                                                            print('Error! Code: {c}, Message, {m}'.format(
                                                                                c=type(e).__name__, m=str(e)))
                                                            print(item_target)
                                                        else:
                                                            for key in item_source:
                                                                item_target[key] = item_source[key]
                                                                if len(_ARRAY_SYNCHRONIZE_FILTER) > 0:
                                                                    for _item_filter in _ARRAY_SYNCHRONIZE_FILTER:
                                                                        print(_item_filter)
                                                                if key.lower() == "dealercode":  # config database
                                                                    print(' ******** dealrer ********')
                                                                    print(ods_connector.lookup_partner(item_source[key]))

                                                        array_target.append(item_target)
                                                        now = datetime.now(tz)
                                                        if _TARGET_FIELD_STATIC != "":
                                                            topic_time = now.strftime(_TARGET_FIELD_STATIC)
                                                        else:
                                                            topic_time = now.strftime("%Y%m")

                                                        with open('{0}.json'.format(message.topic), 'w') as f:
                                                            json.dump(array_target, f)
                                                        if is_target:
                                                            if _TARGET_CONNECTION_TYPE.lower() == "elasticsearch":
                                                                try:
                                                                    target_elasticsearch(_TARGET_PARAM_HOST,
                                                                                         _TARGET_PARAM_PORT,
                                                                                         _TARGET_DATABASE_USER,
                                                                                         _TARGET_DATABASE_PASSWORD,
                                                                                         _TARGET_STATIC,
                                                                                         topic_time,
                                                                                         array_target)
                                                                except cx.Error as error:
                                                                    print('Error occurred:')
                                                                    print(error)
                                                                finally:
                                                                    data_source_target = []
                                                                    array_target = []
                                                                    gc.collect()
                                except Exception as e:
                                    print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
                        elif _SOURCE_CONNECTION_TYPE.lower() == "elasticsearch":
                            ssl_context = create_ssl_context(cafile='cacert-esd.pem')
                            _size_hits = 1000
                            if is_have_option_source:
                                key = ""
                                value = ""
                                for item_option in _ARRAY_SOURCE_OPTION:
                                    key = str(item_option["key"])  # KEY
                                    value = str(item_option["value"])  # VALUE
                                    if key.lower() == "size":
                                        _size_hits = int(value)
                                print(_size_hits)

                            ssl_context.check_hostname = False
                            ssl_context.verify_mode = ssl.CERT_OPTIONAL
                            es = Elasticsearch(hosts=[{'host': _SOURCE_PARAM_HOST, 'port': _TARGET_PARAM_PORT}],
                                               scheme="https",
                                               verify_certs=True,
                                               ssl_context=ssl_context,
                                               http_auth=(_SOURCE_DATABASE_USER, _SOURCE_DATABASE_PASSWORD))

                            res = es.search(index=_SOURCE_STATIC, query={"match_all": {}}, size=_size_hits)
                            print(res)
                            print("Got %d Hits:" % res['hits']['total']['value'])
                            array_target = []

                            for hit in res['hits']['hits']:
                                item_target = {}
                                if is_have_mapping_body and _IS_MAPPING:
                                    print(hit["_source"])
                                    item_target["_source"] = hit["_source"]
                                else:
                                    print(hit["_source"])
                                    item_target["_source"] = hit["_source"]
                                array_target.append(item_target)

                            if is_target:
                                if _TARGET_CONNECTION_TYPE.lower() == "elasticsearch":
                                    now = datetime.now(tz)
                                    topic_time = now.strftime(_TARGET_FIELD_STATIC)
                                    try:
                                        target_elasticsearch(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT,
                                                             _TARGET_DATABASE_USER,
                                                             _TARGET_DATABASE_PASSWORD,
                                                             _TARGET_STATIC,
                                                             topic_time,
                                                             array_target)
                                    except cx.Error as error:
                                        print('Error occurred:')
                                        print(error)
                                    finally:
                                        data_source_target = []
                                        array_target = []
                                        gc.collect()

                        elif _SOURCE_CONNECTION_TYPE.lower() == "txtfile":
                            dsn_tns_source = cx.makedsn(_SOURCE_PARAM_HOST, _SOURCE_PARAM_PORT,
                                                        _SOURCE_PARAM_SERVICE_NAME)
                            source_connection = cx.connect(_SOURCE_DATABASE_USER, _SOURCE_DATABASE_PASSWORD,
                                                           dsn=dsn_tns_source)
                        elif _SOURCE_CONNECTION_TYPE.lower() == "csvfile":
                            dsn_tns_source = cx.makedsn(_SOURCE_PARAM_HOST, _SOURCE_PARAM_PORT,
                                                        _SOURCE_PARAM_SERVICE_NAME)
                            source_connection = cx.connect(_SOURCE_DATABASE_USER, _SOURCE_DATABASE_PASSWORD,
                                                           dsn=dsn_tns_source)

                    if is_target:
                        if _TARGET_CONNECTION_TYPE.lower() == "oracle":
                            print(_TARGET_STATIC)

                            dsn_tns_target = cx.makedsn(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT,
                                                        _TARGET_PARAM_SERVICE_NAME)
                            target_connection = cx.connect(_TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                                                           dsn=dsn_tns_target)
                            sql_validate_target = "select cname,coltype from col where lower(tname) = '{0}'".format(
                                _TARGET_STATIC)
                            sql_format_target = "insert into {0} ({1}) values ({2})"
                            sql_value_format = ""
                            sql_column_target = ""
                            sql_insert_target = ""
                            sql_value_target = ""
                            cursor_schema_oracle = target_connection.cursor()
                            cursor_schema_oracle.execute(sql_validate_target)
                            result_schema_oracle = cursor_schema_oracle.fetchall()

                            isfind_column = False
                            if len(array_target) > 0:
                                for item_target in array_target:
                                    for key in item_target:
                                        if len(result_schema_oracle) > 0:
                                            for itcolumn in result_schema_oracle:
                                                isfind_column = False
                                                if str(itcolumn[0]).lower() == key.lower():
                                                    isfind_column = True
                                                if isfind_column:

                                                    if sql_column_target != "":
                                                        sql_column_target += ","
                                                    sql_column_target += key

                                                    if sql_value_format != "":
                                                        sql_value_format += ","
                                                    if str(itcolumn[1]).lower() == "varchar2":
                                                        sql_value_format += "'{0}'".format(key)
                                                    else:
                                                        sql_value_format += "{0}".format(key)

                                    break

                            if len(array_target) > 0:
                                sql_value_target = sql_value_format
                                for item_target in array_target:
                                    for key in item_target:
                                        if sql_column_target.find(key) > -1:
                                            sql_value_target = str(sql_value_target).replace(key, str(item_target[key]))

                                    sql_insert_target = sql_format_target.format(_TARGET_STATIC, sql_column_target,
                                                                                 sql_value_target)
                                    print(sql_insert_target)
                                    cursor_execute_target = target_connection.cursor()
                                    cursor_execute_target.execute(sql_insert_target)
                                if target_connection:
                                    target_connection.commit()

                        elif _TARGET_CONNECTION_TYPE.lower() == "hadoop":
                            dsn_tns_target = cx.makedsn(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT,
                                                        _TARGET_PARAM_SERVICE_NAME)
                            target_connection = cx.connect(_TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                                                           dsn=dsn_tns_target)
                        elif _TARGET_CONNECTION_TYPE.lower() == "kafka":
                            dsn_tns_target = cx.makedsn(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT,
                                                        _TARGET_PARAM_SERVICE_NAME)
                            target_connection = cx.connect(_TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                                                           dsn=dsn_tns_target)
                        elif _TARGET_CONNECTION_TYPE.lower() == "txtfile":
                            dsn_tns_target = cx.makedsn(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT,
                                                        _TARGET_PARAM_SERVICE_NAME)
                            target_connection = cx.connect(_TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                                                           dsn=dsn_tns_target)
                        elif _TARGET_CONNECTION_TYPE.lower() == "csvfile":
                            dsn_tns_target = cx.makedsn(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT,
                                                        _TARGET_PARAM_SERVICE_NAME)
                            target_connection = cx.connect(_TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                                                           dsn=dsn_tns_target)

                        ####################################### PROCESS             #######################################

                        ####################################### TARGET CONNECTION    #######################################
                    # if target_connection:
    except Exception as e:
        print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))


class aux_connection(object):
    def __init__(self, _host, _port, _service, _user, _password):
        self.host = _host
        self.port = _port
        self.service = _service
        self.user = _user
        self.password = _password
        print(_host)
        print(_port)
        print(_service)
        print(_user)
        print(_password)


class ods_connection(object):
    def __init__(self, _host, _port, _service, _user, _password):
        self.host = _host
        self.port = _port
        self.service = _service
        self.user = _user
        self.password = _password
        print(_host)
        print(_port)
        print(_service)
        print(_user)
        print(_password)
        partner_sql = "partner.sql"
        try:
            t0 = time.time()
            now = datetime.now(tz)
            now_start = now.strftime('%Y-%m-%dT%H:%M:%S') + ('-%02d' % (now.microsecond / 10000))
            print(f'============= {now_start} ============= ')

            result_temp = []
            sql_file = open(partner_sql, "r")
            sql_result = sql_file.read()
            print(sql_result)
            dsn_tns_temp = cx.makedsn(self.host, self.port, service_name=self.service)
            connect_temp = cx.connect(self.user, self.password, dsn=dsn_tns_temp)
            if connect_temp:
                cursor_temp = connect_temp.cursor()
                cursor_temp.execute(sql_result)
                result_temp = cursor_temp.fetchall()
        except Exception as e:
            print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
        finally:
            now = datetime.now(tz)
            time_used = (time.time() - t0)
            now_finish = now.strftime('%Y-%m-%dT%H:%M:%S') + ('-%02d' % (now.microsecond / 10000))
            print(f'============= {now_finish} ============= ')
            print(f"elapsed time {(time.time() - t0):0.1f} seconds")
        self.partner_record = result_temp

    def len_record(self):
        return len(self.partner_record)

    def lookup_partner(self, employee_id):
        print('*******' + employee_id)
        if len(self.partner_record) > 0:
            Record = self.partner_record
            for a in Record:
                if str(a[0]) == str(employee_id):
                    lookup = a
                    break
            return lookup


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    #logger = logging.getLogger('kafka')
    #logger.addHandler(logging.StreamHandler(sys.stdout))
    #logger.setLevel(logging.DEBUG)

    # os.environ["KRB5CCNAME"] = "C:\\temp\\krb5cache"
    # os.environ["KRB5_CONFIG"] = "krb5.conf"
    # os.environ['KRB5_CLIENT_KTNAME'] = 'kfuat_dwhpdpa.client.keytab'
    # os.environ["APP_HOME"] = os.path.dirname(os.path.realpath(__file__))
    # os.environ["APP_PID"] = str(os.getpid())
    # os.environ['KAFKA_OPTS'] = '-Djava.security.auth.login.config=client_jaas.conf -Djava.security.krb5.conf=krb5.conf'
    # os.environ["DB_HOST"] = "dbdwhdv1"
    # os.environ["DB_PORT"] = "1550"
    # os.environ["DB_SERVICE"] = "TEDWDEV"
    # os.environ["DB_USER"] = "TEDWBORAPPO"
    # os.environ["DB_PASSWORD"] = "bortedw123"
    if "DB_HOST" in os.environ:
        _host = os.environ["DB_HOST"]
    else:
        _host = "dbdwhdv1"
    if "DB_PORT" in os.environ:
        _port = os.environ["DB_PORT"]
    else:
        _port = "1550"
    if "DB_SERVICE" in os.environ:
        _service = os.environ["DB_SERVICE"]
    else:
        _service = "TEDWDEV"
    if "DB_USER" in os.environ:
        _user = os.environ["DB_USER"]
    else:
        _user = "TEDWBORAPPO"
    if "DB_PASSWORD" in os.environ:
        _password = os.environ["DB_PASSWORD"]
    else:
        _password = "bortedw123"

    if "SYNCHONIE_ID" in os.environ:
        _syn_id = os.environ["SYNCHONIE_ID"]
    else:
        _syn_id = "d264a4907227f82de05329da13ac8e18"  # "d164a4907227f82de05329da13ac8e18"  d22d22227821996de05329da13ac97c2 # "d164a4907227f82de05329da13ac8e18"  # "d224a6019b656c17e05329da13acc0fc"  #
    if "APP_NAME" in os.environ:
        _log_name = os.environ["APP_NAME"]
    else:
        _log_name = "synchonize-{0}".format(_syn_id)
    if "REFERSH_TIME_LOOKUP" in os.environ:
        _time_refresh = os.environ["REFERSH_TIME_LOOKUP"]
    else:
        _time_refresh = "00:00"

    if "DB_HOST_ODS" in os.environ:
        _host_ods = os.environ["DB_HOST_ODS"]
    else:
        _host_ods = "172.19.249.192"
    if "DB_PORT_ODS" in os.environ:
        _port_ods = os.environ["DB_PORT_ODS"]
    else:
        _port_ods = "1552"
    if "DB_SERVICE_ODS" in os.environ:
        _service_ods = os.environ["DB_SERVICE_ODS"]
    else:
        _service_ods = "ODSPRD2"
    if "DB_USER_ODS" in os.environ:
        _user_ods = os.environ["DB_USER_ODS"]
    else:
        _user_ods = "NRTBIAPPR"
    if "DB_PASSWORD_ODS" in os.environ:
        _password_ods = os.environ["DB_PASSWORD_ODS"]
    else:
        _password_ods = "Ytd78#578kkp"
    _aux_connector = aux_connection(_host, _port, _service, _user, _password)
    # _ods_connector = ods_connection(_host_ods, _port_ods, _service_ods, _user_ods, _password_ods)
    _ods_connector = _aux_connector
    process(_aux_connector, _ods_connector, _syn_id, _time_refresh)
