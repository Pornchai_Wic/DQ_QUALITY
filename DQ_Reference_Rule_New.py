import os
import sys
import argparse
import datetime

import gc
import cx_Oracle as cx
import pyodbc as hadoop
import pytz
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from datetime import datetime, timedelta
import time

import pandas as pd
import json

from elasticsearch.connection import create_ssl_context
import urllib3
import ssl
from cryptography.fernet import Fernet, InvalidToken
import logging

key_api = "8zNU418HIcCijV_KPfCm-OwNsVKGhMOxtl906nyzzmM="


def decryption(key: None, param: None):
    cipher_suite = None
    try:
        f = Fernet(key)
    except InvalidToken:
        f = None
    if not f is None:
        try:
            decrypted_bytes = f.decrypt(param.encode("utf-8"))
        except InvalidToken:
            decrypted_bytes = None  # TODO(kmullins): Shall we log this case? Is it expected?
        if not decrypted_bytes is None:
            decrypted_string = decrypted_bytes.decode()  # bytes -> str
        else:
            decrypted_string = "encryption is not support"
    else:
        key = "key is not support"
    return decrypted_string


tz = pytz.timezone('Asia/Bangkok')


def fields(cursor):
    """ Given a DB API 2.0 cursor object that has been executed, returns
    a dictionary that maps each field name to a column index; 0 and up. """
    results = {}
    column = 0
    for d in cursor.description:
        results[d[0]] = column
        column = column + 1

    return results


def target_elasticsearch(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT, _TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                         _TARGET_STATIC, _TARGET_FIELD_STATIC, array_target):
    urllib3.disable_warnings()

    host = _TARGET_PARAM_HOST
    port = _TARGET_PARAM_PORT
    user = _TARGET_DATABASE_USER
    password = _TARGET_DATABASE_PASSWORD
    now = datetime.today()

    ssl_context = create_ssl_context(cafile='cacert-esd.pem')
    ssl_context.check_hostname = False
    ssl_context.verify_mode = ssl.CERT_OPTIONAL
    es = Elasticsearch(hosts=[{'host': host, 'port': _TARGET_PARAM_PORT}],
                       scheme="https",
                       verify_certs=True,
                       ssl_context=ssl_context,
                       http_auth=(user, password))
    if _TARGET_FIELD_STATIC == "":
        document_id = str(now.strftime('%Y-%m-%d'))
    else:
        document_id = _TARGET_FIELD_STATIC

    index = _TARGET_STATIC + document_id
    print(index)
    doc_list = []
    now = datetime.today()
    now_timestatmp = now.strftime('%Y-%m-%dT%H:%M:%S') + ('-%02d' % (now.microsecond / 10000))
    ind = 0
    topic_name = ""
    if len(array_target) > 0:
        for item_target in array_target:
            doc_list += [item_target]

        print(index)
        if len(doc_list) > 0:
            try:
                print(
                    "\nAttempting to index =" + index + " the list of docs using helpers.bulk(" + str(
                        len(doc_list)) + ")")

                resp = helpers.bulk(es, doc_list, index=index, doc_type='_doc')
                print("helpers.bulk " + now_timestatmp + " = ", resp)
            except Exception as e:
                print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
            finally:
                es.close()


def is_connection_validate(HOST, PORT, USER, PASS):
    return len(HOST) > 0 and len(PORT) > 0 and len(USER) > 0 and len(PASS) > 0


def percent(x, y):
    result = ""
    if not x and not y:
        result = ("x = 0%\ny = 0%")
    elif x < 0 or y < 0:
        result = ("The inputs can't be negative!")
    else:
        final = 100 / (x + y)
        x *= final
        y *= final
        result = ('x = {}%\ny = {}%'.format(x, y))
    return result


def process(host, port, service, user, password, _row_commit):
    try:
        _RULE_ID = ""
        _INGRESS_DATA_ID = ""
        _INGRESS_DATA_NAME = ""
        _INGRESS_CONNECTION_ID = ""
        _INGRESS_SQL_COMMAND = ""
        _INGRESS_TARGET_TABLE = ""
        _INGRESS_CONNECTION_NAME = ""
        _INGRESS_PARAM_HOST = ""
        _INGRESS_PARAM_PORT = ""
        _INGRESS_PARAM_SERVICE_NAME = ""
        _CDE_ID = ""
        _CDE_NAME = ""
        _REMARK = ""
        _INGRESS_CONNECTION_TYPE = ""
        _INGRESS_CONNECTION_STRING = ""
        _INGRESS_PARAM_USER = ""
        _INGRESS_PARAM_PASSWORD = ""
        _INGRESS_EXISTS_TABLE = "0"
        is_ingress_exists_table = False
        is_ingress_exec_table = False

        _BUSINESS_RULE_ID_DETAIL = ""
        _BUSINESS_RULE_NAME_DETAIL = ""
        _SQL_COMMAND_DETAIL = ""
        _CONNECTION_NAME_DETAIL = ""
        _CONNECTION_STRING_DETAIL = ""
        _QUALITY_INDEX_ID_DETAIL = ""
        _PARAM_HOST_DETAIL = ""
        _PARAM_PORT_DETAIL = ""
        _PARAM_SERVICE_NAME_DETAIL = ""
        _CONNECTION_TYPE_DETAIL = ""
        _CONNECTION_USER_DETAIL = ""
        _CONNECTION_PASSWORD_DETAIL = ""

        _is_transaction = False
        _is_transaction_detail = False
        _is_transaction_detail_delete = False
        _is_new_uuid = False
        _TRANSACTION_ID = ""
        _RECORD_QTY = 0
        if _row_commit == "":
            _row_ = 100

        sql_running_reference = "select distinct reference_id ,SEQ,COLUMN_SEQ " \
                                "from DQ_REFERENCE_DETAIL " \
                                "where status ='0' " \
                                "and nvl(RUN_IMMEDIATE,'0')='1'"

        sql_format_reference_id = "select a.reference_id," \
                                  "\n b.INGRESS_DATA_ID," \
                                  "\n b.COLUMN_SEQ," \
                                  "\n b.COLUMN_NAME," \
                                  "\n b.COLUMN_TYPE ," \
                                  "\n b.RULE_DEFINITION_ID," \
                                  "\n b.SQL_COMMAND, " \
                                  "\n C.INGRESS_DATA_NAME, " \
                                  "\n C.SQL_COMMAND AS INGRESS_SQL_COMMAND, " \
                                  "\n C.INGRESS_TARGET || ' ' || nvl(C.INGRESS_TARGET_FILTER,'') AS INGRESS_TARGET_TABLE, " \
                                  "\n N.CONNECTION_NAME, " \
                                  "\n N.PARAM_HOST, " \
                                  "\n N.PARAM_PORT, " \
                                  "\n N.PARAM_SERVICE_NAME, " \
                                  "\n N.CONNECTION_TYPE, " \
                                  "\n N.DATABASE_USER," \
                                  "\n N.DATABASE_PASSWORD," \
                                  "\n C.USED_IS_COMMAND," \
                                  "\n N.CONNECTION_STRING," \
                                  "\n b.RULE_DEFINITION_NAME," \
                                  "\n nvl(b.SQL_COMMAND,R.SQL_COLUMN) as SQL_COLUMN," \
                                  "\n nvl(b.SEQ,1) SEQ," \
                                  "\n nvl(c.INGRESS_DATA_TYPE_RUN,1) INGRESS_DATA_TYPE_RUN," \
                                  "\n nvl(b.QUERY,'') as QUERY," \
                                  "\n nvl(b.SQL_COMMAND_OPPOSITE,'') as SQL_COMMAND_OPPOSITE," \
                                  "\n nvl(b.QUERY_OPPOSITE,'') as QUERY_OPPOSITE," \
                                  "\n nvl(b.QUERY_RECORD,'') as QUERY_RECORD" \
                                  "\n from DQ_REFERENCE a" \
                                  "\n inner join  DQ_REFERENCE_DETAIL b on (a.reference_id=b.reference_id) " \
                                  "\n inner join DQ_CONFIG_INGRESS_DATA C ON (b.INGRESS_DATA_ID = C.INGRESS_DATA_ID) " \
                                  "\n LEFT JOIN DWH_CONFIG_CONNECTION N ON (C.CONNECTION_ID = N.ID)  " \
                                  "\n LEFT JOIN DQ_CONFIG_RULE_DEFINITIONS R ON (b.RULE_DEFINITION_ID = R.RULE_DEFINITION_ID)  " \
                                  "\n where a.reference_id = '{reference_id}'" \
                                  "\n and b.seq = '{seq}'" \
                                  "\n and b.column_seq = '{column_seq}'" \
                                  "\n and a.status='0' " \
                                  "\n and nvl(b.RUN_IMMEDIATE,'0')='1' " \
                                  "\n order by a.reference_id,b.INGRESS_DATA_ID,nvl(b.SEQ,1),b.COLUMN_SEQ"

        sql_reference = ""
        sql_format_update_reference_dt = "UPDATE DQ_REFERENCE_DETAIL t" \
                                         "\n set  " \
                                         "\n t.STATUS_RUNNING       = '{3}' " \
                                         "\n ,t.START_DATE          = '{4}' " \
                                         "\n ,t.END_DATE            = '{5}' " \
                                         "\n ,t.RECORD_QTY          =  {6} " \
                                         "\n ,t.VALIDATE_RECORD_QTY =  {7} " \
                                         "\n ,t.UPD_BY          = sysdate " \
                                         "\n WHERE t.REFERENCE_ID='{0}'" \
                                         "\n and t.SEQ={1}" \
                                         "\n and t.COLUMN_SEQ={2}"
        sql_update_reference_dt = ""

        sql_format_transaction_detail = "Merge Into DQ_TRANSACTION_DETAIL t" \
                                        "\n Using (" \
                                        "\n select " \
                                        "\n  '{0}' as TRANSACTION_ID" \
                                        "\n ,'{1}' as RULE_ID " \
                                        "\n ,'{2}' as BUSINESS_RULE_ID " \
                                        "\n , {3}  as QUALITY_INDEX_ID " \
                                        "\n ,'{4}' as SQL_COMMAND " \
                                        "\n , {5}  as RECORD_QTY " \
                                        "\n , {6}  as VALIDATE_RECORD_QTY " \
                                        "\n ,'{7}' as USER_BY " \
                                        "\n ,to_date('{8}','dd/MM/yyyy HH24:mi:ss') as start_date " \
                                        "\n ,to_date('{9}','dd/MM/yyyy HH24:mi:ss') as end_date  " \
                                        "\n ,sysdate as TM " \
                                        "\n from dual ) s " \
                                        "\n on (" \
                                        "\n t.transaction_id=s.transaction_id" \
                                        "\n and t.RULE_ID=s.RULE_ID" \
                                        "\n and t.BUSINESS_RULE_ID=s.BUSINESS_RULE_ID" \
                                        "\n and t.QUALITY_INDEX_ID=s.QUALITY_INDEX_ID" \
                                        "\n )" \
                                        "\n when Matched Then" \
                                        "\n UPDATE set  " \
                                        "\n t.RECORD_QTY            = s.RECORD_QTY " \
                                        "\n ,t.VALIDATE_RECORD_QTY  = s.VALIDATE_RECORD_QTY " \
                                        "\n ,t.SQL_COMMAND          = s.SQL_COMMAND " \
                                        "\n ,t.START_DATE           = s.START_DATE " \
                                        "\n ,t.END_DATE             = s.END_DATE " \
                                        "\n ,t.UPD_TM               = s.TM " \
                                        "\n ,t.UPD_BY               = s.USER_BY " \
                                        "\n when Not Matched Then" \
                                        "\n Insert (" \
                                        "t.TRANSACTION_ID" \
                                        ",t.RULE_ID" \
                                        ",t.BUSINESS_RULE_ID " \
                                        ",t.QUALITY_INDEX_ID " \
                                        ",t.SQL_COMMAND" \
                                        ",t.RECORD_QTY" \
                                        ",t.START_DATE" \
                                        ",t.END_DATE" \
                                        ", t.PPN_BY" \
                                        ", t.PPN_TM)" \
                                        "\n Values (" \
                                        "s.TRANSACTION_ID" \
                                        ", s.RULE_ID" \
                                        ", s.BUSINESS_RULE_ID" \
                                        ", s.QUALITY_INDEX_ID " \
                                        ", s.SQL_COMMAND " \
                                        ", s.RECORD_QTY " \
                                        ", s.START_DATE " \
                                        ", s.END_DATE " \
                                        ", s.USER_BY" \
                                        ", s.TM) "

        sql_format_transaction_value = "Insert Into DQ_TRANSACTION_DETAIL_VALUE " \
                                       "\n  (" \
                                       "TRANSACTION_ID " \
                                       ",RULE_ID " \
                                       ",BUSINESS_RULE_ID " \
                                       ",QUALITY_INDEX_ID " \
                                       ",ROW_SEQ " \
                                       ",COL_SEQ " \
                                       ",COLUMN_NAME " \
                                       ",COLUMN_VALUE " \
                                       ",COLUMN_TYPE " \
                                       ",PPN_BY " \
                                       ") " \
                                       "\n values " \
                                       "\n (" \
                                       ":TRANSACTION_ID " \
                                       ",:RULE_ID " \
                                       ",:BUSINESS_RULE_ID " \
                                       ",:QUALITY_INDEX_ID " \
                                       ",:ROW_SEQ " \
                                       ",:COL_SEQ " \
                                       ",:COLUMN_NAME " \
                                       ",:COLUMN_VALUE " \
                                       ",:COLUMN_TYPE " \
                                       ",:PPN_BY " \
                                       ") "

        dsn_tns_config = cx.makedsn(host, port, service_name=service)
        connect_config = cx.connect(user, password, dsn=dsn_tns_config)
        if connect_config:
            print(sql_running_reference)
            cursor_loop = connect_config.cursor()
            cursor_loop.execute(sql_running_reference)
            result_loop = cursor_loop.fetchall()
            if len(result_loop) > 0:
                for item_loop in result_loop:
                    _reference_id = item_loop[0]
                    _seq = item_loop[1]
                    _column_seq = item_loop[2]
                    if _reference_id != "":
                        sql_reference = sql_format_reference_id.format(reference_id=_reference_id, seq=_seq,
                                                                       column_seq=_column_seq)
                        cursor_qa_dt = connect_config.cursor()
                        cursor_qa_dt.execute(sql_reference)
                        result_qa_dt = cursor_qa_dt.fetchall()
                        if len(result_qa_dt) > 0:
                            for item_qa_dt in result_qa_dt:

                                _REFERENCE_ID = str(item_qa_dt[0])
                                _INGRESS_DATA_ID = str(item_qa_dt[1])
                                _COLUMN_SEQ = str(item_qa_dt[2])
                                _COLUMN_NAME = str(item_qa_dt[3])
                                _COLUMN_TYPE = str(item_qa_dt[4])
                                _RULE_DEFINITION_ID = str(item_qa_dt[5])
                                _SQL_COMMAND = str(item_qa_dt[6])
                                _INGRESS_DATA_NAME = str(item_qa_dt[7])
                                _INGRESS_SQL_COMMAND = str(item_qa_dt[8])
                                _INGRESS_TARGET_TABLE = str(item_qa_dt[9])
                                _CONNECTION_NAME = str(item_qa_dt[10])
                                _PARAM_HOST = str(item_qa_dt[11])
                                _PARAM_PORT = str(item_qa_dt[12])
                                _PARAM_SERVICE_NAME = str(item_qa_dt[13])
                                _CONNECTION_TYPE = str(item_qa_dt[14])
                                _DATABASE_USER = str(item_qa_dt[15])
                                _DATABASE_PASSWORD = str(item_qa_dt[16])
                                _USED_IS_COMMAND = str(item_qa_dt[17])
                                _CONNECTION_STRING = str(item_qa_dt[18])
                                _RULE_DEFINITION_NAME = str(item_qa_dt[19])
                                _SQL_COLUMN = item_qa_dt[20]
                                _SEQ = item_qa_dt[21]
                                _INGRESS_DATA_TYPE_RUN = item_qa_dt[22]
                                _SQL_COLUMN_QUERY = item_qa_dt[23]
                                _SQL_COLUMN_OPPOSITE = item_qa_dt[24]
                                _SQL_COLUMN_QUERY_OPPOSITE = item_qa_dt[25]
                                _QUERY_RECORD = item_qa_dt[26]
                                is_user_is_command = (_INGRESS_DATA_TYPE_RUN == "2")
                                print(_CONNECTION_TYPE)
                                if _CONNECTION_TYPE != "":
                                    if _CONNECTION_TYPE.lower() == "hadoop":
                                        data_rule = hadoop.connect(_CONNECTION_STRING, autocommit=True)
                                    elif _CONNECTION_TYPE.lower() == "oracle":
                                        dsn_tns = cx.makedsn(_PARAM_HOST, _PARAM_PORT,
                                                             _PARAM_SERVICE_NAME)
                                        data_rule = cx.connect(_DATABASE_USER, _DATABASE_PASSWORD,
                                                               dsn=dsn_tns)
                                    if data_rule:
                                        try:
                                            sql_query_rule = ""
                                            sql_query_format_rule = ""
                                            sql_query_format = ""
                                            sql_query = ""
                                            print(_INGRESS_TARGET_TABLE)
                                            print(_INGRESS_SQL_COMMAND)
                                            print('************************' + _INGRESS_SQL_COMMAND)

                                            # is_user_is_command = False
                                            if _SQL_COLUMN_QUERY is not None:
                                                if not is_user_is_command:
                                                    sql_query_format = "select count(*) as qty from {0} "
                                                    sql_query = sql_query_format.format(_INGRESS_TARGET_TABLE)
                                                    if _SQL_COLUMN_QUERY is not None:
                                                        sql_query_rule = _SQL_COLUMN_QUERY
                                                    else:
                                                        if _SQL_COLUMN is not None:
                                                            sql_query_format_rule = "select count(*) as qty  from {table_name} and "
                                                            sql_query_rule = sql_query_format_rule.format(
                                                                field_name=_COLUMN_NAME,
                                                                table_name=_INGRESS_TARGET_TABLE) + _SQL_COLUMN

                                                        else:
                                                            sql_query_format_rule = "select count(*) as qty  from {table_name} "
                                                            sql_query_rule = sql_query_format_rule.format(
                                                                field_name=_COLUMN_NAME,
                                                                table_name=_INGRESS_TARGET_TABLE)
                                                else:
                                                    sql_query_format = "select count(*) as qty from ({0}) a "
                                                    sql_query = sql_query_format.format(_INGRESS_SQL_COMMAND)
                                                    if _SQL_COLUMN_QUERY is not None:
                                                        sql_query_rule = _SQL_COLUMN_QUERY
                                                    else:
                                                        if _SQL_COLUMN is not None:
                                                            sql_query_format_rule = "select count(*) as qty  from ({table_name}) a where "
                                                            sql_query_rule = sql_query_format_rule.format(
                                                                field_name=_COLUMN_NAME,
                                                                table_name=_INGRESS_SQL_COMMAND) + _SQL_COLUMN
                                                        else:
                                                            sql_query_format_rule = "select count(*) as qty  from ({table_name}) a "
                                                            sql_query_rule = sql_query_format_rule.format(
                                                                field_name=_COLUMN_NAME,
                                                                table_name=_INGRESS_SQL_COMMAND)

                                                if sql_query_rule != "":
                                                    try:
                                                        _RECORD_VALIDATE_DT_QTY = 0
                                                        _RECORD_DT_QTY = 0

                                                        sql_format_update_reference_dt = "UPDATE DQ_REFERENCE_DETAIL t" \
                                                                                         "\n set  " \
                                                                                         "\n t.STATUS_RUNNING       = '{3}' " \
                                                                                         "\n ,t.START_DATE          = sysdate " \
                                                                                         "\n ,t.UPD_BY          = sysdate " \
                                                                                         "\n ,t.QUERY          = '{4}' " \
                                                                                         "\n WHERE t.REFERENCE_ID='{0}'" \
                                                                                         "\n and t.SEQ ={1}" \
                                                                                         "\n and t.COLUMN_SEQ={2}"
                                                        sql_update_reference_dt = sql_format_update_reference_dt.format(
                                                            _REFERENCE_ID,
                                                            _SEQ,
                                                            _COLUMN_SEQ,
                                                            "1", sql_query_rule.replace("'", "''"))

                                                        cursor_qa_dt_running = connect_config.cursor()
                                                        print(sql_update_reference_dt)
                                                        cursor_qa_dt_running.execute(sql_update_reference_dt)
                                                        connect_config.commit()
                                                        if _QUERY_RECORD is not None:
                                                            if _QUERY_RECORD != "":
                                                                sql_query = _QUERY_RECORD

                                                        print('************'+sql_query+'*************')
                                                        cursor_result_qa = data_rule.cursor()
                                                        cursor_result_qa.execute(sql_query)
                                                        result_qa = cursor_result_qa.fetchone()
                                                        _RECORD_DT_QTY = (result_qa[0])

                                                        sql_format_update_reference_dt = "UPDATE DQ_REFERENCE_DETAIL t" \
                                                                                         "\n set  " \
                                                                                         "\n t.RECORD_QTY          =  {3} " \
                                                                                         "\n ,t.UPD_BY          = sysdate " \
                                                                                         "\n WHERE t.REFERENCE_ID='{0}'" \
                                                                                         "\n and t.SEQ={1}" \
                                                                                         "\n and t.COLUMN_SEQ={2}"
                                                        sql_update_reference_dt = sql_format_update_reference_dt.format(
                                                            _REFERENCE_ID,
                                                            _SEQ,
                                                            _COLUMN_SEQ,
                                                            str(_RECORD_DT_QTY))

                                                        cursor_qa_dt_running = connect_config.cursor()
                                                        print(sql_update_reference_dt)
                                                        cursor_qa_dt_running.execute(sql_update_reference_dt)
                                                        connect_config.commit()

                                                        cursor_result_qa = data_rule.cursor()
                                                        print(sql_query_rule)
                                                        cursor_result_qa.execute(sql_query_rule)
                                                        result_qa = cursor_result_qa.fetchone()
                                                        _RECORD_VALIDATE_DT_QTY = (result_qa[0])
                                                        _DQI = (round(_RECORD_VALIDATE_DT_QTY / _RECORD_DT_QTY * 100, 2))
                                                        #_DQI = (100 - round(
                                                        #    _RECORD_VALIDATE_DT_QTY / _RECORD_DT_QTY * 100, 2))
                                                        #if _DQI == 0:
                                                        #    _DQI = 100
                                                        print('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX = ' + str(_DQI))
                                                        sql_format_update_reference_dt = "UPDATE DQ_REFERENCE_DETAIL t" \
                                                                                         "\n set  " \
                                                                                         "\n t.VALIDATE_RECORD_QTY  =  {3} " \
                                                                                         "\n ,t.DQI              = {4} " \
                                                                                         "\n ,t.UPD_BY              = sysdate " \
                                                                                         "\n WHERE t.REFERENCE_ID='{0}'" \
                                                                                         "\n and t.SEQ={1}" \
                                                                                         "\n and t.COLUMN_SEQ={2}"
                                                        sql_update_reference_dt = sql_format_update_reference_dt.format(
                                                            _REFERENCE_ID,
                                                            _SEQ,
                                                            _COLUMN_SEQ,
                                                            str(_RECORD_VALIDATE_DT_QTY),
                                                            str(_DQI)
                                                        )
                                                        print('error 496')

                                                        cursor_qa_dt_running = connect_config.cursor()
                                                        print(sql_update_reference_dt)
                                                        cursor_qa_dt_running.execute(sql_update_reference_dt)
                                                        connect_config.commit()

                                                        if (_RECORD_DT_QTY == _RECORD_VALIDATE_DT_QTY) and (_RECORD_DT_QTY > 0):
                                                            sql_format_update_reference_dt = "UPDATE DQ_REFERENCE_DETAIL t" \
                                                                                             "\n set  " \
                                                                                             "\n t.STATUS_RUNNING       = '{3}' " \
                                                                                             "\n ,t.END_DATE          = sysdate " \
                                                                                             "\n ,t.UPD_BY          = sysdate " \
                                                                                             "\n ,t.DQI          = 100 " \
                                                                                             "\n ,t.RUN_IMMEDIATE  = '0' " \
                                                                                             "\n WHERE t.REFERENCE_ID='{0}'" \
                                                                                             "\n and t.SEQ={1}" \
                                                                                             "\n and t.COLUMN_SEQ={2}"
                                                            sql_update_reference_dt = sql_format_update_reference_dt.format(
                                                                _REFERENCE_ID,
                                                                _SEQ,
                                                                _COLUMN_SEQ,
                                                                "2")
                                                        else:
                                                            sql_format_update_reference_dt = "UPDATE DQ_REFERENCE_DETAIL t" \
                                                                                             "\n set  " \
                                                                                             "\n t.STATUS_RUNNING       = '{3}' " \
                                                                                             "\n ,t.END_DATE          = sysdate " \
                                                                                             "\n ,t.UPD_BY          = sysdate " \
                                                                                             "\n ,t.RUN_IMMEDIATE  = '0' " \
                                                                                             "\n WHERE t.REFERENCE_ID='{0}'" \
                                                                                             "\n and t.SEQ={1}" \
                                                                                             "\n and t.COLUMN_SEQ={2}"

                                                            sql_update_reference_dt = sql_format_update_reference_dt.format(
                                                                _REFERENCE_ID,
                                                                _SEQ,
                                                                _COLUMN_SEQ,
                                                                "2")

                                                        cursor_qa_dt_running = connect_config.cursor()
                                                        print('error 536')

                                                        print(sql_update_reference_dt)
                                                        cursor_qa_dt_running.execute(   sql_update_reference_dt)
                                                        connect_config.commit()
                                                    except Exception as e:
                                                        print('error 541')
                                                        print(
                                                            'Error! Code: {c}, Message, {m}'.format(c=type(e).__name__,
                                                                                                    m=str(e)))
                                                        err_msg = 'Error! Code: {c}, Message, {m}'.format(
                                                            c=type(e).__name__, m=str(e))
                                                        sql_format_update_reference_dt = "UPDATE DQ_REFERENCE_DETAIL t" \
                                                                                         "\n set  " \
                                                                                         "\n t.STATUS_RUNNING       = '{3}' " \
                                                                                         "\n ,t.RUN_IMMEDIATE       = '3' " \
                                                                                         "\n ,t.END_DATE          = sysdate " \
                                                                                         "\n ,t.UPD_BY          = sysdate " \
                                                                                         "\n ,t.DQI             = 0 " \
                                                                                         "\n ,t.ERROR_MSG          = '{4}' " \
                                                                                         "\n WHERE t.REFERENCE_ID='{0}'" \
                                                                                         "\n and t.SEQ={1}" \
                                                                                         "\n and t.COLUMN_SEQ={2}"
                                                        sql_update_reference_dt = sql_format_update_reference_dt.format(
                                                            _REFERENCE_ID,
                                                            _SEQ,
                                                            _COLUMN_SEQ,
                                                            "3", err_msg.replace("'", "''"))
                                                        cursor_qa_dt_running = connect_config.cursor()
                                                        print(sql_update_reference_dt)
                                                        cursor_qa_dt_running.execute(sql_update_reference_dt)
                                                        connect_config.commit()


                                        except Exception as e:
                                            print('error 543')
                                            print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
                                            err_msg = 'Error! Code: {c}, Message, {m}'.format(
                                                c=type(e).__name__, m=str(e))
    except Exception as e:
        print('error 548')
        print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    os.environ["APP_HOME"] = os.path.dirname(os.path.realpath(__file__))
    os.environ["APP_PID"] = str(os.getpid())
    if "CDE" in os.environ:
        _cde_id = os.environ["CDE"]
    else:
        _cde_id = ""
    if "DB_HOST" in os.environ:
        _host_db = os.environ["DB_HOST"]
    else:
        _host_db = "dbdwhdv1"
    if "DB_PORT" in os.environ:
        _port_db = os.environ["DB_PORT"]
    else:
        _port_db = "1550"
    if "DB_SERVICE" in os.environ:
        _service_db = os.environ["DB_SERVICE"]
    else:
        _service_db = "TEDWDEV"
    if "UD" in os.environ:
        _user_db = os.environ["USER_DB"]
    else:
        _user_db = "TEDWBORAPPO"
    if "PD" in os.environ:
        _password_db = os.environ["PASSWORD"]
    else:
        _password_db = "bortedw123"

    if "ROW_COMMIT" in os.environ:
        _row_commit = os.environ["RULE_ID"]
    else:
        _row_commit = "100"

    process(_host_db, _port_db, _service_db, _user_db, _password_db, _row_commit)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
