import os
import sys
import argparse
import yaml
import datetime

import gc
import cx_Oracle as cx
import pyodbc as hadoop
import pytz
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from datetime import datetime, timedelta
import time

import pandas as pd
import json

from elasticsearch.connection import create_ssl_context
import urllib3
import ssl
from cryptography.fernet import Fernet, InvalidToken
import logging

key_api = "8zNU418HIcCijV_KPfCm-OwNsVKGhMOxtl906nyzzmM="


def decryption(key: None, param: None):
    cipher_suite = None
    try:
        f = Fernet(key)
    except InvalidToken:
        f = None
    if not f is None:
        try:
            decrypted_bytes = f.decrypt(param.encode("utf-8"))
        except InvalidToken:
            decrypted_bytes = None  # TODO(kmullins): Shall we log this case? Is it expected?
        if not decrypted_bytes is None:
            decrypted_string = decrypted_bytes.decode()  # bytes -> str
        else:
            decrypted_string = "encryption is not support"
    else:
        key = "key is not support"
    return decrypted_string


tz = pytz.timezone('Asia/Bangkok')


def fields(cursor):
    """ Given a DB API 2.0 cursor object that has been executed, returns
    a dictionary that maps each field name to a column index; 0 and up. """
    results = {}
    column = 0
    for d in cursor.description:
        results[d[0]] = column
        column = column + 1

    return results


def target_elasticsearch(_TARGET_PARAM_HOST, _TARGET_PARAM_PORT, _TARGET_DATABASE_USER, _TARGET_DATABASE_PASSWORD,
                         _TARGET_STATIC, _TARGET_FIELD_STATIC, array_target):
    urllib3.disable_warnings()

    host = _TARGET_PARAM_HOST
    port = _TARGET_PARAM_PORT
    user = _TARGET_DATABASE_USER
    password = _TARGET_DATABASE_PASSWORD
    now = datetime.today()

    ssl_context = create_ssl_context(cafile='cacert-esd.pem')
    ssl_context.check_hostname = False
    ssl_context.verify_mode = ssl.CERT_OPTIONAL
    es = Elasticsearch(hosts=[{'host': host, 'port': _TARGET_PARAM_PORT}],
                       scheme="https",
                       verify_certs=True,
                       ssl_context=ssl_context,
                       http_auth=(user, password))
    if _TARGET_FIELD_STATIC == "":
        document_id = str(now.strftime('%Y-%m-%d'))
    else:
        document_id = _TARGET_FIELD_STATIC

    index = _TARGET_STATIC + document_id
    print(index)
    doc_list = []
    now = datetime.today()
    now_timestatmp = now.strftime('%Y-%m-%dT%H:%M:%S') + ('-%02d' % (now.microsecond / 10000))
    ind = 0
    topic_name = ""
    if len(array_target) > 0:
        for item_target in array_target:
            doc_list += [item_target]

        print(index)
        if len(doc_list) > 0:
            try:
                print(
                    "\nAttempting to index =" + index + " the list of docs using helpers.bulk(" + str(
                        len(doc_list)) + ")")

                resp = helpers.bulk(es, doc_list, index=index, doc_type='_doc')
                print("helpers.bulk " + now_timestatmp + " = ", resp)
            except Exception as e:
                print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))
            finally:
                es.close()


def is_connection_validate(HOST, PORT, USER, PASS):
    return len(HOST) > 0 and len(PORT) > 0 and len(USER) > 0 and len(PASS) > 0


def percent(x, y):
    result = ""
    if not x and not y:
        result = ("x = 0%\ny = 0%")
    elif x < 0 or y < 0:
        result = ("The inputs can't be negative!")
    else:
        final = 100 / (x + y)
        x *= final
        y *= final
        result = ('x = {}%\ny = {}%'.format(x, y))
    return result


def process(host, port, service, user, password):
    try:

        _is_transaction = False
        _is_transaction_detail = False
        _is_transaction_detail_delete = False
        _is_new_uuid = False
        _TRANSACTION_ID = ""
        _RECORD_QTY = 0

        sql_job_reference = "select 'd9a59843b12ae84be05329da13ace93f' as reference_id,'47 12 * * *' as CRONJOB " \
                            " from DQ_REFERENCE  " \
                            " where status ='0'  and rownum <= 1"
        # " and status_running='1'" \
        # " and nvl(active_cronjob,'0') = '1'"

        dsn_tns_config = cx.makedsn(host, port, service_name=service)
        connect_config = cx.connect(user, password, dsn=dsn_tns_config)
        if connect_config:
            cursor_loop = connect_config.cursor()
            cursor_loop.execute(sql_job_reference)
            result_loop = cursor_loop.fetchall()
            if len(result_loop) > 0:
                for item_loop in result_loop:
                    _reference_id = item_loop[0]
                    _cronjob = item_loop[1]
                    if _reference_id != "" and _cronjob is not None:
                        print(_reference_id)
                        print(_cronjob)
                        _docker = "reghbpr01.dc1.true.th/dwh-python/python-qa-reference-job:dev"
                        schedule_master_yaml = f"""\
                                apiVersion: batch/v1beta1
                                kind: CronJob
                                metadata:
                                  name: {_reference_id}
                                spec:
                                  schedule: {_cronjob}
                                  jobTemplate:
                                    spec:
                                      template:
                                        spec:
                                          containers:
                                          - name: {_reference_id}
                                            image: {_docker}
                                            args:
                                            - python3
                                            - /usr/src/app/app.py {_reference_id}
                                            resources:
                                              requests:
                                                cpu: "500Mi"
                                              limits:
                                                cpu: "1000Mi"
                                              requests:
                                                memory: "500Mi"
                                              limits:
                                                memory: "1000Mi"
                                          restartPolicy: OnFailure
                                """
                        schedule_yaml = yaml.safe_load(schedule_master_yaml)
                        with open('{0}.yaml'.format(_reference_id), 'w') as file:
                            yaml.dump(schedule_yaml, file)
                        print(open('{0}.yaml'.format(_reference_id)).read())

                        with open('{0}.yaml'.format(_reference_id), 'r') as file:
                            pod_schedule = yaml.safe_load(file)
                            pod_schedule['metadata']['name'] = _reference_id
                            print(pod_schedule['spec']['schedule'])
                        os.system('cmd /k "kubectl apply -f {0}"'.format('{0}.yaml'.format(_reference_id)))  # windows
                        os.system("kubectl apply -f {0}".format('{0}.yaml'.format(_reference_id)))  # linux
    except Exception as e:
        print('Error! Code: {c}, Message, {m}'.format(c=type(e).__name__, m=str(e)))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    os.environ["APP_HOME"] = os.path.dirname(os.path.realpath(__file__))
    os.environ["APP_PID"] = str(os.getpid())
    if "CDE" in os.environ:
        _cde_id = os.environ["CDE"]
    else:
        _cde_id = ""
    if "DB_HOST" in os.environ:
        _host_db = os.environ["DB_HOST"]
    else:
        _host_db = "dbdwhdv1"
    if "DB_PORT" in os.environ:
        _port_db = os.environ["DB_PORT"]
    else:
        _port_db = "1550"
    if "DB_SERVICE" in os.environ:
        _service_db = os.environ["DB_SERVICE"]
    else:
        _service_db = "TEDWDEV"
    if "UD" in os.environ:
        _user_db = os.environ["USER_DB"]
    else:
        _user_db = "TEDWBORAPPO"
    if "PD" in os.environ:
        _password_db = os.environ["PASSWORD"]
    else:
        _password_db = "bortedw123"

    process(_host_db, _port_db, _service_db, _user_db, _password_db)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
